-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 08, 2019 at 10:16 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bizzon_vn`
--

-- --------------------------------------------------------

--
-- Table structure for table `bz1_contacts`
--

CREATE TABLE `bz1_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  `lang_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `files` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_content_types`
--

CREATE TABLE `bz1_content_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `params` varchar(5000) COLLATE utf8_unicode_ci DEFAULT '',
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `ordering` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bz1_content_types`
--

INSERT INTO `bz1_content_types` (`id`, `type`, `title`, `description`, `params`, `group`, `status`, `ordering`) VALUES
(9, 'content_news', 'news', '', '', 'content', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_customers`
--

CREATE TABLE `bz1_customers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` enum('Male','Female','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `member_class` int(11) DEFAULT '1',
  `score` int(11) NOT NULL,
  `exchange_score` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_groups`
--

CREATE TABLE `bz1_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `weight` int(11) DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rght` int(11) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_groups`
--

INSERT INTO `bz1_groups` (`id`, `title`, `weight`, `parent_id`, `lft`, `rght`, `description`) VALUES
(8, 'Customer', 1, 0, 0, 0, ''),
(9, 'Anonymous', 1, 0, 0, 0, NULL),
(10, 'Authenticated', 2, 0, 0, 0, ''),
(11, 'Administrator', 3, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_groups_permission`
--

CREATE TABLE `bz1_groups_permission` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_groups_permission`
--

INSERT INTO `bz1_groups_permission` (`id`, `group_id`, `permission`, `controller`, `module`) VALUES
(916, 9, 'testAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(917, 10, 'testAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(918, 11, 'testAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(1172, 11, 'changepasswordAction', 'User\\Admin\\Controller\\UserController', 'User'),
(1173, 11, 'profileAction', 'User\\Admin\\Controller\\UserController', 'User'),
(1957, 11, 'addAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(1958, 11, 'editAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(1959, 11, 'indexAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(1960, 11, 'publishAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(1961, 11, 'unpublishAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(1962, 11, 'deleteitemAction', 'Menu\\Admin\\Controller\\MenuController', 'Menu'),
(3884, 9, 'testAction', 'Application\\Controller\\IndexController', 'Application'),
(3885, 10, 'testAction', 'Application\\Controller\\IndexController', 'Application'),
(3886, 11, 'testAction', 'Application\\Controller\\IndexController', 'Application'),
(6599, 9, 'demouploadAction', 'Application\\Controller\\IndexController', 'Application'),
(6600, 10, 'demouploadAction', 'Application\\Controller\\IndexController', 'Application'),
(6601, 11, 'demouploadAction', 'Application\\Controller\\IndexController', 'Application'),
(7156, 9, 'commentAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7157, 10, 'commentAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7158, 11, 'commentAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7159, 9, 'addAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7160, 10, 'addAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7161, 11, 'addAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7162, 9, 'indexAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7163, 10, 'indexAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7164, 11, 'indexAction', 'Comment\\Front\\Controller\\CommentController', 'Comment'),
(7165, 11, 'deleteAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7166, 11, 'addAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7167, 11, 'editAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7168, 11, 'indexAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7169, 11, 'publishAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7170, 11, 'unpublishAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7171, 11, 'deleteitemAction', 'Comment\\Admin\\Controller\\CommentController', 'Comment'),
(7205, 9, 'uploadimageAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7206, 10, 'uploadimageAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7207, 11, 'uploadimageAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7208, 9, 'indexAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7209, 10, 'indexAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7210, 11, 'indexAction', 'Media\\Front\\Controller\\UploadController', 'Media'),
(7211, 11, 'indexAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7212, 11, 'popupAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7213, 11, 'addAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7214, 11, 'editAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7215, 11, 'publishAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7216, 11, 'unpublishAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7217, 11, 'deleteitemAction', 'Media\\Admin\\Controller\\MediaController', 'Media'),
(7224, 11, 'getscheduleAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7225, 11, 'addAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7226, 11, 'editAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7227, 11, 'indexAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7228, 11, 'publishAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7229, 11, 'unpublishAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7230, 11, 'deleteitemAction', 'Prize\\Admin\\Controller\\PrizeController', 'Prize'),
(7231, 11, 'addAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7232, 11, 'editAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7233, 11, 'indexAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7234, 11, 'publishAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7235, 11, 'unpublishAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7236, 11, 'deleteitemAction', 'Prize\\Admin\\Controller\\LuckyController', 'Prize'),
(7268, 9, 'indexAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7269, 10, 'indexAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7270, 11, 'indexAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7271, 9, 'categoryAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7272, 10, 'categoryAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7273, 11, 'categoryAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7274, 9, 'viewAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7275, 10, 'viewAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7276, 11, 'viewAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7277, 9, 'typesAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7278, 10, 'typesAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7279, 11, 'typesAction', 'Facebook\\Front\\Controller\\PostsController', 'Facebook'),
(7280, 9, 'aboutAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7281, 10, 'aboutAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7282, 11, 'aboutAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7283, 9, 'supportAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7284, 10, 'supportAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7285, 11, 'supportAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7286, 9, 'helpAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7287, 10, 'helpAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7288, 11, 'helpAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7289, 9, 'ruleAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7290, 10, 'ruleAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7291, 11, 'ruleAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7292, 9, 'termAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7293, 10, 'termAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7294, 11, 'termAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7295, 9, 'securityAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7296, 10, 'securityAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7297, 11, 'securityAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7298, 9, 'indexAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7299, 10, 'indexAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7300, 11, 'indexAction', 'Facebook\\Front\\Controller\\PagesController', 'Facebook'),
(7301, 9, 'indexAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7302, 10, 'indexAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7303, 11, 'indexAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7304, 9, 'viewAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7305, 10, 'viewAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7306, 11, 'viewAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7307, 9, 'submitAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7308, 10, 'submitAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7309, 11, 'submitAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7310, 9, 'successAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7311, 10, 'successAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7312, 11, 'successAction', 'Facebook\\Front\\Controller\\ContestItemController', 'Facebook'),
(7495, 11, 'changemoduleAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(7572, 11, 'addAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7573, 11, 'editAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7574, 11, 'indexAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7575, 11, 'publishAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7576, 11, 'unpublishAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7577, 11, 'deleteitemAction', 'Menu\\Controller\\MenuAdminController', 'Menu'),
(7578, 11, 'addAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7579, 11, 'editAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7580, 11, 'indexAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7581, 11, 'publishAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7582, 11, 'unpublishAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7583, 11, 'deleteitemAction', 'Menu\\Controller\\MenuItemAdminController', 'Menu'),
(7746, 11, 'addAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7747, 11, 'editAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7748, 11, 'indexAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7749, 11, 'publishAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7750, 11, 'unpublishAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7751, 11, 'deleteitemAction', 'Banner\\Admin\\Controller\\BannerController', 'Banner'),
(7752, 11, 'addAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7753, 11, 'editAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7754, 11, 'indexAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7755, 11, 'publishAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7756, 11, 'unpublishAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7757, 11, 'deleteitemAction', 'Link\\Admin\\Controller\\LinkController', 'Link'),
(7758, 11, 'addAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7759, 11, 'editAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7760, 11, 'indexAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7761, 11, 'publishAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7762, 11, 'unpublishAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7763, 11, 'deleteitemAction', 'Tag\\Admin\\Controller\\TagController', 'Tag'),
(7764, 11, 'addAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7765, 11, 'editAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7766, 11, 'indexAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7767, 11, 'publishAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7768, 11, 'unpublishAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7769, 11, 'deleteitemAction', 'Subscription\\Admin\\Controller\\SubscriptionController', 'Subscription'),
(7783, 11, 'addAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7784, 11, 'editAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7785, 11, 'indexAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7786, 11, 'publishAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7787, 11, 'unpublishAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7788, 11, 'deleteitemAction', 'Template\\Admin\\Controller\\TemplateController', 'Template'),
(7789, 11, 'addAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7790, 11, 'editAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7791, 11, 'indexAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7792, 11, 'publishAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7793, 11, 'unpublishAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7794, 11, 'deleteitemAction', 'UrlRewrite\\Admin\\Controller\\UrlRewriteController', 'UrlRewrite'),
(7923, 9, 'aboutAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7924, 10, 'aboutAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7925, 11, 'aboutAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7926, 9, 'supportAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7927, 10, 'supportAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7928, 11, 'supportAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7929, 9, 'helpAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7930, 10, 'helpAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7931, 11, 'helpAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7932, 9, 'ruleAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7933, 10, 'ruleAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7934, 11, 'ruleAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7935, 11, 'termAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7936, 9, 'securityAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7937, 10, 'securityAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(7938, 11, 'securityAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(8688, 9, 'categoryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8689, 10, 'categoryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8690, 11, 'categoryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8691, 9, 'viewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8692, 10, 'viewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8693, 11, 'viewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8694, 9, 'typesAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8695, 10, 'typesAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8696, 11, 'typesAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8888, 9, 'imageindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8889, 10, 'imageindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(8890, 11, 'imageindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9276, 9, 'galleryindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9277, 10, 'galleryindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9278, 11, 'galleryindexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9885, 9, 'newssearchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9886, 10, 'newssearchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(9887, 11, 'newssearchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(11752, 9, 'apartmentindexAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(11753, 10, 'apartmentindexAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(11754, 11, 'apartmentindexAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(12756, 9, 'indexAction', 'Znamlonghouse\\Front\\Controller\\IndexController', 'Znamlonghouse'),
(12757, 10, 'indexAction', 'Znamlonghouse\\Front\\Controller\\IndexController', 'Znamlonghouse'),
(12758, 11, 'indexAction', 'Znamlonghouse\\Front\\Controller\\IndexController', 'Znamlonghouse'),
(12759, 9, 'contactAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12760, 10, 'contactAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12761, 11, 'contactAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12762, 9, 'indexAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12763, 10, 'indexAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12764, 11, 'indexAction', 'Znamlonghouse\\Front\\Controller\\ContactController', 'Znamlonghouse'),
(12765, 9, 'aboutAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12766, 10, 'aboutAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12767, 11, 'aboutAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12768, 9, 'projectAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12769, 10, 'projectAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12770, 11, 'projectAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12771, 9, 'newsAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12772, 10, 'newsAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12773, 11, 'newsAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12774, 9, 'supportAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12775, 10, 'supportAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12776, 11, 'supportAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12777, 9, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12778, 10, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12779, 11, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PostController', 'Znamlonghouse'),
(12780, 9, 'apartmentAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12781, 10, 'apartmentAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12782, 11, 'apartmentAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12783, 9, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12784, 10, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12785, 11, 'indexAction', 'Znamlonghouse\\Front\\Controller\\PagesController', 'Znamlonghouse'),
(12902, 9, 'galleryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12903, 10, 'galleryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12904, 11, 'galleryAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12905, 9, 'searchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12906, 10, 'searchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12907, 11, 'searchAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12908, 9, 'timkiemcanhoAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12909, 10, 'timkiemcanhoAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12910, 11, 'timkiemcanhoAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(12914, 9, 'projectAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12915, 10, 'projectAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12916, 11, 'projectAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12917, 9, 'introAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12918, 10, 'introAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12919, 11, 'introAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12920, 9, 'recruitAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12921, 10, 'recruitAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12922, 11, 'recruitAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12923, 9, 'recruitsearchAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12924, 10, 'recruitsearchAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12925, 11, 'recruitsearchAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12926, 9, 'stockAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12927, 10, 'stockAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12928, 11, 'stockAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(12932, 11, 'typeAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(12953, 9, 'tuyendungnopdonAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(12954, 10, 'tuyendungnopdonAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(12955, 11, 'tuyendungnopdonAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(12995, 9, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\IndexController', 'Zfloraanhdao'),
(12996, 10, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\IndexController', 'Zfloraanhdao'),
(12997, 11, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\IndexController', 'Zfloraanhdao'),
(12998, 9, 'contactAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(12999, 10, 'contactAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(13000, 11, 'contactAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(13001, 9, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(13002, 10, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(13003, 11, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\ContactController', 'Zfloraanhdao'),
(13004, 9, 'aboutAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13005, 10, 'aboutAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13006, 11, 'aboutAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13007, 9, 'projectAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13008, 10, 'projectAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13009, 11, 'projectAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13010, 9, 'newsAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13011, 10, 'newsAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13012, 11, 'newsAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13013, 9, 'supportAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13014, 10, 'supportAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13015, 11, 'supportAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13016, 9, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13017, 10, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13018, 11, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\PostController', 'Zfloraanhdao'),
(13019, 9, 'apartmentAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(13020, 10, 'apartmentAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(13021, 11, 'apartmentAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(13022, 11, 'indexAction', 'Zfloraanhdao\\Front\\Controller\\PagesController', 'Zfloraanhdao'),
(13023, 9, 'indexAction', 'Zehome\\Front\\Controller\\IndexController', 'Zehome'),
(13024, 10, 'indexAction', 'Zehome\\Front\\Controller\\IndexController', 'Zehome'),
(13025, 11, 'indexAction', 'Zehome\\Front\\Controller\\IndexController', 'Zehome'),
(13026, 9, 'contactAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13027, 10, 'contactAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13028, 11, 'contactAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13029, 9, 'indexAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13030, 10, 'indexAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13031, 11, 'indexAction', 'Zehome\\Front\\Controller\\ContactController', 'Zehome'),
(13032, 9, 'aboutAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13033, 10, 'aboutAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13034, 11, 'aboutAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13035, 9, 'projectAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13036, 10, 'projectAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13037, 11, 'projectAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13038, 9, 'newsAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13039, 10, 'newsAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13040, 11, 'newsAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13041, 9, 'supportAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13042, 10, 'supportAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13043, 11, 'supportAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13044, 9, 'indexAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13045, 10, 'indexAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13046, 11, 'indexAction', 'Zehome\\Front\\Controller\\PostController', 'Zehome'),
(13047, 9, 'apartmentAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13048, 10, 'apartmentAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13049, 11, 'apartmentAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13050, 9, 'indexAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13051, 10, 'indexAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13052, 11, 'indexAction', 'Zehome\\Front\\Controller\\PagesController', 'Zehome'),
(13053, 9, 'indexAction', 'Znamlonghome\\Front\\Controller\\IndexController', 'Znamlonghome'),
(13054, 10, 'indexAction', 'Znamlonghome\\Front\\Controller\\IndexController', 'Znamlonghome'),
(13055, 11, 'indexAction', 'Znamlonghome\\Front\\Controller\\IndexController', 'Znamlonghome'),
(13056, 9, 'contactAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13057, 10, 'contactAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13058, 11, 'contactAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13059, 9, 'indexAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13060, 10, 'indexAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13061, 11, 'indexAction', 'Znamlonghome\\Front\\Controller\\ContactController', 'Znamlonghome'),
(13062, 9, 'aboutAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13063, 10, 'aboutAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13064, 11, 'aboutAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13065, 9, 'projectAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13066, 10, 'projectAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13067, 11, 'projectAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13068, 9, 'newsAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13069, 10, 'newsAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13070, 11, 'newsAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13071, 9, 'supportAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13072, 10, 'supportAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13073, 11, 'supportAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13074, 9, 'indexAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13075, 10, 'indexAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13076, 11, 'indexAction', 'Znamlonghome\\Front\\Controller\\PostController', 'Znamlonghome'),
(13077, 9, 'apartmentAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13078, 10, 'apartmentAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13079, 11, 'apartmentAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13080, 9, 'indexAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13081, 10, 'indexAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13082, 11, 'indexAction', 'Znamlonghome\\Front\\Controller\\PagesController', 'Znamlonghome'),
(13418, 11, 'changetemplateAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(13431, 11, 'typeAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13432, 11, 'addAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13433, 11, 'editAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13434, 11, 'indexAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13435, 11, 'publishAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13436, 11, 'unpublishAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13437, 11, 'deleteitemAction', 'Block\\Admin\\Controller\\BlockController', 'Block'),
(13438, 11, 'addAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(13439, 11, 'editAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(13440, 11, 'indexAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(13441, 11, 'publishAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(13442, 11, 'unpublishAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(13443, 11, 'deleteitemAction', 'Extension\\Admin\\Controller\\ExtensionController', 'Extension'),
(14316, 9, 'registersuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14317, 11, 'registersuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14318, 9, 'activeAction', 'User\\Front\\Controller\\UserController', 'User'),
(14319, 11, 'activeAction', 'User\\Front\\Controller\\UserController', 'User'),
(14320, 9, 'activesuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14321, 11, 'activesuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14322, 9, 'activesmssuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14323, 11, 'activesmssuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14324, 9, 'resendsmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(14325, 11, 'resendsmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(14326, 9, 'forgotAction', 'User\\Front\\Controller\\UserController', 'User'),
(14327, 11, 'forgotAction', 'User\\Front\\Controller\\UserController', 'User'),
(14328, 9, 'forgotsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14329, 11, 'forgotsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14332, 9, 'recoversuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14333, 11, 'recoversuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14337, 9, 'facebookloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(14338, 11, 'facebookloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(14339, 9, 'googleloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(14340, 11, 'googleloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(14344, 10, 'changepasswordAction', 'User\\Front\\Controller\\UserController', 'User'),
(14345, 11, 'changepasswordAction', 'User\\Front\\Controller\\UserController', 'User'),
(14346, 10, 'changepasswordsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14347, 11, 'changepasswordsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14348, 10, 'profileAction', 'User\\Front\\Controller\\UserController', 'User'),
(14349, 11, 'profileAction', 'User\\Front\\Controller\\UserController', 'User'),
(14350, 10, 'accountAction', 'User\\Front\\Controller\\UserController', 'User'),
(14351, 11, 'accountAction', 'User\\Front\\Controller\\UserController', 'User'),
(14352, 10, 'editAction', 'User\\Front\\Controller\\UserController', 'User'),
(14353, 11, 'editAction', 'User\\Front\\Controller\\UserController', 'User'),
(14354, 10, 'editsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14355, 11, 'editsuccessAction', 'User\\Front\\Controller\\UserController', 'User'),
(14356, 10, 'changepictureAction', 'User\\Front\\Controller\\UserController', 'User'),
(14357, 11, 'changepictureAction', 'User\\Front\\Controller\\UserController', 'User'),
(14358, 10, 'settingAction', 'User\\Front\\Controller\\UserController', 'User'),
(14359, 11, 'settingAction', 'User\\Front\\Controller\\UserController', 'User'),
(15118, 9, 'shareAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(15119, 10, 'shareAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(15120, 11, 'shareAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(15277, 9, 'getcaptchaAction', 'User\\Admin\\Controller\\UserController', 'User'),
(15278, 10, 'getcaptchaAction', 'User\\Admin\\Controller\\UserController', 'User'),
(15279, 11, 'getcaptchaAction', 'User\\Admin\\Controller\\UserController', 'User'),
(15685, 11, 'addAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15686, 11, 'editAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15687, 11, 'indexAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15688, 11, 'publishAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15689, 11, 'unpublishAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15690, 11, 'deleteitemAction', 'Subscribe\\Admin\\Controller\\SubscribeController', 'Subscribe'),
(15770, 11, 'backupdbAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(16252, 9, 'ruleAction', 'Api\\Controller\\PagesController', 'Api'),
(16253, 10, 'ruleAction', 'Api\\Controller\\PagesController', 'Api'),
(16254, 11, 'ruleAction', 'Api\\Controller\\PagesController', 'Api'),
(16255, 9, 'indexAction', 'Api\\Controller\\PagesController', 'Api'),
(16256, 10, 'indexAction', 'Api\\Controller\\PagesController', 'Api'),
(16257, 11, 'indexAction', 'Api\\Controller\\PagesController', 'Api'),
(16495, 11, 'downloadAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(16680, 9, 'newsviewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(16681, 10, 'newsviewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(16682, 11, 'newsviewAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(19441, 9, 'apiCompanyAction', 'User\\Admin\\Controller\\UserController', 'User'),
(19442, 10, 'apiCompanyAction', 'User\\Admin\\Controller\\UserController', 'User'),
(19443, 11, 'apiCompanyAction', 'User\\Admin\\Controller\\UserController', 'User'),
(19444, 9, 'apiCustomerAction', 'User\\Admin\\Controller\\UserController', 'User'),
(19445, 10, 'apiCustomerAction', 'User\\Admin\\Controller\\UserController', 'User'),
(19446, 11, 'apiCustomerAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20199, 9, 'indexAction', 'Core\\Controller\\CoreController', 'Core'),
(20200, 10, 'indexAction', 'Core\\Controller\\CoreController', 'Core'),
(20201, 11, 'indexAction', 'Core\\Controller\\CoreController', 'Core'),
(20202, 9, 'indexAction', 'Application\\Controller\\IndexController', 'Application'),
(20203, 10, 'indexAction', 'Application\\Controller\\IndexController', 'Application'),
(20204, 11, 'indexAction', 'Application\\Controller\\IndexController', 'Application'),
(20205, 9, 'translatelangAction', 'Application\\Controller\\IndexController', 'Application'),
(20206, 10, 'translatelangAction', 'Application\\Controller\\IndexController', 'Application'),
(20207, 11, 'translatelangAction', 'Application\\Controller\\IndexController', 'Application'),
(20208, 9, 'indexAction', 'Category\\Front\\Controller\\CategoryController', 'Category'),
(20209, 11, 'indexAction', 'Category\\Front\\Controller\\CategoryController', 'Category'),
(20210, 11, 'indexAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20211, 11, 'apicategoryAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20212, 11, 'addAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20213, 11, 'editAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20214, 11, 'publishAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20215, 11, 'unpublishAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20216, 11, 'deleteitemAction', 'Category\\Admin\\Controller\\CategoryController', 'Category'),
(20217, 9, 'contactAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20218, 10, 'contactAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20219, 11, 'contactAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20220, 9, 'contactsuccessAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20221, 10, 'contactsuccessAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20222, 11, 'contactsuccessAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20223, 9, 'indexAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20224, 10, 'indexAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20225, 11, 'indexAction', 'Contact\\Front\\Controller\\ContactController', 'Contact'),
(20226, 11, 'addAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20227, 11, 'editAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20228, 11, 'indexAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20229, 11, 'publishAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20230, 11, 'unpublishAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20231, 11, 'deleteitemAction', 'Contact\\Admin\\Controller\\ContactController', 'Contact'),
(20232, 9, 'newsAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20233, 10, 'newsAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20234, 11, 'newsAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20235, 9, 'saleAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20236, 10, 'saleAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20237, 11, 'saleAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20238, 9, 'exchangegiftAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20239, 10, 'exchangegiftAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20240, 11, 'exchangegiftAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20241, 9, 'apiProductAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20242, 10, 'apiProductAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20243, 11, 'apiProductAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20244, 9, 'indexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20245, 10, 'indexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20246, 11, 'indexAction', 'Content\\Front\\Controller\\PostsController', 'Content'),
(20247, 9, 'indexAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(20248, 10, 'indexAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(20249, 11, 'indexAction', 'Content\\Front\\Controller\\PagesController', 'Content'),
(20250, 11, 'editAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20251, 9, 'apiProductAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20252, 10, 'apiProductAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20253, 11, 'apiProductAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20254, 9, 'orderAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20255, 10, 'orderAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20256, 11, 'orderAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20257, 9, 'exchangeAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20258, 10, 'exchangeAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20259, 11, 'exchangeAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20260, 9, 'getgiftAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20261, 10, 'getgiftAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20262, 11, 'getgiftAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20263, 11, 'addAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20264, 11, 'indexAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20265, 11, 'publishAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20266, 11, 'unpublishAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20267, 11, 'deleteitemAction', 'Content\\Admin\\Controller\\PostController', 'Content'),
(20268, 11, 'addAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20269, 11, 'editAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20270, 11, 'indexAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20271, 11, 'publishAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20272, 11, 'unpublishAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20273, 11, 'deleteitemAction', 'Contenttype\\Admin\\Controller\\ContenttypeController', 'Contenttype'),
(20293, 11, 'cpanelAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20294, 9, 'adminsecureauthenAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20295, 10, 'adminsecureauthenAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20296, 11, 'adminsecureauthenAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20297, 11, 'addAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20298, 11, 'editAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20299, 11, 'indexAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20300, 11, 'publishAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20301, 11, 'unpublishAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20302, 11, 'deleteitemAction', 'Cpanel\\Admin\\Controller\\CpanelController', 'Cpanel'),
(20303, 11, 'addAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20304, 11, 'editAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20305, 11, 'indexAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20306, 11, 'publishAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20307, 11, 'unpublishAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20308, 11, 'deleteitemAction', 'Language\\Admin\\Controller\\LanguageController', 'Language'),
(20309, 11, 'indexAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20310, 11, 'filemanAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20311, 11, 'viewAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20312, 11, 'editfileAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20313, 11, 'savefileAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20314, 11, 'addAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20315, 11, 'editAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20316, 11, 'publishAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20317, 11, 'unpublishAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20318, 11, 'deleteitemAction', 'Setting\\Admin\\Controller\\EmailSettingController', 'Setting'),
(20319, 11, 'infoAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20320, 11, 'addAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20321, 11, 'editAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20322, 11, 'indexAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20323, 11, 'publishAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20324, 11, 'unpublishAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20325, 11, 'deleteitemAction', 'Setting\\Admin\\Controller\\SettingController', 'Setting'),
(20326, 11, 'addAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20327, 11, 'editAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20328, 11, 'indexAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20329, 11, 'publishAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20330, 11, 'unpublishAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20331, 11, 'deleteitemAction', 'Setting\\Admin\\Controller\\RedirectController', 'Setting'),
(20332, 9, 'loginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20333, 10, 'loginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20334, 11, 'loginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20335, 9, 'registerAction', 'User\\Front\\Controller\\UserController', 'User'),
(20336, 11, 'registerAction', 'User\\Front\\Controller\\UserController', 'User'),
(20337, 9, 'apiCustomerAction', 'User\\Front\\Controller\\UserController', 'User'),
(20338, 10, 'apiCustomerAction', 'User\\Front\\Controller\\UserController', 'User'),
(20339, 11, 'apiCustomerAction', 'User\\Front\\Controller\\UserController', 'User'),
(20340, 9, 'apiEmployeeAction', 'User\\Front\\Controller\\UserController', 'User'),
(20341, 10, 'apiEmployeeAction', 'User\\Front\\Controller\\UserController', 'User'),
(20342, 11, 'apiEmployeeAction', 'User\\Front\\Controller\\UserController', 'User'),
(20343, 9, 'apiregisterAction', 'User\\Front\\Controller\\UserController', 'User'),
(20344, 10, 'apiregisterAction', 'User\\Front\\Controller\\UserController', 'User'),
(20345, 11, 'apiregisterAction', 'User\\Front\\Controller\\UserController', 'User'),
(20346, 9, 'apiverifysmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20347, 10, 'apiverifysmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20348, 11, 'apiverifysmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20349, 9, 'apiverifyresendsmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20350, 10, 'apiverifyresendsmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20351, 11, 'apiverifyresendsmsAction', 'User\\Front\\Controller\\UserController', 'User'),
(20352, 9, 'apiloginfbAction', 'User\\Front\\Controller\\UserController', 'User'),
(20353, 10, 'apiloginfbAction', 'User\\Front\\Controller\\UserController', 'User'),
(20354, 11, 'apiloginfbAction', 'User\\Front\\Controller\\UserController', 'User'),
(20355, 9, 'apiloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20356, 10, 'apiloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20357, 11, 'apiloginAction', 'User\\Front\\Controller\\UserController', 'User'),
(20358, 9, 'apilogoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20359, 10, 'apilogoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20360, 11, 'apilogoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20361, 9, 'logoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20362, 10, 'logoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20363, 11, 'logoutAction', 'User\\Front\\Controller\\UserController', 'User'),
(20364, 9, 'apiprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20365, 10, 'apiprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20366, 11, 'apiprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20367, 9, 'apiupdateprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20368, 10, 'apiupdateprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20369, 11, 'apiupdateprofileAction', 'User\\Front\\Controller\\UserController', 'User'),
(20370, 9, 'forgetpassAction', 'User\\Front\\Controller\\UserController', 'User'),
(20371, 10, 'forgetpassAction', 'User\\Front\\Controller\\UserController', 'User'),
(20372, 11, 'forgetpassAction', 'User\\Front\\Controller\\UserController', 'User'),
(20373, 9, 'recoverAction', 'User\\Front\\Controller\\UserController', 'User'),
(20374, 10, 'recoverAction', 'User\\Front\\Controller\\UserController', 'User'),
(20375, 11, 'recoverAction', 'User\\Front\\Controller\\UserController', 'User'),
(20376, 9, 'indexAction', 'User\\Front\\Controller\\UserController', 'User'),
(20377, 10, 'indexAction', 'User\\Front\\Controller\\UserController', 'User'),
(20378, 11, 'indexAction', 'User\\Front\\Controller\\UserController', 'User'),
(20379, 11, 'exportAction', 'User\\Admin\\Controller\\UserController', 'User');
INSERT INTO `bz1_groups_permission` (`id`, `group_id`, `permission`, `controller`, `module`) VALUES
(20380, 11, 'addAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20381, 11, 'editAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20382, 9, 'loginAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20383, 10, 'loginAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20384, 11, 'loginAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20385, 9, 'logoutAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20386, 10, 'logoutAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20387, 11, 'logoutAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20388, 11, 'accountAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20389, 9, 'apiEmployeeAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20390, 10, 'apiEmployeeAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20391, 11, 'apiEmployeeAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20392, 11, 'indexAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20393, 11, 'publishAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20394, 11, 'unpublishAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20395, 11, 'deleteitemAction', 'User\\Admin\\Controller\\UserController', 'User'),
(20396, 11, 'addAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20397, 11, 'editAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20398, 11, 'indexAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20399, 11, 'publishAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20400, 11, 'unpublishAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20401, 11, 'deleteitemAction', 'User\\Admin\\Controller\\GroupController', 'User'),
(20402, 11, 'indexAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20403, 11, 'addAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20404, 11, 'editAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20405, 11, 'publishAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20406, 11, 'unpublishAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20407, 11, 'deleteitemAction', 'User\\Admin\\Controller\\PermissionController', 'User'),
(20408, 9, 'voteAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20409, 10, 'voteAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20410, 11, 'voteAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20411, 9, 'indexAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20412, 10, 'indexAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20413, 11, 'indexAction', 'Vote\\Front\\Controller\\VoteController', 'Vote'),
(20414, 11, 'exportAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20415, 11, 'addAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20416, 11, 'editAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20417, 11, 'indexAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20418, 11, 'publishAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20419, 11, 'unpublishAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20420, 11, 'deleteitemAction', 'Vote\\Admin\\Controller\\VoteController', 'Vote'),
(20427, 11, 'addAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20428, 11, 'editAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20429, 11, 'apiCompanyAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20430, 11, 'apiCustomerAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20431, 11, 'apiAddCustomerAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20432, 11, 'exportAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20433, 11, 'indexAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20434, 11, 'publishAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20435, 11, 'unpublishAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer'),
(20436, 11, 'deleteitemAction', 'Customer\\Admin\\Controller\\CustomerController', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `bz1_languages`
--

CREATE TABLE `bz1_languages` (
  `id` int(11) UNSIGNED NOT NULL,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT '',
  `description` varchar(512) DEFAULT '',
  `status` tinyint(1) DEFAULT '1',
  `ordering` int(11) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bz1_languages`
--

INSERT INTO `bz1_languages` (`id`, `lang_code`, `title`, `slug`, `image`, `description`, `status`, `ordering`, `is_default`, `created`, `modified`) VALUES
(3, 'vi_VN', 'VI', 'VI', '', 'Tiếng Việt', 0, 0, 1, '2011-11-02 09:07:34', '2011-11-02 09:07:34'),
(4, 'en_US', 'EN', 'en', '', 'English', 0, 0, 0, '2011-11-02 09:07:34', NULL),
(5, 'ja_JP', 'JP', 'jp', '', 'Tiếng Nhật', 0, 3, 0, '2015-12-04 00:04:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_metatags`
--

CREATE TABLE `bz1_metatags` (
  `id` int(11) NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `object_id` int(11) NOT NULL DEFAULT '0',
  `extension` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bz1_metatags`
--

INSERT INTO `bz1_metatags` (`id`, `meta_title`, `meta_description`, `meta_keywords`, `object_id`, `extension`) VALUES
(2, 'page title - update', 'page description - update', 'asda dadasd -update', 100, NULL),
(3, '', '', '', 6, 'page'),
(4, '', '', '', 7, 'page'),
(5, 'Page Title', 'Meta description', 'meta keyword', 7, NULL),
(6, 'Support Page Title', 'Support Meta description', 'Support meta keywords', 8, 'page'),
(7, 'Help Page Title', 'Help meta description', 'Help meta keywords', 9, 'page'),
(8, '', '', '', 10, 'page'),
(15, '', '', '', 12, NULL),
(17, '', '', '', 13, 'post'),
(18, '', '', '', 14, 'post'),
(19, '', '', '', 15, 'post'),
(20, '', '', '', 16, 'post'),
(22, '', '', '', 17, 'post'),
(23, '', '', '', 18, 'page'),
(25, '', '', '', 19, 'page'),
(27, '', '', '', 20, 'page'),
(28, '', '', '', 21, 'page'),
(29, '', '', '', 11, 'post'),
(30, '', '', '', 22, 'page'),
(33, '', '', '', 12, 'post'),
(34, '', '', '', 23, 'post'),
(35, '', '', '', 24, 'captcha'),
(37, '', '', '', 25, 'post'),
(40, '', '', '', 26, 'post'),
(53, '', '', '', 2, 'post'),
(55, '', '', '', 1, 'post'),
(61, '', '', '', 5, 'news'),
(62, '', '', '', 6, 'news'),
(63, '', '', '', 7, 'news'),
(64, '', '', '', 8, 'news'),
(65, '', '', '', 9, 'post'),
(66, '', '', '', 10, 'post'),
(68, '', '', '', 12, 'banner'),
(69, '', '', '', 11, 'banner'),
(76, '', '', '', 13, 'partner'),
(77, '', '', '', 14, 'partner'),
(78, '', '', '', 15, 'partner'),
(79, '', '', '', 16, 'partner'),
(84, 'Test thôi nhé', 'Không có gì phải không', 'hehehe', 15, 'news'),
(94, '', '', '', 16, 'news'),
(95, '', '', '', 17, 'news'),
(96, '', '', '', 18, 'news'),
(97, '', '', '', 19, 'news'),
(100, '', '', '', 20, 'news'),
(101, '', '', '', 21, 'news'),
(102, '', '', '', 22, 'news'),
(103, '', '', '', 23, 'news'),
(105, '', '', '', 1, 'news'),
(106, '', '', '', 2, 'news'),
(107, '', '', '', 3, 'news'),
(115, '', '', '', 4, 'news'),
(131, '', '', '', 5, 'content_news'),
(148, '', '', '', 7, 'content_product'),
(149, '', '', '', 8, 'content_product'),
(153, '', '', '', 6, 'content_gift'),
(156, '', '', '', 21, 'content_voucher'),
(158, '', '', '', 24, 'content_voucher');

-- --------------------------------------------------------

--
-- Table structure for table `bz1_posts`
--

CREATE TABLE `bz1_posts` (
  `id` bigint(19) UNSIGNED NOT NULL,
  `identity` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subtitle` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `body` longtext COLLATE utf8_unicode_ci,
  `intro` longtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT '0',
  `image` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `large_image` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `multi_image` longtext COLLATE utf8_unicode_ci,
  `files` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `lang_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` longtext COLLATE utf8_unicode_ci,
  `subtype` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `score` int(11) DEFAULT NULL,
  `inventory` int(11) DEFAULT NULL,
  `eco_price` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `pro_status` int(11) DEFAULT '1',
  `product_brand` int(11) DEFAULT NULL,
  `customerId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `voucherName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voucherId` int(11) NOT NULL,
  `voucherScore` int(11) NOT NULL,
  `getgift` int(11) NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `featured` tinyint(4) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `category` int(11) DEFAULT '0',
  `type` varchar(45) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_posts_metas`
--

CREATE TABLE `bz1_posts_metas` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` text COLLATE utf8_unicode_ci,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_posts_tags`
--

CREATE TABLE `bz1_posts_tags` (
  `id` int(15) NOT NULL,
  `post_id` int(15) NOT NULL,
  `tag_id` int(15) NOT NULL,
  `extension` varchar(150) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_posts_terms`
--

CREATE TABLE `bz1_posts_terms` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_posts_terms`
--

INSERT INTO `bz1_posts_terms` (`id`, `post_id`, `term_id`, `extension`) VALUES
(1, 2, 2, 'post'),
(3, 1, 1, 'post'),
(13, 54, 14, 'post'),
(60, 52, 9, 'post'),
(63, 53, 9, 'post'),
(65, 144, 9, 'post'),
(68, 145, 9, 'post'),
(70, 146, 9, 'post'),
(73, 147, 9, 'post'),
(75, 217, 9, 'post'),
(77, 218, 12, 'post'),
(78, 219, 12, 'post'),
(92, 220, 10, 'post'),
(95, 221, 11, 'post'),
(101, 222, 13, 'post'),
(107, 223, 13, 'post'),
(109, 7, 4, 'post'),
(111, 8, 4, 'post'),
(112, 30, 7, 'post'),
(113, 33, 11, 'post'),
(114, 34, 11, 'post');

-- --------------------------------------------------------

--
-- Table structure for table `bz1_provinces`
--

CREATE TABLE `bz1_provinces` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bz1_provinces`
--

INSERT INTO `bz1_provinces` (`id`, `title`, `area`, `status`) VALUES
(1, 'TP Hồ Chí Minh', 'vn', 1),
(2, 'Hà Nội', 'vn', 1),
(3, 'Hải Phòng', 'vn', 1),
(4, 'Đà Nẵng', 'vn', 1),
(5, 'Bình Dương', 'vn', 1),
(6, 'Đồng Nai', 'vn', 1),
(7, 'An Giang', 'vn', 1),
(8, 'Bà Rịa Vũng Tàu', 'vn', 1),
(9, 'Bình Phước', 'vn', 1),
(10, 'Bình Thuận', 'vn', 1),
(11, 'Bình Định', 'vn', 1),
(12, 'Bạc Liêu', 'vn', 1),
(13, 'Bắc Giang', 'vn', 1),
(14, 'Bắc Kạn', 'vn', 1),
(15, 'Bắc Ninh', 'vn', 1),
(16, 'Bến Tre', 'vn', 1),
(17, 'Cao Bằng', 'vn', 1),
(18, 'Cà Mau', 'vn', 1),
(19, 'Cần Thơ', 'vn', 1),
(20, 'Gia Lai', 'vn', 1),
(21, 'Hoà Bình', 'vn', 1),
(22, 'Hà Giang', 'vn', 1),
(23, 'Hà Nam', 'vn', 1),
(24, 'Hà Tĩnh', 'vn', 1),
(25, 'Hưng Yên', 'vn', 1),
(26, 'Hải Dương', 'vn', 1),
(27, 'Hậu Giang', 'vn', 1),
(28, 'Khánh Hòa', 'vn', 1),
(29, 'Kiên Giang', 'vn', 1),
(30, 'Kon Tum', 'vn', 1),
(31, 'Lai Châu', 'vn', 1),
(32, 'Long An', 'vn', 1),
(33, 'Lào Cai', 'vn', 1),
(34, 'Lâm Đồng', 'vn', 1),
(35, 'Lạng Sơn', 'vn', 1),
(36, 'Nam Định', 'vn', 1),
(37, 'Nghệ An', 'vn', 1),
(38, 'Ninh Bình', 'vn', 1),
(39, 'Ninh Thuận', 'vn', 1),
(40, 'Phú Thọ', 'vn', 1),
(41, 'Phú Yên', 'vn', 1),
(42, 'Quảng Bình', 'vn', 1),
(43, 'Quảng Nam', 'vn', 1),
(44, 'Quảng Ngãi', 'vn', 1),
(45, 'Quảng Ninh', 'vn', 1),
(46, 'Quảng Trị', 'vn', 1),
(47, 'Sóc Trăng', 'vn', 1),
(48, 'Sơn La', 'vn', 1),
(49, 'Thanh Hóa', 'vn', 1),
(50, 'Thái Bình', 'vn', 1),
(51, 'Thái Nguyên', 'vn', 1),
(52, 'Huế', 'vn', 1),
(53, 'Tiền Giang', 'vn', 1),
(54, 'Trà Vinh', 'vn', 1),
(55, 'Tuyên Quang', 'vn', 1),
(56, 'Tây Ninh', 'vn', 1),
(57, 'Vĩnh Long', 'vn', 1),
(58, 'Vĩnh Phúc', 'vn', 1),
(59, 'Yên Bái', 'vn', 1),
(60, 'Điện Biên', 'vn', 1),
(61, 'Đắk Lắk', 'vn', 1),
(62, 'Đắk Nông', 'vn', 1),
(63, 'Đồng Tháp', 'vn', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_redirects`
--

CREATE TABLE `bz1_redirects` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL,
  `dst` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `header` varchar(500) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `language` varchar(20) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_settings`
--

CREATE TABLE `bz1_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `module` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_settings`
--

INSERT INTO `bz1_settings` (`id`, `name`, `value`, `desc`, `module`, `group`) VALUES
(15, 'sitestatus', '1', '', 'core', 'core'),
(18, 'ga', 'var ga=12;', '', 'core', 'core'),
(26, 'sitename', 'BZ CMS Custom', '', 'core', 'core'),
(28, 'metakeyword', 'keyword', '', 'core', 'core'),
(29, 'metadescription', 'description', '', 'core', 'core');

-- --------------------------------------------------------

--
-- Table structure for table `bz1_templates`
--

CREATE TABLE `bz1_templates` (
  `id` int(11) NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `params` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('Administrator','Frontend','Facebook') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_templates`
--

INSERT INTO `bz1_templates` (`id`, `template`, `title`, `status`, `params`, `type`) VALUES
(1, 'mytheme', 'Default Administrator', 1, '', 'Administrator'),
(2, 'mytheme', 'Default Frontend', 1, '', 'Frontend');

-- --------------------------------------------------------

--
-- Table structure for table `bz1_terms`
--

CREATE TABLE `bz1_terms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `intro` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci,
  `vocabulary_id` int(11) DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `lang_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_users`
--

CREATE TABLE `bz1_users` (
  `id` int(11) NOT NULL,
  `token` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `company_id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` enum('Male','Female','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date NOT NULL,
  `birthday` date DEFAULT NULL,
  `identify` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `province_id` int(11) DEFAULT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mobile_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `forget_pass_code` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `social_type` enum('Normal','Yahoo','Google','Facebook','Twitter') COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_token` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `social_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `group_role` int(11) NOT NULL,
  `side` text COLLATE utf8_unicode_ci,
  `is_updated_info` tinyint(4) DEFAULT '0',
  `ip` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_users`
--

INSERT INTO `bz1_users` (`id`, `token`, `username`, `password`, `email`, `first_name`, `last_name`, `name`, `address`, `phone`, `company_id`, `position`, `avatar`, `gender`, `dob`, `birthday`, `identify`, `province_id`, `activation_code`, `mobile_code`, `forget_pass_code`, `created`, `modified`, `social_id`, `social_type`, `social_token`, `social_link`, `social_picture`, `role`, `group_role`, `side`, `is_updated_info`, `ip`, `type`, `status`) VALUES
(19, '12345abc@123', '', 'cd0c7a1a20ebc66bbf498d660e6eb6798e154b0171b2cd5bc', 'admin@bizzon.com.vn', '', '', 'Admin', '', '', 0, '', '', NULL, '0000-00-00', NULL, '', NULL, '', '', NULL, '2014-12-12 07:34:23', NULL, '', NULL, '', NULL, NULL, 11, 1, NULL, 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_users_groups`
--

CREATE TABLE `bz1_users_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bz1_users_login_log`
--

CREATE TABLE `bz1_users_login_log` (
  `id` bigint(19) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `type` enum('fail','success') COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_users_login_log`
--

INSERT INTO `bz1_users_login_log` (`id`, `title`, `ip`, `created`, `type`, `status`) VALUES
(1, 'Login fail!', '::1', '2016-03-25 17:36:18', 'fail', 0),
(2, 'Login fail!', '::1', '2016-03-25 17:36:20', 'fail', 0),
(3, 'Login fail!', '::1', '2016-03-25 17:36:21', 'fail', 0),
(4, 'Login fail!', '::1', '2016-03-25 17:36:23', 'fail', 0),
(5, 'Login fail!', '::1', '2016-03-25 17:36:24', 'fail', 0),
(6, 'Login fail!', '::1', '2016-03-25 17:37:25', 'fail', 0),
(7, 'Login fail!', '::1', '2016-03-25 17:37:27', 'fail', 0),
(8, 'Login success!', '::1', '2016-03-25 17:37:37', 'success', 1),
(9, 'Login fail!', '::1', '2016-03-25 17:37:54', 'fail', 0),
(10, 'Login fail!', '::1', '2016-03-25 17:37:56', 'fail', 0),
(11, 'Login fail!', '::1', '2016-03-25 17:42:40', 'fail', 0),
(12, 'Login success!', '::1', '2016-03-25 17:42:45', 'success', 1),
(13, 'Login fail!', '::1', '2016-03-25 19:31:14', 'fail', 0),
(14, 'Login fail!', '::1', '2016-03-25 19:31:17', 'fail', 0),
(15, 'Login fail!', '::1', '2016-03-25 19:31:18', 'fail', 0),
(16, 'Login fail!', '::1', '2016-03-25 19:31:20', 'fail', 0),
(17, 'Login fail!', '::1', '2016-03-25 19:31:22', 'fail', 0),
(18, 'Login fail!', '::1', '2016-03-25 19:32:22', 'fail', 0),
(19, 'Login fail!', '::1', '2016-03-25 19:32:23', 'fail', 0),
(20, 'Login success!', '::1', '2016-03-25 19:32:29', 'success', 1),
(21, 'Login success!', '::1', '2016-03-27 19:54:56', 'success', 1),
(22, 'Login success!', '::1', '2016-03-28 11:20:47', 'success', 1),
(23, 'Login fail!', '::1', '2016-03-28 11:20:52', 'fail', 0),
(24, 'Login fail!', '::1', '2016-03-28 11:20:53', 'fail', 0),
(25, 'Login fail!', '::1', '2016-03-28 11:20:56', 'fail', 0),
(26, 'Login fail!', '::1', '2016-03-28 11:20:57', 'fail', 0),
(27, 'Login fail!', '::1', '2016-03-28 11:20:58', 'fail', 0),
(28, 'Login fail!', '::1', '2016-03-28 11:23:06', 'fail', 0),
(29, 'Login success!', '::1', '2016-03-28 11:23:15', 'success', 1),
(30, 'Login success!', '::1', '2016-03-30 16:54:47', 'success', 1),
(31, 'Login success!', '::1', '2016-04-07 09:32:10', 'success', 1),
(32, 'Login success!', '::1', '2016-04-07 09:35:41', 'success', 1),
(33, 'Login success!', '::1', '2016-04-07 09:36:40', 'success', 1),
(34, 'Login success!', '::1', '2016-04-07 09:37:27', 'success', 1),
(35, 'Login fail!', '::1', '2016-04-07 09:37:33', 'fail', 0),
(36, 'Login fail!', '::1', '2016-04-07 09:37:34', 'fail', 0),
(37, 'Login fail!', '::1', '2016-04-07 09:37:36', 'fail', 0),
(38, 'Login fail!', '::1', '2016-04-07 09:37:37', 'fail', 0),
(39, 'Login fail!', '::1', '2016-04-07 09:37:38', 'fail', 0),
(40, 'Login fail!', '::1', '2016-04-07 09:38:38', 'fail', 0),
(41, 'Login fail!', '::1', '2016-04-07 09:38:39', 'fail', 0),
(42, 'Login fail!', '::1', '2016-04-07 09:38:43', 'fail', 0),
(43, 'Login success!', '::1', '2016-04-07 09:38:49', 'success', 1),
(44, 'Login success!', '::1', '2016-04-07 09:52:50', 'success', 1),
(45, 'Login success!', '::1', '2016-04-07 09:56:29', 'success', 1),
(46, 'Login success!', '::1', '2016-04-07 10:05:07', 'success', 1),
(47, 'Login success!', '::1', '2016-04-07 10:16:52', 'success', 1),
(48, 'Login success!', '::1', '2016-04-07 10:17:23', 'success', 1),
(49, 'Login success!', '::1', '2016-04-07 11:49:44', 'success', 1),
(50, 'Login success!', '::1', '2016-04-07 11:54:22', 'success', 1),
(51, 'Login success!', '::1', '2016-04-07 11:56:20', 'success', 1),
(52, 'Login success!', '::1', '2016-04-07 14:28:57', 'success', 1),
(53, 'Login success!', '::1', '2016-04-07 14:37:59', 'success', 1),
(54, 'Login success!', '::1', '2016-04-11 11:30:38', 'success', 1),
(55, 'Login success!', '::1', '2016-04-11 11:32:30', 'success', 1),
(56, 'Login fail!', '::1', '2016-04-24 08:36:39', 'fail', 0),
(57, 'Login success!', '::1', '2016-04-24 08:36:47', 'success', 1),
(58, 'Login success!', '::1', '2016-04-29 08:05:03', 'success', 1),
(59, 'Login success!', '::1', '2016-04-29 14:46:37', 'success', 1),
(60, 'Login success!', '::1', '2016-05-23 11:55:41', 'success', 1),
(61, 'Login success!', '::1', '2016-06-02 08:59:22', 'success', 1),
(62, 'Login success!', '::1', '2016-07-05 15:42:46', 'success', 1),
(63, 'Login success!', '::1', '2016-07-08 08:16:57', 'success', 1),
(64, 'Login success!', '::1', '2016-07-26 10:05:53', 'success', 1),
(65, 'Login success!', '::1', '2016-07-26 10:05:57', 'success', 1),
(66, 'Login success!', '::1', '2016-07-26 10:07:22', 'success', 1),
(67, 'Login success!', '::1', '2016-07-26 10:18:14', 'success', 1),
(68, 'Login success!', '::1', '2016-07-26 10:18:37', 'success', 1),
(69, 'Login success!', '::1', '2016-08-16 16:19:17', 'success', 1),
(70, 'Login success!', '127.0.0.1', '2016-08-16 16:24:51', 'success', 1),
(71, 'Login success!', '127.0.0.1', '2016-08-16 18:50:17', 'success', 1),
(72, 'Login success!', '::1', '2016-08-17 10:50:47', 'success', 1),
(73, 'Login success!', '::1', '2016-08-19 08:28:44', 'success', 1),
(74, 'Login success!', '127.0.0.1', '2016-08-19 16:24:36', 'success', 1),
(75, 'Login success!', '127.0.0.1', '2016-08-24 15:02:02', 'success', 1),
(76, 'Login success!', '::1', '2016-08-31 16:40:32', 'success', 1),
(77, 'Login fail!', '::1', '2016-09-05 08:46:36', 'fail', 0),
(78, 'Login success!', '::1', '2016-09-05 08:47:01', 'success', 1),
(79, 'Login fail!', '127.0.0.1', '2016-09-07 17:26:54', 'fail', 0),
(80, 'Login fail!', '127.0.0.1', '2016-09-07 17:27:01', 'fail', 0),
(81, 'Login fail!', '127.0.0.1', '2016-09-07 17:27:08', 'fail', 0),
(82, 'Login fail!', '127.0.0.1', '2016-09-07 17:29:16', 'fail', 0),
(83, 'Login success!', '127.0.0.1', '2016-09-07 17:31:22', 'success', 1),
(84, 'Login success!', '127.0.0.1', '2016-09-08 15:40:10', 'success', 1),
(85, 'Login success!', '127.0.0.1', '2016-09-09 08:01:23', 'success', 1),
(86, 'Login fail!', '127.0.0.1', '2016-09-09 09:53:20', 'fail', 0),
(87, 'Login fail!', '127.0.0.1', '2016-09-09 09:53:32', 'fail', 0),
(88, 'Login fail!', '127.0.0.1', '2016-09-09 09:53:46', 'fail', 0),
(89, 'Login fail!', '127.0.0.1', '2016-09-09 09:53:54', 'fail', 0),
(90, 'Login fail!', '127.0.0.1', '2016-09-09 09:53:55', 'fail', 0),
(91, 'Login success!', '127.0.0.1', '2016-09-09 09:54:58', 'success', 1),
(92, 'Login success!', '127.0.0.1', '2016-09-09 09:55:07', 'success', 1),
(93, 'Login success!', '127.0.0.1', '2016-09-09 09:55:26', 'success', 1),
(94, 'Login success!', '127.0.0.1', '2016-09-09 09:55:33', 'success', 1),
(95, 'Login fail!', '127.0.0.1', '2016-09-09 10:03:50', 'fail', 0),
(96, 'Login fail!', '127.0.0.1', '2016-09-09 10:03:57', 'fail', 0),
(97, 'Login fail!', '127.0.0.1', '2016-09-09 10:04:06', 'fail', 0),
(98, 'Login fail!', '127.0.0.1', '2016-09-09 10:04:16', 'fail', 0),
(99, 'Login success!', '127.0.0.1', '2016-09-09 10:04:19', 'success', 1),
(100, 'Login success!', '127.0.0.1', '2016-09-09 10:04:24', 'success', 1),
(101, 'Login success!', '127.0.0.1', '2016-09-09 10:04:28', 'success', 1),
(102, 'Login success!', '::1', '2016-09-09 10:05:05', 'success', 1),
(103, 'Login success!', '127.0.0.1', '2016-09-09 10:05:20', 'success', 1),
(104, 'Login success!', '127.0.0.1', '2016-09-09 10:05:24', 'success', 1),
(105, 'Login success!', '127.0.0.1', '2016-09-09 10:15:09', 'success', 1),
(106, 'Login success!', '127.0.0.1', '2016-09-09 10:20:51', 'success', 1),
(107, 'Login success!', '127.0.0.1', '2016-09-09 10:22:03', 'success', 1),
(108, 'Login success!', '127.0.0.1', '2016-09-09 10:25:58', 'success', 1),
(109, 'Login success!', '::1', '2016-09-10 11:38:55', 'success', 1),
(110, 'Login success!', '::1', '2016-09-14 08:07:58', 'success', 1),
(111, 'Login success!', '::1', '2016-09-23 15:30:25', 'success', 1),
(112, 'Login success!', '::1', '2016-10-06 09:38:25', 'success', 1),
(113, 'Login success!', '::1', '2016-10-25 23:32:59', 'success', 1),
(114, 'Login success!', '::1', '2016-10-26 11:08:15', 'success', 1),
(115, 'Login success!', '::1', '2016-11-24 12:00:40', 'success', 1),
(116, 'Login success!', '::1', '2019-05-10 15:42:37', 'success', 1),
(117, 'Login fail!', '::1', '2019-05-10 22:35:05', 'fail', 0),
(118, 'Login success!', '::1', '2019-05-10 22:35:38', 'success', 1),
(119, 'Login fail!', '::1', '2019-05-11 08:12:18', 'fail', 0),
(120, 'Login success!', '::1', '2019-05-11 08:15:01', 'success', 1),
(121, 'Login success!', '::1', '2019-05-11 14:20:05', 'success', 1),
(122, 'Login success!', '::1', '2019-05-11 22:10:28', 'success', 1),
(123, 'Login success!', '::1', '2019-05-12 11:05:31', 'success', 1),
(124, 'Login success!', '::1', '2019-05-12 13:00:54', 'success', 1),
(125, 'Login success!', '::1', '2019-05-12 22:29:09', 'success', 1),
(126, 'Login success!', '::1', '2019-05-13 12:34:03', 'success', 1),
(127, 'Login fail!', '::1', '2019-05-14 08:27:36', 'fail', 0),
(128, 'Login success!', '::1', '2019-05-14 08:28:07', 'success', 1),
(129, 'Login success!', '::1', '2019-05-16 10:41:10', 'success', 1),
(130, 'Login fail!', '::1', '2019-05-16 10:54:26', 'fail', 0),
(131, 'Login success!', '::1', '2019-05-16 10:54:34', 'success', 1),
(132, 'Login success!', '::1', '2019-05-16 11:40:07', 'success', 1),
(133, 'Login fail!', '::1', '2019-06-07 08:10:59', 'fail', 0),
(134, 'Login success!', '::1', '2019-06-07 08:12:01', 'success', 1),
(135, 'Login success!', '::1', '2019-06-07 09:33:41', 'success', 1),
(136, 'Login success!', '::1', '2019-06-07 09:45:36', 'success', 1),
(137, 'Login success!', '::1', '2019-06-07 09:49:48', 'success', 1),
(138, 'Login success!', '::1', '2019-06-07 11:15:39', 'success', 1),
(139, 'Login success!', '::1', '2019-06-08 08:24:18', 'success', 1),
(140, 'Login success!', '::1', '2019-06-08 09:34:09', 'success', 1),
(141, 'Login success!', '::1', '2019-06-08 11:01:14', 'success', 1),
(142, 'Login success!', '::1', '2019-06-08 15:07:24', 'success', 1),
(143, 'Login success!', '::1', '2019-06-10 10:15:31', 'success', 1),
(144, 'Login success!', '::1', '2019-06-10 11:16:06', 'success', 1),
(145, 'Login success!', '::1', '2019-06-10 21:33:17', 'success', 1),
(146, 'Login success!', '::1', '2019-06-11 09:08:03', 'success', 1),
(147, 'Login success!', '::1', '2019-06-11 11:07:51', 'success', 1),
(148, 'Login success!', '::1', '2019-06-11 16:08:20', 'success', 1),
(149, 'Login success!', '::1', '2019-06-11 19:27:50', 'success', 1),
(150, 'Login fail!', '::1', '2019-06-12 11:05:50', 'fail', 0),
(151, 'Login success!', '::1', '2019-06-12 11:05:54', 'success', 1),
(152, 'Login success!', '::1', '2019-06-12 11:11:19', 'success', 1),
(153, 'Login success!', '::1', '2019-06-12 16:23:32', 'success', 1),
(154, 'Login success!', '::1', '2019-06-17 14:06:36', 'success', 1),
(155, 'Login success!', '::1', '2019-06-17 16:13:27', 'success', 1),
(156, 'Login success!', '::1', '2019-06-17 19:55:48', 'success', 1),
(157, 'Login success!', '::1', '2019-06-18 09:19:52', 'success', 1),
(158, 'Login fail!', '::1', '2019-06-18 10:11:49', 'fail', 0),
(159, 'Login success!', '::1', '2019-06-18 10:11:55', 'success', 1),
(160, 'Login fail!', '::1', '2019-06-18 16:30:16', 'fail', 0),
(161, 'Login fail!', '::1', '2019-06-18 16:30:20', 'fail', 0),
(162, 'Login success!', '::1', '2019-06-18 16:30:28', 'success', 1),
(163, 'Login success!', '::1', '2019-06-18 21:50:19', 'success', 1),
(164, 'Login success!', '::1', '2019-06-18 22:20:44', 'success', 1),
(165, 'Login success!', '::1', '2019-06-19 05:44:12', 'success', 1),
(166, 'Login success!', '::1', '2019-06-19 08:25:56', 'success', 1),
(167, 'Login success!', '::1', '2019-06-19 12:34:58', 'success', 1),
(168, 'Login success!', '::1', '2019-06-19 15:15:03', 'success', 1),
(169, 'Login success!', '::1', '2019-06-19 23:06:46', 'success', 1),
(170, 'Login success!', '::1', '2019-06-20 06:44:48', 'success', 1),
(171, 'Login success!', '::1', '2019-06-21 07:35:22', 'success', 1),
(172, 'Login success!', '::1', '2019-06-21 11:08:16', 'success', 1),
(173, 'Login success!', '::1', '2019-06-22 11:47:35', 'success', 1),
(174, 'Login success!', '::1', '2019-06-23 08:48:31', 'success', 1),
(175, 'Login success!', '::1', '2019-06-23 12:50:36', 'success', 1),
(176, 'Login success!', '::1', '2019-06-23 14:32:46', 'success', 1),
(177, 'Login success!', '::1', '2019-06-23 15:33:32', 'success', 1),
(178, 'Login fail!', '::1', '2019-06-23 15:44:01', 'fail', 0),
(179, 'Login success!', '::1', '2019-06-23 15:44:12', 'success', 1),
(180, 'Login fail!', '::1', '2019-06-23 15:44:57', 'fail', 0),
(181, 'Login success!', '::1', '2019-06-23 15:45:03', 'success', 1),
(182, 'Login success!', '::1', '2019-06-23 15:45:48', 'success', 1),
(183, 'Login success!', '::1', '2019-06-23 16:17:33', 'success', 1),
(184, 'Login success!', '::1', '2019-06-23 16:18:12', 'success', 1),
(185, 'Login success!', '::1', '2019-06-23 16:39:30', 'success', 1),
(186, 'Login success!', '::1', '2019-06-23 22:53:08', 'success', 1),
(187, 'Login success!', '::1', '2019-06-24 06:22:35', 'success', 1),
(188, 'Login success!', '::1', '2019-06-24 06:45:37', 'success', 1),
(189, 'Login success!', '::1', '2019-06-24 06:53:24', 'success', 1),
(190, 'Login success!', '::1', '2019-06-24 06:53:44', 'success', 1),
(191, 'Login success!', '::1', '2019-06-24 06:55:20', 'success', 1),
(192, 'Login success!', '::1', '2019-06-24 06:55:40', 'success', 1),
(193, 'Login success!', '::1', '2019-06-24 07:02:24', 'success', 1),
(194, 'Login success!', '::1', '2019-06-24 07:02:42', 'success', 1),
(195, 'Login success!', '::1', '2019-06-24 07:03:25', 'success', 1),
(196, 'Login success!', '::1', '2019-06-24 07:05:12', 'success', 1),
(197, 'Login success!', '::1', '2019-06-24 07:06:59', 'success', 1),
(198, 'Login success!', '::1', '2019-06-24 07:07:09', 'success', 1),
(199, 'Login success!', '::1', '2019-06-24 07:11:15', 'success', 1),
(200, 'Login success!', '::1', '2019-06-24 07:11:28', 'success', 1),
(201, 'Login success!', '::1', '2019-06-24 07:31:58', 'success', 1),
(202, 'Login success!', '::1', '2019-07-05 14:56:22', 'success', 1),
(203, 'Login success!', '::1', '2019-07-05 16:31:41', 'success', 1),
(204, 'Login success!', '::1', '2019-07-06 06:35:38', 'success', 1),
(205, 'Login success!', '::1', '2019-07-06 10:21:44', 'success', 1),
(206, 'Login success!', '::1', '2019-07-08 08:32:53', 'success', 1),
(207, 'Login success!', '::1', '2019-07-08 15:11:22', 'success', 1),
(208, 'Login success!', '::1', '2019-07-09 06:42:34', 'success', 1),
(209, 'Login success!', '::1', '2019-07-09 22:01:36', 'success', 1),
(210, 'Login success!', '::1', '2019-07-10 09:15:10', 'success', 1),
(211, 'Login success!', '::1', '2019-07-11 08:12:43', 'success', 1),
(212, 'Login success!', '::1', '2019-07-11 09:28:28', 'success', 1),
(213, 'Login success!', '::1', '2019-07-11 13:18:24', 'success', 1),
(214, 'Login success!', '::1', '2019-07-11 21:19:24', 'success', 1),
(215, 'Login success!', '::1', '2019-07-12 06:44:29', 'success', 1),
(216, 'Login success!', '::1', '2019-07-12 09:17:01', 'success', 1),
(217, 'Login success!', '::1', '2019-07-13 13:49:57', 'success', 1),
(218, 'Login success!', '::1', '2019-07-13 17:44:03', 'success', 1),
(219, 'Login success!', '::1', '2019-07-13 18:44:09', 'success', 1),
(220, 'Login success!', '::1', '2019-07-15 07:18:15', 'success', 1),
(221, 'Login success!', '::1', '2019-07-15 14:39:19', 'success', 1),
(222, 'Login success!', '::1', '2019-07-15 18:36:45', 'success', 1),
(223, 'Login success!', '::1', '2019-07-28 08:43:41', 'success', 1),
(224, 'Login success!', '::1', '2019-08-05 06:17:36', 'success', 1),
(225, 'Login success!', '::1', '2019-08-06 09:56:16', 'success', 1),
(226, 'Login success!', '::1', '2019-08-06 21:42:16', 'success', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bz1_votes`
--

CREATE TABLE `bz1_votes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT 'ID of user',
  `object_id` int(11) NOT NULL COMMENT 'ID of album, post.. belong to extension',
  `created` datetime DEFAULT NULL,
  `extension` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'album, post...or any object type',
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ex: like, dislike',
  `ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '~week'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bz1_votes`
--

INSERT INTO `bz1_votes` (`id`, `title`, `user_id`, `object_id`, `created`, `extension`, `type`, `ip`, `status`) VALUES
(1, 'Bình chọn bài dự thi', 19, 20, '2016-03-07 09:57:33', 'contest', 'votes', '::1', 1),
(2, 'Chia sẻ bài dự thi', 19, 20, '2016-03-07 09:58:10', 'contest', 'shares', '::1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bz1_contacts`
--
ALTER TABLE `bz1_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_content_types`
--
ALTER TABLE `bz1_content_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_customers`
--
ALTER TABLE `bz1_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `bz1_groups`
--
ALTER TABLE `bz1_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_groups_permission`
--
ALTER TABLE `bz1_groups_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_languages`
--
ALTER TABLE `bz1_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_metatags`
--
ALTER TABLE `bz1_metatags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_nodewords_shops1` (`object_id`);

--
-- Indexes for table `bz1_posts`
--
ALTER TABLE `bz1_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_posts_metas`
--
ALTER TABLE `bz1_posts_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_posts_tags`
--
ALTER TABLE `bz1_posts_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_posts_terms`
--
ALTER TABLE `bz1_posts_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_posts_terms_shops1` (`post_id`),
  ADD KEY `fk_posts_terms_terms1` (`term_id`);

--
-- Indexes for table `bz1_provinces`
--
ALTER TABLE `bz1_provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_redirects`
--
ALTER TABLE `bz1_redirects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_link_old` (`source`) USING BTREE,
  ADD KEY `idx_link_modifed` (`modified`) USING BTREE;

--
-- Indexes for table `bz1_settings`
--
ALTER TABLE `bz1_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_templates`
--
ALTER TABLE `bz1_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_terms`
--
ALTER TABLE `bz1_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_terms_vocabularies1` (`vocabulary_id`);

--
-- Indexes for table `bz1_users`
--
ALTER TABLE `bz1_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `bz1_users_groups`
--
ALTER TABLE `bz1_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_roles_users1` (`user_id`),
  ADD KEY `fk_users_roles_roles1` (`group_id`);

--
-- Indexes for table `bz1_users_login_log`
--
ALTER TABLE `bz1_users_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bz1_votes`
--
ALTER TABLE `bz1_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_votes_users1` (`user_id`),
  ADD KEY `fk_votes_shops1` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bz1_contacts`
--
ALTER TABLE `bz1_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_content_types`
--
ALTER TABLE `bz1_content_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bz1_customers`
--
ALTER TABLE `bz1_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `bz1_groups`
--
ALTER TABLE `bz1_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bz1_groups_permission`
--
ALTER TABLE `bz1_groups_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20437;

--
-- AUTO_INCREMENT for table `bz1_languages`
--
ALTER TABLE `bz1_languages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bz1_metatags`
--
ALTER TABLE `bz1_metatags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `bz1_posts`
--
ALTER TABLE `bz1_posts`
  MODIFY `id` bigint(19) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_posts_metas`
--
ALTER TABLE `bz1_posts_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_posts_tags`
--
ALTER TABLE `bz1_posts_tags`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_posts_terms`
--
ALTER TABLE `bz1_posts_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `bz1_redirects`
--
ALTER TABLE `bz1_redirects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_settings`
--
ALTER TABLE `bz1_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `bz1_templates`
--
ALTER TABLE `bz1_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bz1_terms`
--
ALTER TABLE `bz1_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_users`
--
ALTER TABLE `bz1_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `bz1_users_groups`
--
ALTER TABLE `bz1_users_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bz1_users_login_log`
--
ALTER TABLE `bz1_users_login_log`
  MODIFY `id` bigint(19) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `bz1_votes`
--
ALTER TABLE `bz1_votes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
