<?php

namespace Api\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Core\Model\AppModel;
use Zend\Db\Sql\Select;

class Contest extends AppModel {

    public $table = 'bz1_contests';
    public $context = 'contest';

}