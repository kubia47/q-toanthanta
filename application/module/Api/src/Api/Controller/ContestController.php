<?php

namespace Api\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ContestController extends FrontController {

    public function ruleAction() {
    	$isTrue = true;
    	if($isTrue){
    		$returnInfo = array('status'=>true,'message'=>'Lấy dữ liệu thành công!');
        	return new JsonModel($returnInfo);
    	}else{
    		$returnInfo = array('status'=>false,'message'=>'Lấy dữ liệu thất bại!');
        	return new JsonModel($returnInfo);
    	}
    }

}
