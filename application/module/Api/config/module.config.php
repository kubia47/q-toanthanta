<?php
//module/Api/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Api\Controller\Contest' => 'Api\Controller\ContestController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'contest-rule' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/contest-rule[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Contest',
                        'action'     => 'rule',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
