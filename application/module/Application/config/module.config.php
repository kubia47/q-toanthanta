<?php

return array(
   
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),

            'what-do-we-do' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/what-do-we-do[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'whatdowedo',
                    ),
                ),
            ),

            'about-us' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/who-we-are[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'whoweare',
                    ),
                ),
            ),
            'with-us' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/with-us[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'withus',
                    ),
                ),
            ),
            'all-news-and-events' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/all-news-and-events[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'newsandevents',
                    ),
                ),
            ),
            'contact-page' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/contact-us[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'contactpage',
                    ),
                ),
            ),

            //--Translate language--
            'translate-lang' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ngon-ngu[/:lang][/]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'translatelang',
                    ),
                ),
            ),
            //--Translate language--
            'translate-lang-admin' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/admin/ngon-ngu[/:lang][/:url_redirect][/]',
                    'constraints' => array(
                        'language' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'url_redirect' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'translatelang',
                    ),
                ),
            ),
            //--End Translate language--

            'application' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'vi_VN',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
          
        ),
    ),
   'view_manager' => array(
        'template_path_stack' => array(
            'application' => __DIR__ . '/../src/Application/View',
            
        ),
    ),
);
