<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Session\Container;

class Module
{
    public function onBootstrap($e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();

        //**********Translate language---
        $translator =  $serviceManager->get('MvcTranslator');
        $sessionContainer = new Container('translate_locale');
        $myLocale = $sessionContainer->myLocale;
        if(!$myLocale){ //Set Default
            $myLocale = $this->getDefaultLang($e);
        }

        $myLocale = $this->getDefaultLang($e, true, $myLocale);
        $translator->setLocale($myLocale);

        //--Set to view
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        if($myLocale == 'ja_JP'){
            $myLocale = 'en_US';
            $viewModel->setVariables(array('myLocale'=>$myLocale, 'isLocaleJP'=>true));
        }else{
            $viewModel->myLocale = $myLocale;
        }          

        //************End Translate language---

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    private function getDefaultLang($e, $isCheckLangAvailable = false, $langCode = ''){
        $langCodeDefault = 'vi_VN';
        $langModel = $e->getApplication()->getServiceManager()->get('Language\Admin\Model\Language');

        if($isCheckLangAvailable){
            if($langCode){
                $lang = $langModel->getItem(array('lang_code'=>$langCode,'status'=>1));
                if($lang){
                    return $langCode;
                }
            }
            return $langCodeDefault;
        }else{
            $lang = $langModel->getItem(array('is_default'=>1,'status'=>1));
            if($lang){
                return $lang['lang_code'];
            }
        }     

        return $langCodeDefault;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
               'absolteUrl' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\AbsoluteUrl($locator->get('Request'));
            },
            'ckeditor' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\Ckeditor();
            },
            'UserRole' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\UserRole();
            },
            'formMessenger' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\FormMessenger();
            },
            'plaintext' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\Plaintext();
            },
            'shareThis' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\Sharethis();
            },
            'slug' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\Slug();
            },
            'truncate' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\Truncate();
            },
            'flashMessenger' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\FlashMessenger($locator);
            },
            'layoutHelper' => function($sm) {
                $locator = $sm->getServiceLocator();
                return new \Core\View\Helper\LayoutHelper($locator);
            },
            'factory' => function($sm) {
                $locator = $sm->getServiceLocator();
                $factory = new \Core\View\Helper\Factory($locator->get('Request'));
                $factory->setServiceLocator($locator);
                $factory->setServiceFactory($locator->get('ServiceFactory'));

                return $factory;
            },
            ),
                        
        );
    }
}
