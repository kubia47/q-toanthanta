<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;



class IndexController extends FrontController
{

    public function indexAction()
    {
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();

        $content_global = $postModel->getListItems(array('type' => 'content_news'), array(), 10);
        $content_global = $content_global->toArray();

        $newdocs = $postModel->getListItems(array('type' => 'content_docs'), array(), 5);
        $newdocs = $newdocs->toArray();

        // if($user->id) {
        $newquizs = $postModel->getListItems(array('type' => 'content_quiz'), array(), 5);
        $newquizs = $newquizs->toArray();
        // }
        $newvideos = $postModel->getListItems(array('type' => 'content_video'), array(), 5);
        $newvideos = $newvideos->toArray();

        // print_r($content_global);

        return new ViewModel(array(
            'content_global' => $content_global,
            'newdocs' => $newdocs,
            'newquizs' => $newquizs,
            'newvideos' => $newvideos,
            'user' => $user
        ));
    }
    
    //***************************************************
    //---Change language--
    public function translatelangAction() {
        $params = $this->getParams();
        $lang = $params['lang'];
        $urlRedirect = $params['url_redirect'];
        //--Set lang
        if($lang){
            //$lang = $lang == 'ja_JP' ? 'en_US' : $lang;
            $langModel = $this->getLangModel();
            $curLang = $langModel->getItem(array('lang_code'=>$lang));
            if($curLang['status'] == 1){ //TODO: code here...
                $this->setLangCode($lang);
            }
        }
        //--End set lang
        if($urlRedirect){
            $this->redirectToRoute('admin');
        }else{
            $this->redirectToRoute('home');
        }
    }

    //---End Change language--
}
