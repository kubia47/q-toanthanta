<?php

namespace ZDemo\Front\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;
use LightnCandy\LightnCandy;

class HandlebarsController extends FrontController {
   
    /*
    * Demo handlebars vs zf2
    * https://github.com/zordius/lightncandy
    */ 
    public function handlebarsAction(){
        $params = $this->getParams();
        $this->setMetaData(array(), 'Demo ZF2 vs PhpHandlebars');

        $handlebarTpl = 'z-demo/handlebars/templates/handlebars-tpl.html';
        $handlebarData = array(
            'name' => 'Mr.Nhan', 
            'age' => 27, 
            'items'=>array(
                array('id'=>1, 'title'=>'nhan1'),
                array('id'=>2, 'title'=>'nhan2'),
                array('id'=>3, 'title'=>'nhan3'),
                array('id'=>4, 'title'=>'nhan4'),
                array('id'=>5, 'title'=>'nhan5'),
            )
        );

        $strTemplate = $this->handlebarRender($handlebarTpl, $handlebarData);
        return new ViewModel(array(
           'strTemplate' =>  $strTemplate
        ));
    }

    public function handlebarRender($handlebarTpl, $handlebarData, $zendTplData){
        $tViewModel = new viewModel($zendTplData);
        $tViewModel->setTemplate($handlebarTpl);
        $template = $this->getViewRender()->render($tViewModel);
        $phpStr = LightnCandy::compile($template, Array(
            'flags' => LightnCandy::FLAG_HANDLEBARSJS,
            'helpers' => Array(
                'myif' => function ($conditional, $options) {
                    if ($conditional) {
                        return $options['fn']();
                    } else {
                        return $options['inverse']();
                    }
                }
            )
        ));
        $renderer = LightnCandy::prepare($phpStr);

        return $renderer($handlebarData);
    }

}
