<?php

namespace ZDemo\Front\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;
use Pheanstalk\Pheanstalk;

class BeantalkController extends FrontController {
    
    /*Sử dụng hàng đợi để giảm bớt time response cho client
    * Dùng khi gửi email or 1 tác vụ nào nó mà phải xử lý lâu
    * Dưới đây là code DEMO
    */
    public function beantalkAction() {
        $this->setMetaData(array(), 'Demo PHP Beantalk');

        // $pheanstalk = new Pheanstalk('127.0.0.1', 8080);
        // $t = 'mytube';
        // // ----------------------------------------
        // // producer (queues jobs)

        // $pheanstalk
        // ->useTube($t)
        // ->put($this->sendEmail(array('name'=>'NhanPT 123', 'email'=>'nhanptit90@gmail.com')));

        // // ----------------------------------------
        // // worker (performs jobs)

        // $job = $pheanstalk
        //   ->watch($t)
        //   ->ignore('default')
        //   ->reserve();

        // echo $job->getData();
        // $pheanstalk->delete($job);


        /*=== Demo new:= ====*/
        $tube = 'email_queue';
        $pheanstalk = new Pheanstalk('127.0.0.1', 8080);

        // Add json for job to "email_queue" beanstalk tube
        $pheanstalk
            ->useTube($tube)
            ->put($this->sendEmail(array('name'=>'NhanPT 123', 'email'=>'nhanptit90@gmail.com')));


        // Continuously loop to endlessly monitor beanstalk queue
        while (true) {
            // Checks beanstalk connection
            if (!$pheanstalk->getConnection()->isServiceListening()) {
                echo "error connecting to beanstalk, sleeping for 5 seconds... \n";

                // Sleep for 5 seconds
                sleep(1);

                // Skip to next iteration of loop
                continue;
            }

            // Get job from queue, if none wait for a job to be available
            $job = $pheanstalk
                ->watch($tube)
                ->ignore('default')
                ->reserve();
            $pheanstalk->delete($job);

            echo $job->getData(); 
        }

        /*=== End Demo new:= ====*/


        return new ViewModel(array());   
    }

    private function sendEmail($data){
        $mail = $this->getServiceLocator()->get('SendMail');
        $mail->send(array(
            'from' => array('name' => EMAIL_SEND_FROM_NAME, 'email' => EMAIL_SEND_FROM_EMAIL),
            'to' => array('name' => $data['name'], 'email' => $data['email']),
            'subject' => '[BZ CMS Custom] thông báo',
            'template' => 'email/contact',
            'data' => $data
        ));
    }

}
