<?php

namespace ZDemo\Front\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;
use Pheanstalk\Pheanstalk;

class OfficeController extends FrontController {
    
    /*Xuất ra file word
    * Dưới đây là code DEMO
    * Info plugin : http://phpword.readthedocs.org/en/latest/index.html#welcome-to-phpword-s-documentation
    * Permission: 777
    */
    /*=====Word======*/
    public function wordAction(){
        $params = $this->getParams();
        $this->setMetaData(array(), 'Demo PHP Office');

        $file = strtotime(date('Y-m-d')) .rand(2000, 7000) . '.docx';
        $path = WEB_ROOT . '/media/' . $file;
        $this->saveWord($file, $item);
        // print_r(filesize(WEB_ROOT . '/media/' . $file));die;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile("$path");
        exit();
    }

    private function saveWord($file, $data){
        require_once VENDOR_INCLUDE_DIR . '/PhpWord/Autoloader.php';
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        
        $section = $phpWord->addSection();
        $section->addText(
            htmlspecialchars('Title'),
            array('name' => 'Arial', 'size' => 18)
        );

        $section->addTextBreak(2);

        $section->addText(
            htmlspecialchars('Thông tin'),
            array('name' => 'Arial', 'size' => 24, 'color' => '1B2232', 'bold' => true),
            array('align' => 'center')
        );
        
        $section = $phpWord->addSection();
        $section->addImage(
            'https://pixabay.com/static/uploads/photo/2015/10/01/21/39/background-image-967820_960_720.jpg',
            array(
                'width' => 700,
                'marginTop' => -1,
                'marginLeft' => -1,
                'wrappingStyle' => 'behind'
            )
        );

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(WEB_ROOT . '/media/' . $file);
    }
    /*=====End Word======*/

}
