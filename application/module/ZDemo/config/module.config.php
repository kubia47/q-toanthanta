<?php

// module/ZDemo/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'ZDemo\Front\Controller\Beantalk' => 'ZDemo\Front\Controller\BeantalkController',
            'ZDemo\Front\Controller\Office' => 'ZDemo\Front\Controller\OfficeController',
            'ZDemo\Front\Controller\Handlebars' => 'ZDemo\Front\Controller\HandlebarsController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'beantalk' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/demo/beantalk[/]',
                    'defaults' => array(
                        'controller' => 'ZDemo\Front\Controller\Beantalk',
                        'action'     => 'beantalk',
                    ),
                ),
            ),
            'office-word' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/demo/office-word[/]',
                    'defaults' => array(
                        'controller' => 'ZDemo\Front\Controller\Office',
                        'action'     => 'word',
                    ),
                ),
            ),
            'handlebars' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/demo/handlebars[/]',
                    'defaults' => array(
                        'controller' => 'ZDemo\Front\Controller\Handlebars',
                        'action'     => 'handlebars',
                    ),
                ),
            ),
        ),
    ),
    'view_manager'=>array(
        'template_map'=>array(
           
        )
    )
);
