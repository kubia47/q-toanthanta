<?php

// module/Content/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Content\Front\Controller\Posts' => 'Content\Front\Controller\PostsController',
            'Content\Front\Controller\Pages' => 'Content\Front\Controller\PagesController',
            'Content\Admin\Controller\Post' => 'Content\Admin\Controller\PostController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(

            //--********** Block Front ***************
            'content' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/content[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action'     => 'index',
                    ),
                ),
            ),

            'details-quiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/trac-nghiem[/:slug][/:id][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'quiz',
                    ),
                ),
            ),
            'quiz-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/de-thi-trac-nghiem[/:slug][/:category][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'category'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'quizList',
                    ),
                ),
            ),
            'docs-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tai-lieu[/:category][/:slug][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'category'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'docsList',
                    ),
                ),
            ),
            'videos-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/video[/:slug][/:category][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'category'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'videosList',
                    ),
                ),
            ),
            'api-quizs-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api-get-quizs[/]',
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'apiQuizsList',
                    ),
                ),
            ),
            'api-docs-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api-get-docs[/]',
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'apiDocsList',
                    ),
                ),
            ),
            'docs-details' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/doc-tai-lieu[/:id][/:slug][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'docsDetails',
                    ),
                ),
            ),
            'api-videos-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api-get-videos[/]',
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'apiVideosList',
                    ),
                ),
            ),
            'video-details' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/xem-video[/:slug][/:id][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'videoDetails',
                    ),
                ),
            ),
            /*'detail_news' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/news[/:slug][/:id][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'details',
                    ),
                ),
            ),
            'detail_event' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/event[/:slug][/:id][/]',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Front\Controller\Posts',
                        'action' => 'details',
                    ),
                ),
            ),*/
            //--********** End Block Front ***************

            //--********** Block admin ***************
            'postadmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/post[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'index',
                    ),
                ),
            ),
            'classes' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/classes[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'classes',
                    ),
                ),
            ),
            'class-info' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/class[/:id][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'classInfo',
                    ),
                ),
            ),
            'student-details' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/student[/:id][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'student',
                    ),
                ),
            ),
            'multi-upload' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/multi-upload[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'multiUpload',
                    ),
                ),
            ),
            'add-class' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/add-class[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'addClass',
                    ),
                ),
            ),
            'add-student' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/add-student[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'addStudent',
                    ),
                ),
            ),
            'edit-student' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/edit-student[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'editStudent',
                    ),
                ),
            ),
            'get-classes' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/classes[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'getClasses',
                    ),
                ),
            ),
            'add-schedule' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/add-schedule[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'addSchedule',
                    ),
                ),
            ),
            'remove-schedule' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/remove-schedule[/]',
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'removeSchedule',
                    ),
                ),
            ),
            'muster' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/muster[/:id][/:date][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'date' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Content\Admin\Controller\Post',
                        'action'     => 'muster',
                    ),
                ),
            )
        ),
    ),
     'view_manager'=>array(
        'template_map'=>array(
            'content/block/latest'   => __DIR__ . '/../src/Content/Front/View/content/block/latest/default.phtml',
        )
    )

);
