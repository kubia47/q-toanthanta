<?php

namespace Content\Front\Model;

use Core\Model\FrontAppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class Post extends FrontAppModel {

    public $table = 'bz1_posts';
    public $context = 'post';

    public function getDefaultListQuery() {

        $select = new Select($this->table);
        $status = 1; //$this->getState('filter.status');
        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }
        
        $keyword = $this->getState('filter.keyword');
        if ($keyword) {
            $keyword = addslashes(trim($keyword));
            //$keyword = mysql_real_escape_string(trim($keyword));
            $select->where("(". $this->table.".title like '%$keyword%' OR ". $this->table.".identity like '%$keyword%')");
        }

        $category = $this->getState('filter.category');
        if ($category) {
            $category = intval($category);
            $select->join('bz1_posts_terms', 'bz1_posts_terms.post_id=' . $this->table . '.id', array('term_id'));
            $select->where(array('term_id' => $category));
        }

        $type = $this->getState('filter.type');
        if (!empty($type)) {
            $select->where(array('type' => $type));
        }

        $identity = $this->getState('filter.identity');
        if(!empty($identity)){
            $select->where(array('identity' => $identity));
        }

        $limit = 5; //$limit = $this->getState('filter.limit');
        if (intval($limit) > 0) {
            $select->limit($limit);
        }

        //order
        $filter_order = $this->getState('order.field');
        $filter_order_dir = $this->getState('order.direction');

        if (!empty($filter_order)) {
            $select->order($filter_order . " " . $filter_order_dir);
        }else{
            $select->order($this->table.'.created DESC');
        }
        //print $select->getSqlString(); die;
        return $select;
    }

    public function getContentById($id) {
        $select = new Select($this->table);
        $select->where(array($this->table . '.id' => $id));

        $result = $this->selectWith($select);
        $items = $result->toArray();

        return $items[0];
    }

    public function getListContentType($content_type, $limit = 0, $status = '') {
        $select = new Select($this->table);

        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }

        if ($content_type) {
            $select->where(array('type' => $content_type));
        }

        if (intval($limit) > 0) {
            $select->limit($limit);
        }

        $result = $this->selectWith($select);
        $items = $result->toArray();

        return $items;
    }

    public function getListItems($where = array(), $columns = array(), $limit = 0, $ordering = 'created desc') {
        $select = new Select($this->table);
        if (!empty($columns)) {
            $select->columns($columns);
        }
        $select->where($where);
        if (intval($limit) > 0) {
            $select->limit($limit);
        }

        $select->order($this->table . "." . $ordering);

        $result = $this->selectWith($select);
        return $result;
    }

    public function getByOptions($type = 'news', $limit = 0, $language='*', $wheres = array(), $ordering = 'created desc', $columns = array()) {


        $select = new Select($this->table);

        if (!empty($columns)) {
            $select->columns($columns);
        }

        $select->where(array($this->table . '.status' => 1)); //Alway get publish

        if($type){
            $select->where(array($this->table . '.type' => $type));
        }

        if (!empty($language) && $language != '*') {
            $select->where(array(' ('.$this->table.'.language = "'.$language.'" or '.$this->table.'.language = "*" )'));
        }


        if($wheres && count($wheres) > 0){
            $select->where($wheres);
        }

        if (intval($limit) > 0) {
            $select->limit($limit);
        }

        if($ordering != "rand()"){
            $select->order($this->table . "." . $ordering);
        }else{
            $rand = new \Zend\Db\Sql\Expression('RAND()');
            $select->order($rand);
        }

        $result = $this->selectWith($select);
        $items = $result->toArray();
        if($limit == 1){
            $items =  $items[0];
        }

        return $items;
    }

    public function getByIdAndSlug($id, $slug, $type = 'post', $language = '*') {
        $select = new Select($this->table);
        $select->where(array($this->table . '.status' => 1));

        if ($id) {
            $select->where(array($this->table . '.id' => $id));
        } else {
            if ($slug) {
                $select->where(array($this->table . '.slug' => $slug));
            }
        }

        $select->limit(1);

        // print $select->getSqlString(); die;
        $result = $this->selectWith($select);
        if ($result) {
            return $item = $result->current();
            // return get_object_vars($item);
        }

        return array();
    }

}
