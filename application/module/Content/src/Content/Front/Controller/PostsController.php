<?php

namespace Content\Front\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;

class PostsController extends FrontController {

    // -- Tin tuc --
    public function newsAction() {

        $postModel = $this->getPostModel();

        $language = $this->getLangCode();
        $type ='news';

        $params = $this->getParams();
        if($params['filterOption']){
            $_SESSION['newsFilterOption'] = $params['filterOption'];
            $postModel->setState('order.field','created');
            $postModel->setState('order.direction',$params['filterOption']);
        }
         if(!empty($_SESSION['newsFilterOption'])){
            $postModel->setState('order.field','created');
            $postModel->setState('order.direction',$_SESSION['newsFilterOption'] );
        }

        $postModel->setContext('tintuc');
        $postModel->setParams($params);
        $postModel->setState('filter.type',$type);
        $postModel->setState('filter.language',$language);
        
        $postModel->setLimit(8);
        $news = $postModel->getItems();

        $this->setMetaData(array(), $this->translate('Tin tuc'));
        $this->layout()->setVariables(array('page'=>'tin_tuc', 'sub_menu'=>'tin_tuc'));
        return new ViewModel(array(
            'news'   => $news->toArray(),
            'paging' => $postModel->getPaging(),       
        ));
    }

    public function quizAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $contestModel = $this->getContestModel();
        $user = $this->getUserLogin();


        if ($params['id'] || $params['slug']) {
            $quizDetails = $postModel->getByIdAndSlug($params['id'], $params['slug']);

            $tex = $quizDetails['body'];
            $choiceArr = explode("\choice", $tex);
            array_shift($choiceArr); // remove first element

            $quizResults = array();
            foreach ($choiceArr as $key => $choice) {
                $breakline_arr = explode("\n", $choice);

                $count = 0;
                $position = 0;
                $results = array();
                foreach ($breakline_arr as $key2 => $anwser) {
                    if($count < 9) {
                        if($count != 0 && !empty(trim($anwser))){
                            $position++;
                            if (strpos($anwser, "\True") !== false) {
                                //echo $key.'<br/>';
                                array_push($quizResults, $position);
                            }
                        }
                        $count++;
                    } else {
                        break;
                    }
                }
            }

            // check if this user has a contest with this quiz
            $contest = $contestModel->getContestByUserPost($user->id, $quizDetails['id']);
            $contestList = $contestModel->getListContestUser($params['id']);

            $resultList = array();
            if (count($contestList) > 0) {
                foreach ($contestList as $k => $value) {
                    if ($value['result1'] && $value['result1'] != '[]') {
                        $correct = 0;
                        // compare 
                        $anwsers = json_decode($value['result1']);
                        foreach ($anwsers as $key => $anw) {
                            if ($anw == $quizResults[$key]) {
                                $correct++;
                            }
                        }

                        $results = array(
                            'user_name' => $value['user_name'],
                            'user_avatar' => $value['user_avatar'],
                            'user_class' => $value['class'],
                            'user_school' => $value['social_token'],
                            'correct' => $correct,
                            'worktime' => $value['worktime'],
                            'sum' => count($quizResults)
                        );

                        array_push($resultList, $results);
                    }
                }
            }

            usort($resultList, function($a, $b)
            {
                return $a['correct'] < $b['correct'];
            });

            return new ViewModel(array(
                'contest' => $contest,
                'user' => $user,
                'quiz' => $quizDetails,
                'quizResults' => $quizResults,
                'resultList' => $resultList
            ));
        }
    }

    public function quizListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();
        $categoryModel = $this->getCategoryModel();

        // get list content by category
        if ($params['category']) {
            // $listContents = $postModel->getByOptions('content_quiz', null, null, array('category' => $params['category']));
            $cat = $categoryModel->getCategoryById($params['category']);

            // get list category
            $categories = $categoryModel->getChildCategories($params['category'], $categories, 1);
        }
        
        return new ViewModel(array(
            'user' => $user,
            'categories' => $categories,
            'cat' => $cat
        ));
    }

    public function docsListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $categoryModel = $this->getCategoryModel();

        // get list content by category
        if ($params['category']) {
            $cat = $categoryModel->getCategoryById($params['category']);

            // get list category
            $categories = $categoryModel->getChildCategories($params['category'], $categories, 1);
        }
        
        return new ViewModel(array(
            'categories' => $categories,
            'cat' => $cat
        ));
    }

    public function videosListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $categoryModel = $this->getCategoryModel();
        $user = $this->getUserLogin();

        // get list content by category
        if ($params['category']) {
            $cat = $categoryModel->getCategoryById($params['category']);

            // get list category
            $categories = $categoryModel->getChildCategories($params['category'], $categories, 1);
        }
        
        return new ViewModel(array(
            'user' => $user,
            'categories' => $categories,
            'cat' => $cat
        ));
    }

    public function apiDocsListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();

        if ($params['category']) {
            $where = array('category' => $params['category']);

            $listContents = $postModel->getByOptions('content_docs', null, null, $where);

            $this->returnJsonAjax(array('status'=>true, 'message'=>'success', 'data'=>$listContents));
        }

        $this->returnJsonAjax(array('status'=>false, 'message'=>'failed'));
    }

    public function apiQuizsListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();

        if ($params['category']) {
            $listContents = $postModel->getByOptions('content_quiz', null, null, array('category' => $params['category']));

            $this->returnJsonAjax(array('status'=>true, 'message'=>'success', 'data'=>$listContents));
        }

        $this->returnJsonAjax(array('status'=>false, 'message'=>'failed'));
    }

    public function docsDetailsAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();

        if ($params['id'] || $params['slug']) {
            $docsDetails = $postModel->getByIdAndSlug($params['id'], $params['slug']);
        }

        return new ViewModel(array(
            'docsDetails' => $docsDetails,
            'user' => $user
        ));
    }

    public function apiVideosListAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();

        if ($params['category']) {
            $where = array('category' => $params['category']);
            $listContents = $postModel->getByOptions('content_video', null, null, $where);

            $this->returnJsonAjax(array('status'=>true, 'message'=>'success', 'data'=>$listContents));
        }

        $this->returnJsonAjax(array('status'=>false, 'message'=>'failed'));
    }

    public function videoDetailsAction() {
        $params = $this->getParams();
        $postModel = $this->getPostModel();
        $user = $this->getUserLogin();

        if ($params['id'] || $params['slug']) {
            $videoDetails = $postModel->getByIdAndSlug($params['id'], $params['slug']);
        }

        return new ViewModel(array(
            'videoDetails' => $videoDetails,
            'user' => $user
        ));
    }

}