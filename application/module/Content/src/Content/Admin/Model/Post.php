<?php

namespace Content\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Like;

class Post extends AppModel {

    public $table = 'bz1_posts';
    public $context = 'post';

    public function save($data, $id = 0, $primarykey = 'id') {

        $category = $data['category'];

        //TODO: 1 bai n category
        // if ($data['category']) {
        //     unset($data['category']);
        // } 
        // if ($data instanceof \ArrayObject) {
        //     $data->offsetUnset('category');
        // }

        if ($data['id']) {
            $data['modified'] = date('Y-m-d H:i:s');
        } else {
            $data['created'] = date('Y-m-d H:i:s');
            $data['modified'] = $data['created'];
        }

        if (empty($id)) {

            if (array_key_exists("$primarykey", $data)) {
                unset($data["$primarykey"]);
            }
            if (array_key_exists('created', $data) && empty($data['created'])) {
                $data['created'] = date('Y-m-d H:i:s');
            }
            if (array_key_exists('modified', $data) && empty($data['modified'])) {
                $data['modified'] = date('Y-m-d H:i:s');
            }
            if ($this->insert($data)) {
                $id = $this->getLastInsertValue();
            }
        } else {
            if (array_key_exists('modified', $data) && empty($data['modified'])) {
                $data['modified'] = date('Y-m-d H:i:s');
            }

            $this->update($data, array("$primarykey = ?" => $id));
        }
        if ($id) {
            $data['id'] = $id;
            return array(
                'status' => true, 'id' => $id, 'item' => $data
            );
        } else {
            return array('status' => false);
        }
    }

    public function getItemById($id, $columns = array()) {
        $item = parent::getItemById($id, $columns);
        //TODO: 1 bai n category
        // if ($item) {
        //     $portTermModel = $this->getServiceLocator()->get('Content\Admin\Model\PostTerm');
        //     $item['category'] = $portTermModel->getTermIdsByPostId($item['id']);
        // }
        return $item;
    }

    public function countContentByType($type) {
        $select = new Select($this->table);
        $select->where(array('type' => $type));

        $result = $this->selectWith($select);
        $rows = $result->toArray();

        return count($rows);
    }

    public function getListContentType($content_type, $ordering = null, $limit = 0, $status = '', $where = null) {
        $select = new Select($this->table);

        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }

        if ($content_type) {
            $select->where(array('type' => $content_type));
        }

        if ($where) {
            $select->where($where);
        }

        if (intval($limit) > 0) {
            $select->limit($limit);
        }

        if ($ordering) {
            $select->order($ordering);
        }

        $result = $this->selectWith($select);
        $items = $result->toArray();

        return $items;
    }

    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);

        $published = $this->getUserStateFromRequest('filter.status', 'filter_status', '');
        
        $this->setState('filter.status', $published);

        $language = $this->getUserStateFromRequest('filter.language', 'filter_language', '');
        $this->setState('filter.language', $language);

        $category = $this->getUserStateFromRequest('filter.category', 'filter_category', '');
        $this->setState('filter.category', $category);

        $type = $this->getUserStateFromRequest('filter.type', 'filter_type', '');
        $this->setState('filter.type', $type);

        parent::populateState();
    }

    public function getDefaultListQuery() {

        $select = new Select($this->table);
        $status = $this->getState('filter.status');
        
        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }

        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            // $select->where(" (". $this->table.".id like '%$keyword%' or ". $this->table.".title like '%$keyword%') ");
        }
        
        $select->join('bz1_terms', 'bz1_terms.id=' . $this->table . '.category', array('CategoryTitle' => 'title'), left);
        /* $category = $this->getState('filter.category');
        if ($category) {
            $category = intval($category);
            $select->join('bz1_posts_terms', 'bz1_posts_terms.post_id=' . $this->table . '.id', array('term_id'));
            $select->where(array('term_id' => $category));
        } */

        /* $language = $this->getState('filter.language');
        if (!empty($language) && $language != '*') {
            $select->where(array(' ('.$this->table.'.language = "'.$language.'" or '.$this->table.'.language = "*" )'));
        } */

        $type = $this->getState('filter.type');
        if (!empty($type)) {
            $select->where(array($this->table.'.type' => $type));
        }

        $identity = $this->getState('filter.identity');
        if (!empty($identity)) {
            $select->where(array('identity' => $identity));
        }

        //order

        $filter_order = $this->getState('order.field');
        $filter_order_dir = $this->getState('order.direction');

        if (!empty($filter_order)) {
            if ($type && $type == 'content_product') {
                if ($filter_order != 'A.name' || $filter_order != 'B.name') {
                    if ($filter_order == 'bz1_terms.title') {
                        $select->order($filter_order . " " . $filter_order_dir);
                    } else {
                        $select->order($this->table. "." . $filter_order . " " . $filter_order_dir);
                    }
                }
            } else if ($filter_order != 'bz1_terms.title'){
                if ($filter_order == 'A.name' || $filter_order == 'B.name') {
                    $select->order($filter_order . " " . $filter_order_dir);
                } else {
                    $select->order($this->table. "." . $filter_order . " " . $filter_order_dir);
                }
            }

        }else{
              $select->order($this->table.".created DESC");
        }
        
        // print $select->getSqlString();
        return $select;
    }

    public function getOrderSum() {
        $type = 'content_sale';
        $select = new Select($this->table);
        $select->where(array($this->table.'.type' => $type));
        $select->join(array('C' => 'bz1_posts'), 'C.id='.$this->table.'.productId', array('product_price' => 'price'));

        $result = $this->selectWith($select);
        $rows = $result->toArray();
        $orderSumPrice = 0;

        foreach ($rows as $row) {
            $orderSumPrice += (float) $row['product_price'] * $row['inventory'];
        }

        return $orderSumPrice;
    }
}
