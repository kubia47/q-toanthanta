<?php

namespace Content\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class Rank extends AppModel {

    public $table = 'bz1_ranking';

    public function getMemberClasses() {

        $select = new Select($this->table);

        $result = $this->selectWith($select);
        $items = $result->toArray();

        return $items;
    }
}
