<?php

namespace Content\Admin\Form;

use Core\Form\CoreForm;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PostForm extends CoreForm implements InputFilterAwareInterface {

    protected $inputFilter;

    public function init() {
        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate form-horizontal panel ');
       
        $_SESSION['content_type'] = $type = $_GET['type']; //Created session contentype for content
        $this->addElements($_GET['lang'], $type);
    }

    protected function addElements($lang, $type) {
        $factory = $this->getServiceLocator()->get('ServiceFactory');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'identity',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required form-control',
                'id' => 'identity'
            ),
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required form-control',
                'id' => 'title'
            ),
        ));
        $this->add(array(
            'name' => 'slug',
            'attributes' => array(
                'type' => 'text',
                'id' => 'slug',
                'class' => ' form-control'
            ),
        ));

        $this->add(array(
            'name' => 'subtitle',
            'attributes' => array(
                'type' => 'text',
                'class' => ' form-control'
            ),
        ));

        $this->add(array(
            'name' => 'intro',
            'attributes' => array(
                'type' => 'textarea',
                'class' => ' form-control',
                'name' => 'intro',
                'rows'  => '5'
            ),
        ));
        $this->add(array(
            'name' => 'body',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'body',
                'class' => ' form-control'
            ),
        ));

        $this->add(array(
            'name' => 'type',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        //TODO: 1 bai n category
        // $this->add(array(
        //     'name' => 'category',
        //     'type' => 'Core\Form\Element\TreeCategoryby',
        //     'options' => array(
        //         'serviceLocator' => $this->getServiceLocator(),
        //     ),
        //     'attributes' => array(
        //         'multiple' => false,
        //         'size' => '10',
        //         'class' => ' form-control'
        //     ),
        // ));

        $catsEdu = $factory->getFourTreeOptions();

        // ksort($catsOptions);
        $this->add(array(
            'name' => 'category_edu',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $catsEdu
            ),
            'attributes' => array(
                'class' => 'required form-control',
            )
        ));

        // print_r($catsOptions);

        $catsOptions = array("10" => "Đại số 10", "100" => "Hình Học 10", "11" => "Đại số 11", "110" => "Hình Học 11", "1100" => "Giải Tích 11", "12" => "Giải Tích 12",  "120" => "Hình Học 12");

        // ksort($catsOptions);
        $this->add(array(
            'name' => 'category',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $catsEdu
            ),
            'attributes' => array(
                'class' => 'required form-control',
            )
        ));

        $this->add(array(
            'name' => 'language',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'lang_group',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Core\Form\Element\Status',
            'options' => array(
                'serviceLocator' => $this->getServiceLocator()
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));  

       $this->add(array(
            'name' => 'ordering',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'ordering'
            ),
        )); 

        $this->add(array(
            'name' => 'featured',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => array('1'=>'Offline', '0'=>'Online') ,
                'class'=>'form-control'
            )
        ));

        $this->add(array(
            'name' => 'views',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => array('0'=>'No','1'=>'Yes') ,
                'class'=>'form-control'
            )
        ));

        $this->add(array(
            'name' => 'duration',
            'attributes' => array(
                'type' => 'text',
                'class' => ' form-control'
            ),
        ));

         //image crop
        $this->add(array(
            'name' => 'image',
            'attributes' => array(
                'type' => 'text',
                'id' => 'image',
                'class' => ' form-control'
            ),
        ));

        $this->add(array(
            'name' => 'large_image',
            'attributes' => array(
                'type' => 'text',
                'id' => 'large_image',
                'class' => ' form-control'
            ),
        ));

        //--Multi image
        $multiImageName = 'multi_image';
        $this->add(array(
            'name' => $multiImageName,
            'attributes' => array(
                'type' => 'hidden',
                'id' => $multiImageName,
                'class' => ' form-control'
            ),
        ));
        //--End Multi image    

        $this->add(array(
            'name' => 'files',
            'attributes' => array(
                'type' => 'text',
                'id' => 'files',
                'accept' => '.pdf',
                'class' => 'form-control'
            ),
        )); 

        $this->add(array(
            'name' => 'files_result',
            'attributes' => array(
                'type' => 'text',
                'id' => 'files_result',
                'accept' => 'application/pdf',
                'class' => ' form-control'
            ),
        )); 

        $this->add(array(
            'name' => 'subtitle3',
            'attributes' => array(
                'type' => 'text',
                'class' => ' form-control'
            ),
        ));
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'identity',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter title'
                                    ),
                                ),
                            )
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'body',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'subtitle',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'subtitle3',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'slug',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'intro',
                        'required' => false,
            )));
         
            $inputFilter->add($factory->createInput(array(
                        'name' => 'language',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'lang_group',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_edu',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'type',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'status',
                    'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'ordering',
                    'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'featured',
                    'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'views',
                    'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'duration',
                    'required' => false,
            )));

           //-- image ------
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'large_image',
                'required' => false,
            )));

            //----- end large image -------
            //--Multi image
            $multiImageName = 'multi_image';
            $inputFilter->add($factory->createInput(array(
                        'name' => $multiImageName,
                        'required' => false,
            )));
            //--End Multi image
            $inputFilter->add($factory->createInput(array(
                        'name' => 'files',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'files_result',
                        'required' => false,
            )));

            // $inputFilter->add($factory->createInput(array(
            //             'name' => 'csrf',
            //             'validators' => array(
            //                 array(
            //                     'name' => 'csrf'
            //                 )
            //             )
            // )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        $this->inputFilter = $inputFilter;
    }

}
