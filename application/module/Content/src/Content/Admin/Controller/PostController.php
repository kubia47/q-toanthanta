<?php
namespace Content\Admin\Controller;
use Core\Controller\AdminController;
use Zend\EventManager\EventInterface as Event;
use Zend\View\Model\ViewModel;

class PostController extends AdminController {

    public $routerName = 'post';

    public function __construct() {
        $this->modelServiceName = 'Content\Admin\Model\Post';
    }

    public function getForm() {

        $params = $this->getParams();
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Content\Admin\Form\PostForm');
        $route = $this->params()->fromRoute();
        $action = $this->getEvent()->getRouteMatch()->getParam('action');
        if ($this->getRequest()->isGet() && $action == 'add') {
            if($params['lang']){
                $form->get('language')->setValue($params['lang']);
            }
            $form->bind(new \ArrayObject(array('type' => $route['id'])));
        }

        //if ($this->getRequest()->isGet() && $this->params()->fromRoute('id')) {
            //$valueOptions = $form->get('parent_id')->getValueOptions();
            //unset($valueOptions[$route['id']]);
            //$form->get('parent_id')->setValueOptions($valueOptions);
        //}

        return parent::setupForm($form);
    }

    //-----Process action--------
    public function onBeforeListing(Event $e) {
        $params = $e->getParams();
        $model = $this->getModel();

        $myLocale = $this->layout()->myLocale;
        $model->setState('filter.language', $myLocale);
        if ($params) {

            if (!$params['page']) {
                //Reset filter
                $model->setState('filter.identity', '');
                $model->setState('filter.status', '');
                $model->setState('filter.search', '');
                $model->setState('filter.category', '');
                $model->setState('filter.featured', '');
                // $model->setState('filter.language', '');
                //End Reset filter
            }

            $type = $params['type'];
            if ($type) {
                $model->setState('filter.type', $type);
            }
            $cusId = $params['cusId'];
            if($cusId){
                $model->setState('filter.cusId', $cusId);
            }else{
                $model->setState('filter.cusId', '');
            }

            $identity = $params['identity'];
            if ($identity) {
                $model->setState('filter.identity', $identity);
            }

            //--View by lang default
            // $language =  $model->getState('filter.language');
            // if(!$language){
            //     $model->setState('filter.language', $this->getLangCode());
            // }
        }
    }

    public function onBeforeCreate(Event $e) {
        $params = $e->getParams();

        
        $this->processCropImage('image', $params);
        $this->processCropImage('large_image', $params);
        $this->processMultiImage($params);
        //$this->processUploadFile($params, 'videos'); //Upload file video
    }

    public function onBeforeEdit(Event $e) {
        $params = $e->getParams();

        $this->processCropImage('image', $params);
        $this->processCropImage('large_image', $params);
        $this->processMultiImage($params);
        //$this->processUploadFile($params, 'videos'); //Upload file video
    }

    public function onBeforeDelete(Event $e) {
        $params = $e->getParams();
        $this->processDeleteImage(array('image', 'large_image'), $params);
    }
    //-----End Process action-------

    public function classesAction() {
        $this->layout()->page = 'classes';
    }

    public function musterAction() {
        $params = $this->getParams();
        $model = $this->getModel();
        $this->layout()->page = 'muster';

        $classInfos = $model->getItemById($params['id']);

        return new ViewModel(array(
            'classInfos'   => $classInfos,
            'date' => substr($params['date'], 1)
        ));
    }

    public function classInfoAction() {
        $params = $this->getParams();
        $model = $this->getModel();
        $this->layout()->page = 'class-room';

        $classInfos = $model->getItemById($params['id']);
        $classSchedule = $model->getAllItemsToArray(array('class_id' => $params['id'], 'type' => 'content_schedule', 'status' => 1), null, 'schedule asc');

        return new ViewModel(array(
            'classInfos'   => $classInfos,
            'classSchedule'   => $classSchedule,
        ));
    }

    public function addClassAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        unset($params['action']);
        unset($params['controller']);
        unset($params['submit']);

        $params['type'] = 'content_class';
        $params['status'] = 1;

        $return = $model->save($params);

        $this->returnJsonAjax(array('status' => $return['status'], 'message' => '', 'class_id' => $return['id']));
    } 

    public function addStudentAction() {
        $params = $this->getParams();
        $model = $this->getModel();
        $contactModel = $this->getContactModel();
        
        $student = array();
        $student['name'] = $params['name'];
        $student['gender'] = $params['gender'];
        $student['phone'] = $params['phone'];
        $student['class_id'] = $params['class'];
        $student['status'] = 1;

        $return = $contactModel->save($student);

        if ($return['status']) {
            $currentClass = $model->getItemById($params['class']);
            $numStudent = $currentClass['count'] + 1;
            $model->save(array('count' => $numStudent), $params['class']);
        }

        $this->returnJsonAjax(array('status' => $return['status'], 'message' => '', 'student' => $return));
    } 

    public function editStudentAction() {
        $params = $this->getParams();
        $contactModel = $this->getContactModel();

        $student = array();

        // if remove
        if ($params['_action'] && $params['_action'] == 'REMOVE') {
            $student['status'] = 0;
        } else {
            $student['name'] = $params['name'];
            $student['gender'] = $params['gender'];
            $student['phone'] = $params['phone'];
        }

        $return = $contactModel->save($student, $params['id']);

        $this->returnJsonAjax(array('status' => $return['status'], 'message' => '', 'student' => $return));
    }

    public function getClassesAction() {
        $model = $this->getModel();
        $params = $this->getParams();

        $grade = ($params['grade'] && $params['grade'] != -1) ? array('subtitle2' => $params['grade']) : null;

        $classes = $model->getListContentType('content_class', 'subtitle2 ASC, title ASC', 0, 1, $grade);
        $this->returnJsonAjax(array('status' => true, 'message' => '', 'classes' => $classes));
    } 

    public function addScheduleAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $schedule = array();
        $schedule['class_id'] = $params['class_id'];
        $schedule['title'] = $params['title'];
        $schedule['schedule'] = $params['schedule_date'];
        $schedule['type'] = $params['type'];
        $schedule['status'] = 1;

        $start = explode('_', $params['hour_start']);
        $end = explode('_', $params['hour_end']);

        $schedule['start_hour'] = $start[0];
        $schedule['start_minute'] = $start[1];
        $schedule['end_hour'] = $end[0];
        $schedule['end_minute'] = $end[1];

        $result = $model->save($schedule);

        $this->returnJsonAjax(array('status' => true, 'message' => '', 'result' => $result));
    }

    public function removeScheduleAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $model->save(array('status' => 0), $params['id']);
        $this->returnJsonAjax(array('status' => true, 'message' => ''));
    }

    public function studentAction() {
        $params = $this->getParams();
        $contactModel = $this->getContactModel();
        $this->layout()->page = 'student';

        $student = $contactModel->getItem(array('id' => $params['id']));

        return new ViewModel(array(
            'student'   => $student
        ));
    }
}
