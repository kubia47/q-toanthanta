<?php

namespace Vote\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;

class VoteController extends AdminController {

  public function __construct() {
        $this->modelServiceName = 'Vote\Admin\Model\Vote';
    }

    public function onBeforeListing(\Zend\EventManager\EventInterface $e) {
        $params = $e->getParams();
        $model = $this->getModel();
        if ($params) {   
            $model->setState('filter.extension', 'contest');
        }
    }

    public function musterAction() {
        $params = $this->getParams();
        $model = $this->getModel();
        $userLogin = $this->getUserLogin();

        $muster = array();

        $muster['title'] = 'muster';
        $muster['user_id'] = $userLogin['id'];
        $muster['class_id'] = $params['class_id'];
        $muster['muster'] = $params['muster'];
        $muster['muster_date'] = $params['muster_date'];
        $muster['student_id'] = $params['student_id'];
        $muster['type'] = 'muster';
        $muster['status'] = 1;

        if (!$params['id']) {
            $return = $model->save($muster);
        } else {
            $return = $model->save($muster, $params['id']);
        }

        $this->returnJsonAjax(array('status' => $return['status'], 'message' => '', 'muster' => $return));
    }

    public function studentMusterAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $musters = $model->getAllItemsToArray(array('month(muster_date) = '. $params['month'], 'year(muster_date) = '. $params['year'], 'student_id' => $params['student_id']));

        $this->returnJsonAjax(array('status' => true, 'message' => '', 'musters' => $musters));
    }

    public function mustersAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $musters = $model->getAllItemsToArray(array('class_id' => $params['class_id'], 'muster_date' => $params['muster_date']));
        $this->returnJsonAjax(array('status' => true, 'message' => '', 'musters' => $musters));
    }
   
    public function exportAction(){
        $factory = $this->getServiceLocator()->get('ServiceFactory');
        $model = $this->getModel();
        $data = $model->getDataExport();

        require_once VENDOR_INCLUDE_DIR . '/phpoffice/phpexcel/Classes/PHPExcel.php';

        //Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        //Set document properties
        $objPHPExcel->getProperties()->setCreator("Bizzon")
        ->setLastModifiedBy("Bizzon")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription('Bizzon')
        ->setKeywords("office 2007 openxml php")
        ->setCategory('Bizzon');

        //---Set name--   
        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getDefaultStyle()->applyFromArray($style);     
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "VOTE - SHARE - LIST");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true)->setSize(15); //Size of Intro file
        $objPHPExcel->getActiveSheet()->getStyle("A2:K2")->getFont()->setBold(true)->setSize(14); //Set size Head Title
        //--End set name--

        //Add some data
        $indexCellTitle = 2;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$indexCellTitle, 'STT')
        ->setCellValue('B'.$indexCellTitle, 'Id')
        ->setCellValue('C'.$indexCellTitle, 'Contest')
        ->setCellValue('D'.$indexCellTitle, 'UserId')
        ->setCellValue('E'.$indexCellTitle, 'Name')
        ->setCellValue('F'.$indexCellTitle, 'Email')
        ->setCellValue('G'.$indexCellTitle, 'Phone')
        ->setCellValue('H'.$indexCellTitle, 'FacebookUID')
        ->setCellValue('I'.$indexCellTitle, 'Date');
        
        $cell = 0;
        foreach($data as $key => $val){
            $user = $factory->getUser($val['user_id']);
            $cell = 3 + $key;
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $cell, $key + 1)
            ->setCellValue('B' . $cell, $val['id'])
            ->setCellValue('C' . $cell, $val['object_id'])
            ->setCellValue('D' . $cell, $val['user_id'])
            ->setCellValue('E' . $cell, $user['name'])
            ->setCellValue('F' . $cell, $user['email'])
            ->setCellValue('G' . $cell, $user['phone'])
            ->setCellValue('H'. $cell, $user['social_id'])
            ->setCellValue('I'. $cell, $val['created']);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
          
            $objPHPExcel->getActiveSheet()->getStyle('G'. $cell)->getAlignment()->setWrapText(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->getFont()->setSize(13); //Set size content

        //--Set border---
        // $style = array(
        //     'borders' => array(
        //         'allborders' => array(
        //             'style' => \PHPExcel_Style_Border::BORDER_THIN
        //         )
        //     )
        // );
        // $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->applyFromArray($style);
        //--End Set border---

        //Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Contest');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        //--Set protected--
        //$objSheet = $objPHPExcel->getActiveSheet();
        //$objSheet->getProtection()->setSheet(true)->setPassword('contest');
        //--End Set protected--

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Contest_' .time(). '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}
