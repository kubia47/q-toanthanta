<?php

namespace Vote\Front\Controller;

use Core\Controller\FrontController;

class VoteController extends FrontController {
    /*
    * Params:
    * 1. type: votes | shares
    * 2. object_id: id bai du thi
    * 3. user_token (Neu dung api)
    * 4. extension (Neu có, mac dinh la contest)
    */
    public function commentAction() {
        $params = $this->getParams();
        $user = $this->getUserLogin();
        $model = $this->getVoteModel();

        if ($params['post_id'] && $user['id']) {
            $comment = array();
            $comment['user_id'] = $user['id'];
            $comment['object_id'] = $params['post_id'];
            $comment['comment'] = $params['comment'];
            $comment['type'] = 'comment';
            $comment['status'] = 1;

            $return = $model->save($comment);
            if ($return) {
                $this->returnJsonAjax(array('status' => true, 'message' => 'Comment success', 'data' => $return));
            }
        }

        $this->returnJsonAjax(array('status' => false, 'message' => 'Comment failed'));
    }

    public function getcommentAction() {
        $params = $this->getParams();
        $user = $this->getUserLogin();
        $model = $this->getVoteModel();

        if ($params['post_id'] && $user['id']) {
            $comments = $model->getDataComment($params['post_id']);

             $this->returnJsonAjax(array('status' => true, 'message' => 'Success', 'data' => $comments));
        }

        $this->returnJsonAjax(array('status' => false, 'message' => 'Get comment failed'));
    }

    public function voteAction(){
        $params = $this->getParams();

        $actionAPI = true; //Neu goi api thi = true, neu dung tren site bt thì = false
        if($actionAPI){
            $token = $params['user_token'];
            $userModel = $this->getUserModel();
            $curUser = $userModel->getUserByToken($token);
        }else{
            $curUser = $this->getUserLogin();
        }
        
        $type = $params['type'];
        $validType = array('votes','shares');
        if($type && in_array($type,  $validType)){
            $extension = $params['extension'] ? $params['extension'] : 'contest';
            $objectId = $params['object_id'];
            if($curUser){
                $userId = $curUser['id'];
            }
            switch ($type) {
                case 'votes': //Vote  
                        $validUser = $this->checkUserStatus($curUser);
                        if($validUser['status']){
                            $this->saveVote($type, $objectId, $userId, $extension);
                        }else{
                             $this->returnJsonAjax($validUser);
                        }  
                    break;
                default: //Share
                        $this->saveVote($type, $objectId, $userId, $extension);
                    break;
            }        
        }else{
            $this->returnJsonAjax(array('status' => false, 'message' => 'Dữ liệu không hợp lệ!'));
        }
        exit();
    }

    function saveVote($type, $objectId, $userId, $extension){
        $contestModel = $this->getContestModel();
        $voteModel = $this->getVoteModel();
        $voteService = $this->getVoteService();

        $contest = $contestModel->getItemById($objectId);
        if(!$contest['id']){
            $this->returnJsonAjax(array('status' => false, 'message' => 'Bài dự thi không tồn tại!'));
        }

        $typeValid = array('votes','shares');
        if(in_array($type, $typeValid)){
            if($type == 'votes'){
                $isVoted = $voteService->isVoted($objectId, $userId, $type, $extension);
                if ($isVoted) { //Voted
                    $this->returnJsonAjax(array('status' => false, 'message' => 'Bạn đã bình chọn bài dự thi này rồi!'));
                } 

                $title = 'Bình chọn bài dự thi';
                $textSuccess = 'Bạn đã bình chọn thành công!';
            }else{
                $title = 'Chia sẻ bài dự thi';
                $textSuccess = 'Bạn đã chia sẻ thành công!';
            }

            $resultSave = $voteService->saveInfo(array('user_id'=>$userId, 'title'=>$title, 'object_id'=>$objectId, 'type'=>$type, 'ip'=>$this->getIpClient(), 'extension'=>$extension));
            if ($resultSave['status']) {
                $numCount = $voteModel->getCountDataVote($objectId, $type);
                $resutlSave = $contestModel->save(array($type => $numCount), $objectId);//Update count for contest
                if($resutlSave['status']){
                    $this->returnJsonAjax(array('status' => true, 'message' => $textSuccess));
                }else{
                    $this->returnJsonAjax(array('status' => false, 'message' => 'Đã có lỗi xảy ra, vui lòng thử lại!'));
                }
            } else {
                $this->returnJsonAjax(array('status' => false, 'message' => 'Đã có lỗi xảy ra, vui lòng thử lại!'));
            }
        }else {
            $this->returnJsonAjax(array('status' => false, 'message' => 'Vui lòng thử lại!'));
        }
        
    }

}
