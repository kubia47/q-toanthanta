<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Vote\Front\Controller\Vote' => 'Vote\Front\Controller\VoteController',
            'Vote\Admin\Controller\Vote' => 'Vote\Admin\Controller\VoteController',
        ),
    ),
    'router' => array(
        'routes' => array(
            //----FRONT------
           'vote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vote[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Front\Controller\Vote',
                        'action' => 'vote',
                    ),
                ),
            ),
            'share' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/share[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Front\Controller\Vote',
                        'action' => 'share',
                    ),
                ),
            ),
            'comment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/comment[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Front\Controller\Vote',
                        'action' => 'comment',
                    ),
                ),
            ),
            'get-comment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-comment[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Front\Controller\Vote',
                        'action' => 'getcomment',
                    ),
                ),
            ),
            //----END FRONT------

            //----ADMIN------
            'vote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/api/muster[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Admin\Controller\Vote',
                        'action' => 'muster',
                    ),
                ),
            ),
            'vote-admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/vote[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Vote\Admin\Controller\Vote',
                        'action'     => 'index',
                    ),
                ),
            ),
            'vote-list' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/musters[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Admin\Controller\Vote',
                        'action'     => 'musters',
                    ),
                ),
            ),
            'student-muster' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/student-muster[/]',
                    'defaults' => array(
                        'controller' => 'Vote\Admin\Controller\Vote',
                        'action'     => 'studentMuster',
                    ),
                ),
            ),
            //----END ADMIN------
        ),
    ),
     'view_manager' => array(
        'template_map' => array(
            'vote' => WEB_ROOT . '/application/module/Vote/src/Vote/Front/View/vote/vote/vote.phtml',
        ),      
    )
);
