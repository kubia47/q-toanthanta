<?php
namespace Contact;
use Contact\Front\Form\ContactForm;

class Module {

    public function getAutoLoaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Contact\Front\Form\ContactForm' => function($sm) {
                    $form = new ContactForm();
                    return $form;
                },
                'Contact\Front\Model\Contact' => function($sm) {
                    
                    $contact = new Front\Model\Contact();
                    return $contact;
                },   
                        
                'Contact\Admin\Form\ContactForm' => function($sm) {
                    $form = new Admin\Form\ContactForm();
                    return $form;
                },
                'Contact\Admin\Model\Contact' => function($sm) {
                    
                    $contact = new Admin\Model\Contact();
                    return $contact;
                },           
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
               
            )
        );
    }

}

?>
