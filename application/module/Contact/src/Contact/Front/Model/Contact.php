<?php

namespace Contact\Front\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Core\Model\AppModel;
use Zend\Db\Sql\Select;

class Contact extends AppModel {

    public $table = 'bz1_contacts';
    public $context = 'contact';

}