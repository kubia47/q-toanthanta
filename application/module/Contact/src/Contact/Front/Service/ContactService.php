<?php

namespace Contact\Front\Service;

class ContactService implements \Zend\EventManager\EventManagerAwareInterface, \Zend\ServiceManager\ServiceLocatorAwareInterface {

    protected $serviceLocator;
    protected $eventManager;

    public function __construct() {
        
    }
    
    public function getLatestPosts($type, $limit, $status = null){  
         $options = array(
            'wheres' => array('type'=>$type),
            'order' => array('created desc'),
            'limit'=> $limit
        );
         if($status!==null){
             $options['wheres']['status'] = $status;
         }
        $items = $this->getPostMapper()->find('all', $options);
        return $items;
        
    }

    public function getPopularPosts($type, $limit, $status = null){
        
         $options = array(
            'wheres' => array('type'=>$type),
            'order' => array('views desc'),
            'limit'=> $limit
             
        );
        if($status!==null){
             $options['wheres']['status'] = $status;
        }
        
        $items = $this->getPostMapper()->find('all', $options);
        return $items;
        
    }
    
    public function getPostsByUser($userId, $type, $limit, $status = null){
        
         $options = array(
            'wheres' => array('parent_id' => '0', 'type'=>'page'),
            'order' => array('ordering' => 1),
            'limit'=> 5
             
        );
        $items = $this->getPostMapper()->find('all', $options);
        return $items;
        
    }

    function getPostMapper() {
        return $this->getServiceLocator()->get('Content\Front\Model\Post');
    }

    public function getEventManager() {
        return $this->eventManager;
    }

    public function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

}
