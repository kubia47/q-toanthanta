<?php

namespace Contact\Front\Block\Latest;

use Block\Front\Block\Type\AbstractType;

class Latest extends AbstractType {

    public function getDefaultParams() {
        return array('template' => 'contact/block/latest');
    }

    public function getData() {
        
        $categoryService = $this->getServiceLocator()->get('CategoryService');
        $tree = $categoryService->getTreeData();
        return $tree;
    }

}
