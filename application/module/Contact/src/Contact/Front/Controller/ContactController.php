<?php

namespace Contact\Front\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\FrontController;

class ContactController extends FrontController {

    //--Lien he
    public function contactAction() {
        $this->setMetaData(array(), $this->translate('Lien he'));
        $this->layout()->page = 'contact';

        //IS POST AJAX
        if($this->isAjax()){
            $params = $this->getParams('post');
            $resultValid = $this->validateContact($params);
            if($resultValid['status']){

               

                $params['created'] = date('Y-m-d H:i:s');
                $params['status'] = 0;
                $params['lang_code'] = $this->getLangCode();
                $params['type'] = 'normal';
                $return = $this->getContactModel()->save($params);
                if ($return['status']) {
                     $this->returnJsonAjax(array('status'=>true, 'message'=>"Đăng ký thành công"));

                    //$this->sendEmail(); //TODO:...
                    //return $this->redirectToUrl('contact/success');
                }
            }else{
                $this->returnJsonAjax($resultValid);
            }
        }

        return new ViewModel(array());   
    }

    private function sendEmail($data){
        $mail = $this->getServiceLocator()->get('SendMail');
        $mail->send(array(
            'from' => array('name' => EMAIL_SEND_FROM_NAME, 'email' => EMAIL_SEND_FROM_EMAIL),
            'to' => array('name' => EMAIL_ADMIN_NAME, 'email' => EMAIL_ADMIN_EMAIL),
            'subject' => '[BZ CMS Custom] thông báo',
            'template' => 'email/contact',
            'data' => $data
        ));
    }

    private function validateContact($params){
        if($params){
            if($params['name']==''){
                return array('status'=>false, 'el'=>'name', 'message'=>"Please enter your name");
            }

            if($params['company']==''){
                return array('status'=>false, 'el'=>'company', 'message'=>"Please enter your company name");
            }

            if($params['address']==''){
                return array('status'=>false, 'el'=>'address', 'message'=>"Please enter your address");
            }

            $phone = $params['phone'];
            if($phone == ''){
                return array('status'=>false, 'el'=>'phone', 'message'=>"Please enter your phone number");
            }else{
                if (!is_numeric($phone)) {
                    return array('status' => false, 'el' => 'phone', 'message' => $this->translate('Phone is number'));
                }

                $minLen = 10;
                if (strlen($phone) < $minLen ) {
                    $strTrans = "Phone number must be at least 10 digits";
                    $strTrans = str_replace('%d', $minLen, $strTrans);
                    return array('status' => false, 'el' => 'phone', 'message' => $strTrans);
                }

                $maxLen = 13;
                if (strlen($phone) > $maxLen) {
                    $strTrans = "Phone number has most 13 digits";
                    $strTrans = str_replace('%d', $maxLen, $strTrans);
                    return array('status' => false, 'element' => 'phone', 'message' => $strTrans);
                }
            }

            $email = $params['email'];
            if($email == ''){
                return array('status'=>false, 'el'=>'email', 'message'=> "Please enter your email");
            }else{
                $validator = new \Zend\Validator\EmailAddress();
                if (!$validator->isValid($email)) {
                     return array('status'=>false, 'el'=>'email', 'message'=>$this->translate('Email not correct'));
                }
            }




        }else{
            return array('status'=>false, 'message'=>"Data not valid");
        }

        return array('status'=>true, 'message'=>$this->translate('Data not valid'));
    }

    public function contactsuccessAction() {
        $this->setMetaData(array(), 'Contact Success');
        return new ViewModel(array());
    }
    //--End Lien he

    public function apicontactsaveAction() {
        $params = $this->getParams();
        $params['volunteer_type'] = implode(", ", $params['volunteer_type']);

        unset($params['controller']);
        unset($params['action']);

        $return = $this->getContactModel()->save($params);

        if ($return['status']) {
            $this->returnJsonAjax(array('status'=>true, 'message'=>"Đăng ký thành công"));

        }else{
            $this->returnJsonAjax(array('status'=>false, 'message'=>"Xảy ra lỗi vui lòng thử lại"));
        }


    }

    public function contactProfileAction() {
        $params = $this->getParams();
        $userLogin = $this->getUserLogin();
        $userModel = $this->getUserModel();

        if (isset($params['first_name'])) {
            $data = array(
                'name' => $params['first_name'].' '.$params['last_name'],
                'first_name' => $params['first_name'],
                'last_name' => $params['last_name'],
                'phone' => $params['phone'],
                'social_token' => $params['social_token']
            );

            if ($_FILES['upload_avatar']) {
                $fileSize = $_FILES["upload_avatar"]["size"];
                $fileType = strtolower($_FILES["upload_avatar"]["type"]);
                $fileName = $_FILES["upload_avatar"]["name"];
                $fileTempName = $_FILES["upload_avatar"]["tmp_name"];

                $newfileName= time().'_'.$fileName;

                /* Upload file */
                if ($fileSize < 1048576 && $fileType == "image/jpeg" || $fileType == "image/png") {
                    if(move_uploaded_file($fileTempName, "media/images/".$newfileName)){
                       $data['avatar'] = $newfileName;
                    }
                } else {

                }
                
            }

            $result = $userModel->save($data, $userLogin->id);
            if ($result['status']) {
                $userLogin['name'] = $result['item']['name'];
                $userLogin['first_name'] = $result['item']['first_name'];
                $userLogin['last_name'] = $result['item']['last_name'];
                $userLogin['phone'] = $result['item']['phone'];
                $userLogin['avatar'] = $result['item']['avatar'];

                return $this->redirect()->toRoute('user-profile', array());
            }
        }

        return new ViewModel(array(
            'userLogin' => $userLogin
        ));
    }

}
