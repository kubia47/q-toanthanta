<?php

namespace Contact\Admin\Model;
use Zend\Db\Sql\Select;
use Core\Model\AppModel;

class Contact extends AppModel {

    public $table = 'bz1_contacts';
    public $context = 'contact';
    
    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);

        $published = $this->getUserStateFromRequest('filter.status', 'filter_status', '');
        $this->setState('filter.status', $published);
        parent::populateState();
    }

    public function getDefaultListQuery() {
        $select = new Select($this->table);
        $status = $this->getState('filter.status');
        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }
        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            $select->where(" (". $this->table.".id like '%$keyword%' or ". $this->table.".title like '%$keyword%' or ". $this->table.".name like '%$keyword%' or ". $this->table.".email like '%$keyword%' or ". $this->table.".phone like '%$keyword%' or ". $this->table.".company like '%$keyword%' or ". $this->table.".object_id like '%$keyword%') ");
        }

        //Filter by type
        $type = $this->getState('filter.type');
        if ($type) {
            $select->where(array('type' => $type));
        }

        //Filter by Object id
        $objectId= $this->getState('filter.object_id');
        if ($objectId) {
            $select->where(array('object_id' => $objectId));
        }

        //order
        $filter_order = $this->getState('order.field');
        $filter_order_dir = $this->getState('order.direction');

        if (!empty($filter_order)) {
            $select->order($filter_order . " " . $filter_order_dir);
        }else{
              $select->order("created DESC");
        }


        //print $select->getSqlString(); die;
        $_SESSION['select_export'] = $select;
        return $select;
    }

    public function getDataExport(){
        $select = $_SESSION['select_export'];
        if ($select) {
            $select->limit(10000000);
            return  $this->selectWith($select);
        }

        return  null;
    }

    public function getConditionContacts($class_id, $month, $year) {
        $select = new Select($this->table);

        $select->join(array('C' => 'bz1_contests'), 'C.contact_id='.$this->table.'.id', array('class_id', 'month', 'year', 'charge'));

        $select->where(array('C.class_id' => $class_id, 'month' => $month, 'year' => $year, 'C.status' => 1));

        $result = $this->selectWith($select);

        if ($result) {
            $result = $result->toArray();
        }

        return $result;
    }

}