<?php

namespace Contact\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;
use Zend\EventManager\EventInterface as Event;

class ContactController extends AdminController {

    public function getForm() {
        if (empty($this->form)) {
            $this->form = $this->getServiceLocator()->get('FormElementManager')->get('Contact\Admin\Form\ContactForm');
        }
        return parent::setupForm($this->form);
    }

    public function getModelServiceName() {
        $this->modelServiceName = 'Contact\Admin\Model\Contact';
        return $this->modelServiceName;
    }

    //-----Process action--------
    public function onBeforeListing(Event $e) {
        $params = $e->getParams();
        if ($params) {
            $model = $this->getModel();

            if (!$params['page']) {
                //Reset filter
                $model->setState('filter.object_id', '');
                //End Reset filter
            }

            //Filter by type
            $type = $params['type'];
            if ($type) {
                $model->setState('filter.type', $type);
            }

            //Filter by objectId
            $objectId = $params['object_id'];
            if ($objectId) {
                $model->setState('filter.object_id', $objectId);
            }
        }
    }

    public function onBeforeCreate(Event $e) {
        $params = $e->getParams();
    }

    public function onBeforeEdit(Event $e) {
        $params = $e->getParams();
    }

    //-----End Process action-------
    //--Khi export co image thi can zip image (admin/contest/export?type=zip) va download ve de cung folder voi file excel nay
    public function exportAction(){
        $params = $this->getParams();
        $type = $params['type'];

        $this->processExport();  /*Export with no image*/
    }


    private function processExport(){
        require_once VENDOR_INCLUDE_DIR . '/phpoffice/phpexcel-1.8.1/Classes/PHPExcel.php';
        $factory = $this->getServiceLocator()->get('ServiceFactory');
        $model = $this->getModel();
        $data = $model->getDataExport();
        $ageArr = array('Không xác định', '18 - 25 tuổi', '26 - 35 tuối', '36 - 45 tuổi', '46 trở lên');



        //Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        //Set document properties
        $objPHPExcel->getProperties()->setCreator("BzCMS")
            ->setLastModifiedBy("BzCMS")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription('BzCMS')
            ->setKeywords("office 2007 openxml php")
            ->setCategory('BzCMS');

        //---Set name--
        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getDefaultStyle()->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Danh Sách Nhân Viên");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true)->setSize(15); //Size of Intro file
        $objPHPExcel->getActiveSheet()->getStyle("A2:K2")->getFont()->setBold(true)->setSize(14); //Set size Head Title
        //--End set name--

        //Add some data
        $indexCellTitle = 2;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$indexCellTitle, 'STT')
            ->setCellValue('B'.$indexCellTitle, 'Tên')
            ->setCellValue('C'.$indexCellTitle, 'Địa Chỉ')
            ->setCellValue('D'.$indexCellTitle, 'Điện thoại')
            ->setCellValue('E'.$indexCellTitle, 'Tuổi')
            ->setCellValue('F'.$indexCellTitle, 'Email')
            ->setCellValue('G'.$indexCellTitle, 'Lựa chọn')
            ->setCellValue('H'.$indexCellTitle, 'Nội dung')
            ->setCellValue('I'.$indexCellTitle, 'Thời gian')
            ->setCellValue('J'.$indexCellTitle, 'Tần Suất')
            ->setCellValue('K'.$indexCellTitle, 'Nhận xét');

        $cell = 0;
        foreach($data as $key => $val){
            $user = $factory->getUser($val['user_id']);
            $cell = 3 + $key;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $cell, $key + 1)
                ->setCellValue('B' . $cell, $val['name'])
                ->setCellValue('C' . $cell, $val['address'])
                ->setCellValue('D' . $cell, $val['phone'])
                ->setCellValue('E' . $cell, $ageArr[$val['age']])
                ->setCellValue('F' . $cell, $val['email'])
                ->setCellValue('G' . $cell, $val['volunteer_type'])
                ->setCellValue('H' . $cell, $val['message'])
                ->setCellValue('I' . $cell, $val['volunteer_time'])
                ->setCellValue('J' . $cell, $val['offen_message'])
                ->setCellValue('K' . $cell, $val[' evaluate_message'])


               ;
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

            $objPHPExcel->getActiveSheet()->getStyle('I'. $cell)->getAlignment()->setWrapText(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->getFont()->setSize(13); //Set size content

        //--Set border---
        // $style = array(
        //     'borders' => array(
        //         'allborders' => array(
        //             'style' => \PHPExcel_Style_Border::BORDER_THIN
        //         )
        //     )
        // );
        // $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->applyFromArray($style);
        //--End Set border---

        //Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Users');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        //--Set protected--
        //$objSheet = $objPHPExcel->getActiveSheet();
        //$objSheet->getProtection()->setSheet(true)->setPassword('contest');
        //--End Set protected--

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Users_' .time(). '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
    //--End Export file zipfile image----
    public function getAllStudentAction() {
        $model = $this->getModel();
        $students = $model->getAllItemsToArray(array(), null, 'id asc');

        $this->returnJsonAjax(array('status' => true, 'message' => '', 'students' => $students));
    }

    public function getStudentAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $students = $model->getAllItemsToArray(array('class_id' => $params['class'], 'status' => 1), null, 'id asc');
        $this->returnJsonAjax(array('status' => true, 'message' => '', 'students' => $students));
    }

    public function chargedStudentAction() {
        $params = $this->getParams();
        $model = $this->getModel();

        $students = $model->getConditionContacts($params['class_id'], $params['month'], $params['year']);

        $this->returnJsonAjax(array('status' => true, 'message' => '', 'students' => $students));
    }
    
}

