<?php

namespace Contact\Admin\Form;

use Core\Form\CoreForm;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ContactForm extends CoreForm implements InputFilterAwareInterface {

    protected $inputFilter;

    public function __construct() {
        parent::__construct('n_contact');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate form-horizontal panel ');
    }

    public function init() {
        parent::init();
        $this->addElements();
    }

    function addElements() {

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
                'class' => 'required'
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required form-control'
            ),
        ));

        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required email form-control'
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'class'=>'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'required form-control'
            ),
        ));

        $this->add(array(
            'name' => 'offen_message',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'required form-control'
            ),
        ));
        $this->add(array(
            'name' => 'volunteer_type',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'required form-control'
            ),
        ));
        $this->add(array(
            'name' => 'volunteer_time',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'required form-control'
            ),
        ));
        $this->add(array(
            'name' => 'age',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => array('0' => 'Không xác định','1' => '18 - 25 tuổi','2' => '26 - 35 tuối','3' => '36 - 45 tuổi','4' => '46 trở lên'),

            ),
            'attributes' => array(
                'class'=>' form-control',
            )
        ));



       // $this->add(array(
       //      'type' => 'Zend\Form\Element\Csrf',
       //      'name' => 'csrf',
       //      'options' => array(
       //          'csrf_options' => array(
       //              'timeout' => 600
       //          )
       //      )
       //  ));
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter name'
                                    ),
                                ),
                            )
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'company',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'address',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter email'
                                    ),
                                ),
                            )
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter message'
                                    ),
                                ),
                            )
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'offen_message',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter message'
                            ),
                        ),
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'volunteer_type',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter message'
                            ),
                        ),
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'volunteer_time',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter message'
                            ),
                        ),
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'age',
                    'required' => false,
                )
            ));

            // $inputFilter->add($factory->createInput(array(
            //             'name' => 'csrf',
            //             'validators' => array(
            //                 array(
            //                     'name' => 'csrf'
            //                 )
            //             )
            // )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        $this->inputFilter = $inputFilter;
    }

}
