<?php

// module/Contact/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Contact\Front\Controller\Contact' => 'Contact\Front\Controller\ContactController',
            'Contact\Admin\Controller\Contact' => 'Contact\Admin\Controller\ContactController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(

            //--*****Block front--**********
            'contact-vi_VN' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/lien-he[/]',
                    'defaults' => array(
                        'controller' => 'Contact\Front\Controller\Contact',
                        'action'     => 'contact',
                    ),
                ),
            ),
            'contact-en_US' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/contact[/]',
                    'defaults' => array(
                        'controller' => 'Contact\Front\Controller\Contact',
                        'action'     => 'contact',
                    ),
                ),
            ),
            'get-students' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/students[/]',
                    'defaults' => array(
                        'controller' => 'Contact\Admin\Controller\Contact',
                        'action'     => 'getStudent',
                    ),
                ),
            ),
            'get-all-students' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/all-students[/]',
                    'defaults' => array(
                        'controller' => 'Contact\Admin\Controller\Contact',
                        'action'     => 'getAllStudent',
                    ),
                ),
            ),
            'get-charged-students' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/api/charged-students[/]',
                    'defaults' => array(
                        'controller' => 'Contact\Admin\Controller\Contact',
                        'action'     => 'chargedStudent',
                    ),
                ),
            ),
            //--*****End Block front--**********
            //****************************************************************************************************
            //--*****Block admin--**********
            'contactadmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/contact[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Contact\Admin\Controller\Contact',
                        'action'     => 'index',
                    ),
                ),
            ),
            //--*****End Block admin--**********
            
        ),
    ),
    'view_manager'=>array(
        'template_map'=>array(
            'contact/block/latest'   => __DIR__ . '/../src/Contact/Front/View/contact/block/latest/default.phtml',
        )
    )
);
