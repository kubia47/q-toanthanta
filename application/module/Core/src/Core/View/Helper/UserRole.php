<?php
namespace Core\View\Helper;

use Zend\Http\Request;
use Zend\View\Helper\AbstractHelper;

//$this->baseUrl() in view file
class UserRole extends AbstractHelper {
    public function UserRolePermission($user, $permission) {

        switch ($permission) {
            case 'customer_manage': if($user['customer_manage']) return true; break;
            case 'customer_role': if($user['customer_role']) return true; break;
            case 'product_manage': if($user['product_manage']) return true; break;
            case 'product_role': if($user['product_role']) return true; break;
            case 'employee_manage': if($user['employee_manage']) return true; break;
            case 'history_order_manage': if($user['history_order_manage']) return true; break;
            case 'history_order_role': if($user['history_order_role']) return true; break;
            case 'history_exchange_manage': if($user['history_exchange_manage']) return true; break;
            case 'history_exchange_role': if($user['history_exchange_role']) return true; break;
            default: return false; break;
        }
    }
}