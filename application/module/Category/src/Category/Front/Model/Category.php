<?php

namespace Category\Front\Model;

use Core\Model\AppModel;

class Category extends AppModel {

    public $table = 'bz1_terms';
    public $context = 'term';

    public function getChildCategories($parentId, &$result, $level) {

        $childData = $this->getChildCategoriesData($parentId);

    //        if($idx!=null){
    //            $result[$idx]['childs'] = $childData;
    //        }else{
    //            $result['childs'] = $childData;
    //        }
            // print_r($childData);

        if ($childData) {
            foreach ($childData as $idx => $child) {

                $childId = $child['id'];
                    // $result[]['level'] = $level+1;
                $childXs = $this->getChildCategories($childId, $child, $level + 1);
                $result['child'][] = $childXs;
            }
        }
        return $result;
    }

    public function getChildCategoriesData($parentId) {

      $options = array(
        'wheres'=>array('parent_id' => $parentId),
        'order'=> array('ordering' => 1)
        );
      $childData = $this->find('all',$options);

      return $childData;
    }

    public function getCategoryById($category_id) {
    	$options = array(
	        'wheres'=>array('id' => $category_id)
	        );
	      $childData = $this->find('all',$options);

	      return $childData;
    }
}