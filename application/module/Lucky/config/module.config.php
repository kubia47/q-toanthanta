<?php

// module/Prize/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Lucky\Front\Controller\Prize' => 'Lucky\Front\Controller\PrizeController',
            'Lucky\Admin\Controller\Prize' => 'Lucky\Admin\Controller\PrizeController',
            'Lucky\Admin\Controller\Lucky' => 'Lucky\Admin\Controller\LuckyController',
            
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            
            'prizeadmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/prize[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Lucky\Admin\Controller\Prize',
                        'action'     => 'index',
                    ),
                ),
            ),
            'luckyadmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/lucky[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Lucky\Admin\Controller\Lucky',
                        'action'     => 'index',
                    ),
                ),
            ),
            
        ),
    ),
   
);
