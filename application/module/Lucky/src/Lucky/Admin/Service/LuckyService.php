<?php

namespace Lucky\Admin\Service;

class LuckyService implements \Zend\EventManager\EventManagerAwareInterface, \Zend\ServiceManager\ServiceLocatorAwareInterface {

    protected $serviceLocator;
    protected $eventManager;

    public function __construct() {
        
    }

    public function getEventManager() {
        return $this->eventManager;
    }

    public function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    function getUserModel() {
        return $this->getServiceLocator()->get('User\Front\Model\User');
    }

    function getPrizeModel() {
        return $this->getServiceLocator()->get('Lucky\Admin\Model\Prize');
    }

    function getLuckyModel() {
        return $this->getServiceLocator()->get('Lucky\Admin\Model\Lucky');
    }

    function getSettingService() {
        return $this->getServiceLocator()->get('SettingService');
    }

    public function doLucky($userid, $data, $limit = true, $limitWeek = false, $limitDay = false) {
        $lucky_mapp = $this->getLuckyModel();
        if ($limitWeek) {
            $data_lucky_in_week = $lucky_mapp->isLuckyInWeek($userid);
            if (count($data_lucky_in_week)) {
                $result['message'] = "Bạn đã trúng giải trong tuần này rồi!";
                $result['saved'] = false;
                $result['status'] = false;
                $this->doSaveRandom($userid, 0, $data);
                return $result;
            }
        }
        if ($limitDay) {
            $data_lucky_in_day = $lucky_mapp->isLuckyInday($userid);
            if (count($data_lucky_in_day)) {
                $result['message'] = "Bạn đã trúng giải trong ngày rồi!";
                $result['saved'] = false;
                $result['status'] = false;
                $this->doSaveRandom($userid, 0, $data);
                return $result;
            }
        }
        $city = trim($data['city']);

        $probability_constant = $this->getProbabilityPrice(); //Độ khó của giải
        $prizes = $this->updatePrizeInformation($probability_constant);

        $pick_prize_index = $this->pickPrizeIndex($prizes, $city);
        $prize = $prizes[$pick_prize_index];
        $result['prize'] = $prize;
        $result['status'] = $this->doRandom($userid, $result['prize'], $limit); // limit random = true or false
        if ($result['status']) {
            $resultLucky = $this->doSaveRandom($userid, $prize['id'], $data);
            $result['saved'] = $resultLucky['status'];
            $result['lucky_code'] = $resultLucky['lucky_code'];
            $result['prize_name'] = $prize['title'];
        } else {
            $this->doSaveRandom($userid, 0, $data);
            $result['saved'] = false;
        }

        return $result;
    }

    private function pickPrizeIndex($prizes, $city) {
        $prizes_tmp = array();
        foreach ($prizes as $k => $v) {
            $i = 1;
            $be_included = 1;
            $be_excluded = 0;
            if (is_array($prizes[$k]['include_location']) && count($prizes[$k]['include_location'])) {
                $be_included = in_array($city, $prizes[$k]['include_location']);
            }
            if (is_array($prizes[$k]['exclude_location']) && count($prizes[$k]['exclude_location'])) {
                $be_excluded = in_array($city, $prizes[$k]['exclude_location']);
            }
            if ($be_included && !$be_excluded) {
                if ($prizes[$k]['is_possible_this_time'] > 0) {
                    for ($i = 1; $i <= ceil($v['real_quantity_per_day']); $i++) {
                        $prizes_tmp[] = array("prize_index" => $k);
                    }
                }
            }
        }

        $pick_prize_data = array_rand($prizes_tmp); // Lấy giải
        $pick_prize_index = $prizes_tmp[$pick_prize_data]['prize_index'];

        return $pick_prize_index;
    }

    private function updatePrizeInformation($probability_constant) {
        $hours_remain_today = 24 - date("H"); // Số giờ còn lại        
        $prizes = $this->processRemaning($prizes, $hours_remain_today, $probability_constant, 0);
        return $prizes;
    }

    private function getProbabilityPrice() {
        $probability_constant = 1; // tỉ lệ nghịch với độ khó    
        $settingService = $this->getSettingService(); //TODO: do again when you have free time
        $probabilityConstant = $settingService->getByIdentity('probability_constant');
        if (isset($probabilityConstant)) { // Độ khó của mỗi giải             
            $probability_constant = intval(trim($probabilityConstant));
            $probability_constant /= 100;
        }

        return $probability_constant;
    }

    public function updateFullPrizeInformation() {
        $probability_constant = $this->getProbabilityPrice(); //Độ khó của giải
        $hours_remain_today = 24 - date("H"); // Số giờ còn lại        
        $prizes = $this->processRemaning($prizes, $hours_remain_today, $probability_constant, 1);
        return $prizes;
    }

    private function processRemaning($prizes, $hours_remain_today, $probability_constant, $show = 0) {
        $prizemapp = $this->getPrizeModel();
        if ($show) {
            $prizes = $prizemapp->getAll();
        } else {
            $prizes = $prizemapp->getAvailablePrize(); // Lấy giải nào còn số lượng và nằm trong thời gian cho phép
        }

        foreach ($prizes as $k => $v) {
            $schedule = $prizes[$k]['schedule'];
            $prizes[$k]['time_remain'] = $this->calculate_real_time_remain($v['from_date'], $v['to_date'], $v['from_date'], $schedule); // ngày còn lại
            $prizes[$k]['quantity_per_day'] = $v['quantity'] / $prizes[$k]['time_remain']; // số lượng giải trong ngày       
            $prizes[$k]['real_time_remain'] = $this->calculate_real_time_remain($v['from_date'], $v['to_date'], date("Y-m-d G:i:s"), $schedule); // ngày còn lại thực tế
            if ($prizes[$k]['real_time_remain'] <= 0) {
                $prizes[$k]['real_time_remain'] = 0;
                $prizes[$k]['real_quantity_per_day'] = 0;
            } else
                $prizes[$k]['real_quantity_per_day'] = $v['remaining'] - (($prizes[$k]['real_time_remain'] - 1) * $prizes[$k]['quantity_per_day']);

            if ($prizes[$k]['real_quantity_per_day'] < 0)
                $prizes[$k]['real_quantity_per_day'] = 0;
            $prizes[$k]['probability_today'] = ($prizes[$k]['real_quantity_per_day'] / $hours_remain_today) * $probability_constant;

            $is_possible_this_time = $this->scheduleFilter($schedule, $prizes[$k]['id']);

            if ($show) {
                $prizes[$k]['probability_today'] *= $is_possible_this_time;
                if ($prizes[$k]['probability_today'] > 1)
                    $prizes[$k]['probability_today'] = 1;
                $prizes[$k]['probability_today'] *=100;
                $prizes[$k]['probability_today'] = round($prizes[$k]['probability_today']);
                $prizes[$k]['probability_today'] .="%";
            }else {
                $prizes[$k]['is_possible_this_time'] = $is_possible_this_time;
                if (trim($prizes[$k]['include_location'])) {
                    $include_location = explode(",", $prizes[$k]['include_location']);
                    foreach ($include_location as $k1 => $v1) {
                        $include_location[$k1] = trim($v1);
                    }
                    $prizes[$k]['include_location'] = $include_location;
                }
                if (trim($prizes[$k]['exclude_location'])) {
                    $exclude_location = explode(",", $prizes[$k]['exclude_location']);
                    foreach ($exclude_location as $k1 => $v1) {
                        $exclude_location[$k1] = trim($v1);
                    }
                    $prizes[$k]['exclude_location'] = $exclude_location;
                }
            }
        }
        return $prizes;
    }

    private function calculate_real_time_remain($from_date, $to_date, $cur_date, $schedule) {
        $cur_date = date("Y-m-d 00:00:00", strtotime($cur_date));
        if (strtotime($cur_date) < strtotime($from_date)) {
            return 0;
        }
        $schedule = json_decode($schedule, true);
        if (!$schedule)
            return ceil((strtotime($to_date) - strtotime($cur_date)) / 86400);
        else {
            $remain_days = ceil((strtotime($to_date) - strtotime($cur_date)) / 86400);

            $lucky_days = 0;
            if ($remain_days) {
                $today = date("N", strtotime($cur_date));
                foreach ($schedule['day'] as $day) {
                    if ($day['dayofweek'] == $today) {
                        if ($day['lucky']) {
                            $lucky_days++;
                        }
                        break;
                    }
                }
                for ($i = 1; $i <= $remain_days; $i++) {
                    $cur_date = date("Y-m-d G:i:s", strtotime($cur_date . ' + 1 day'));

                    $today = date("N", strtotime($cur_date));
                    foreach ($schedule['day'] as $day) {
                        if ($day['dayofweek'] == $today) {
                            if ($day['lucky']) {
                                $lucky_days++;
                            }
                            break;
                        }
                    }
                }
            }
            if ($lucky_days > $remain_days)
                $lucky_days = $remain_days;
            return $lucky_days;
        }
    }

    private function doSaveRandom($userid, $prize, $data) {
        $luckyMapper = $this->getLuckyModel();
        $lucky_data = array(
            'user_id' => $userid,
            'prize_id' => $prize,
            'created' => date('Y-m-d G:i:s')
        );

        $luckyCode = $this->generateLuckyCode();
        if ($prize > 0) {
            $lucky_data['lucky_code'] = $luckyCode;
        }

        if ($data) {
            $lucky_data = array_merge($lucky_data, $data);
        }

        $savedlucky = $luckyMapper->save($lucky_data);

        $updatedlucky = 0;
        if (intval($prize) > 0) { //TODO: Set prize for user
            $userModel = $this->getUserModel();
            $entry = array('id' => $userid, 'is_lucky' => 1);
            $updatedlucky = $userModel->edit($entry);
        }

        if ($prize && $savedlucky && $updatedlucky) {
            $prizemapp = $this->getPrizeModel();
            $prizemapp->decrease(array('id' => $prize), 'remaining');
            return array('status' => true, 'lucky_code' => $luckyCode, 'Trúng may mắn!');
        }

        return array('status' => false, 'lucky_code' => '', 'Không trúng may mắn!');
    }

    public function generateLuckyCode($lenCode = 6) {
        $characters = array(
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
            "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $keys = array();

        while (count($keys) < $lenCode) {
            $x = mt_rand(0, count($characters) - 1);
            if (!in_array($x, $keys)) {
                $keys[] = $x;
            }
        }

        foreach ($keys as $key) {
            $random_chars .= $characters[$key];
        }

        $luckyMapper = $this->getLuckyModel();
        if (!$luckyMapper->isLuckyCodeExist($random_chars)) {
            return $random_chars;
        } else {
            $this->generateLuckyCode();
        }
    }

    private function doRandom($userid, $prize, $limit = true) {

        $probability_today_update = $prize['probability_today'];
        $schedule = $prize['schedule'];
        $prize_limit = $prize['limit'];
        $prize_id = $prize['id'];
        $is_possible_this_time = $this->scheduleFilter($schedule, $prize['id']);

        if ($userid && $is_possible_this_time > 0) {
            $probability_today_update *= $is_possible_this_time;
            //check user đã trúng giải may mắn lần nào chưa

            $luckyMapper = $this->getLuckyModel();
            $hasLuckyBefore = 0;
            $hasLuckyThisPrizeBefore = 0;
            if ($limit)
                $hasLuckyBefore = $luckyMapper->isLuckyBefore($userid);
            if ($prize_limit)
                $hasLuckyThisPrizeBefore = $luckyMapper->isLuckyThisPrizeBefore($userid, $prize_id);
            if (!$hasLuckyBefore && !$hasLuckyThisPrizeBefore) {
                if ($probability_today_update >= 1) {
                    return $this->preventRandom(true);
                } else if ($probability_today_update > 0.7 && $probability_today_update < 1) {
                    return $this->extremlyEasyRandom();
                } else if ($probability_today_update > 0.4 && $probability_today_update <= 0.7) {
                    return $this->easyRandom();
                } else if ($probability_today_update > 0 && $probability_today_update <= 0.4) {
                    return $this->hardRandom();
                } else if ($probability_today_update <= 0) {
                    return $this->preventRandom(false);
                } else {
                    return $this->hardRandom();
                }
            }
        }

        return false;
    }

    private function scheduleFilter($schedule, $prize_id) {
        $schedule = json_decode($schedule, true);
        $today = date("N");
        $current_hour = date("H");
        $current_minute = date("i");
        foreach ($schedule['day'] as $day) {
            if ($day['dayofweek'] == $today) {
                if (!$day['lucky']) {
                    return 0;
                } else {
                    $luckyMapper = $this->getLuckyModel();
                    if ($day['limit_max_quantity'] && ($day['limit_max_quantity'] <= $luckyMapper->countLuckyIndayByPrize_id($prize_id))) {
                        return 0;
                    }

                    foreach ($day['hours'] as $hour) {
                        if ($current_hour == $hour['hourofday'] && $current_minute >= $hour['from_minute'] && $current_minute <= $hour['to_minute']) {
                            if (!$hour['lucky'])
                                return 0;
                            else {
                                if ($hour['limit_max_quantity'] && $hour['limit_max_quantity'] <= $luckyMapper->countLuckyInhourByPrize_id($prize_id)) {
                                    return 0;
                                }
                                return (intval($hour['probability_constant']) * intval($day['probability_constant']) / 10000);
                            }
                            break;
                        }
                    }
                }
                break;
            }
        }
        return 0;
    }

    private function preventRandom($status) {
        if ($status) {
            return $status;
        }
        return false;
    }

    private function hardRandom() {
        $time = strtotime(date("Y-m-d H:i:s"));
        $number = 10;
        $sum = 0;

        for ($i = 0; $i < 10; $i++) {
            $value = $time % $number;
            $sum += $value;
            $time = floor($time / $number);
            $number *= 10;
        }

        $number_random = array(17, 19, 23, 29, 31, 37, 41, 43);
        $index = array_rand($number_random);
        $ran = $number_random[$index];
        if ($sum % $ran == 0) {
            return true;
        } else {
            return false;
        }
    }

    private function easyRandom() {
        $random = rand(1, 15);
        //if (in_array($random, array(5, 4, 3, 2, 1, 6, 7, 8, 9, 10))) {
        if ($random > 12) {
            return true;
        } else {
            return false;
        }
    }

    private function extremlyEasyRandom() {
        $ran = rand(1, 6);
        if ($ran >= 3) {
            return true;
        } else {
            return false;
        }
    }

    public function generateSchedule() {
        $return = array();
        for ($i = 0; $i <= 6; $i++) {
            $day = '';
            switch ($i) {
                case 0: $day = "Monday";
                    break;
                case 1: $day = "Tuesday";
                    break;
                case 2: $day = "Wednesday";
                    break;
                case 3: $day = "Thursday";
                    break;
                case 4: $day = "Friday";
                    break;
                case 5: $day = "Saturday";
                    break;
                case 6: $day = "Sunday";
                    break;
                default: $day = "Sunday";
            }
            $return['day'][$day]['dayofweek'] = $i + 1;
            $return['day'][$day]['lucky'] = 1;
            $return['day'][$day]['limit_max_quantity'] = 0;
            $return['day'][$day]['probability_constant'] = 100;
            for ($j = 0; $j <= 23; $j++) {
                $return['day'][$day]['hours'][$j]['hourofday'] = $j;
                $return['day'][$day]['hours'][$j]['lucky'] = 1;
                $return['day'][$day]['hours'][$j]['limit_max_quantity'] = 0;
                $return['day'][$day]['hours'][$j]['from_minute'] = 0;
                $return['day'][$day]['hours'][$j]['to_minute'] = 60;
                $return['day'][$day]['hours'][$j]['probability_constant'] = 100;
            }
        }
        echo json_encode($return);
        exit;
    }

}
