<?php

namespace Lucky\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;

class PrizeController extends AdminController {

    public $routerName = 'prize';

    public function getForm() {
        if (empty($this->form)) {
            $this->form = $this->getServiceLocator()->get('FormElementManager')->get('Lucky\Admin\Form\PrizeForm');
        }
        return parent::setupForm($this->form);
    }

    public function getModelServiceName() {
        $this->modelServiceName = 'Lucky\Admin\Model\Prize';
        return $this->modelServiceName;
    }

    public function indexAction() {
        $model = $this->getModel();
        $params = $this->getParams();
        $model->setParams($params);
        $callAction = @$params['callaction'];
        $this->getEventManager()->trigger('onBeforeListing', $this, $params);
        if (trim($callAction)) {
            if (method_exists($this, $callAction)) {
                $return = call_user_func(array($callAction, $this));
                if ($callAction == 'deleteitem') {
                    $return = $this->deleteitem();
                } else {
                    if ($callAction == 'publish') {
                        $return = $this->publish();
                    } else {
                        if ($callAction == 'unpublish') {
                            $return = $this->unpublish();
                        }
                    }
                }

                if (is_array($return) && $return['message']) {
                    $this->addMessage($return['message']);
                }
            } elseif (method_exists($model, $callAction)) {
                $return = call_user_func(array($callAction, $model));
                if (is_array($return) && $return['message']) {
                    $this->addMessage($return['message']);
                }
            } else {
                throw new \Exception('Method Not Exists!');
            }
        }
        $items = $model->getItems();
        if ($items && $items instanceof \Traversable) {
            $items = iterator_to_array($items);
        }

        $prizeService = $this->getServiceLocator()->get('LuckyService');
        $items = $prizeService->updateFullPrizeInformation(); //$this->getEventManager()->prepareArgs($items);
        //print_r($items); exit;
        $this->getEventManager()->trigger('onListing', $this, $items);
        return new ViewModel(array(
            'items' => $items,
            'paging' => $model->getPaging(),
            'state' => $model->getStateObject()
        ));
    }

    public function getscheduleAction() {
        $prizeService = $this->getServiceLocator()->get('LuckyService');
        $prizeService->generateSchedule();
        exit();
    }

}
