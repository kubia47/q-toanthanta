<?php

namespace Lucky\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;

class LuckyController extends AdminController {

    public $routerName = 'lucky';

    public function getForm() {
        if (empty($this->form)) {
            $this->form = $this->getServiceLocator()->get('FormElementManager')->get('Lucky\Admin\Form\LuckyForm');
        }
        return parent::setupForm($this->form);
    }

    public function getModelServiceName() {
        $this->modelServiceName = 'Lucky\Admin\Model\Lucky';
        return $this->modelServiceName;
    }

    public function getPrizeModel() {
        return $this->getServiceLocator()->get('Lucky\Admin\Model\Prize');
    }

    public function indexAction() {
        $model = $this->getModel();
        $params = $this->getParams();
        $model->setParams($params);
        $callAction = @$params['callaction'];
        $this->getEventManager()->trigger('onBeforeListing', $this, $params);
        if (trim($callAction)) {
            if (method_exists($this, $callAction)) {
                $return = call_user_func(array($callAction, $this));
                if ($callAction == 'deleteitem') {
                    $return = $this->deleteitem();
                } else {
                    if ($callAction == 'publish') {
                        $return = $this->publish();
                    } else {
                        if ($callAction == 'unpublish') {
                            $return = $this->unpublish();
                        }
                    }
                }

                if (is_array($return) && $return['message']) {
                    $this->addMessage($return['message']);
                }
            } elseif (method_exists($model, $callAction)) {
                $return = call_user_func(array($callAction, $model));
                if (is_array($return) && $return['message']) {
                    $this->addMessage($return['message']);
                }
            } else {
                throw new \Exception('Method Not Exists!');
            }
        }
        $items = $model->getItems();
        if ($items && $items instanceof \Traversable) {
            $items = iterator_to_array($items);
        }
        $items = $this->getEventManager()->prepareArgs($items);
        $this->getEventManager()->trigger('onListing', $this, $items);
        return new ViewModel(array(
            'items' => $items,
            'paging' => $model->getPaging(),
            'state' => $model->getStateObject(),
            'controller' => $this
        ));
    }

    public function getPrize($id) {
        $prizeModel = $this->getPrizeModel();
        $prize = $prizeModel->getById($id);
        return $prize;
    }

    public function exportAction() {
        include_once VENDOR_INCLUDE_DIR . '/excelwriter/excelwriter.inc.php';

        $luckyModel = $this->getModel();
        $data = $luckyModel->getItems();
        $data = $data->toArray();

        $filename = 'lucky-' . date('d-m-Y') . ".xls";

        $file = WEB_ROOT . '/media/tmp/' . $filename;
        $excel = new \ExcelWriter($file);

        if ($excel == false) {
            echo $excel->error;
        }
        $row_title = array(' ', 'List lucky ');
        $row_space = array(' ');

        $excel->writeLine($row_title);
        $excel->writeLine($row_space);


        $arrayVal = $arrayVal = array('STT', 'ID', 'Họ và tên', 'Điện thoại', 'Prize', 'Lucky code', 'Lucky date');
        $excel->writeLine($arrayVal);

        $i = 1;
        foreach ($data as $value) {
            $prize = $this->getPrize($value['prize_id']);
            $arrayVal = array($i, $value['id'], $value['user_name'], $value['user_phone'], $prize ? $prize['title'] : '---', $value['lucky_code'], $value['created']);
            $excel->writeLine($arrayVal);
            $i++;
        }

        $excel->close();
        $filesize = filesize($file);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=$filename");
        readfile($file);
        exit();
    }

}
