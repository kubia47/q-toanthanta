<?php

namespace Lucky\Admin\Form;

use Core\Form\CoreForm;
use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Form\Element\Select;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PrizeForm extends CoreForm implements InputFilterAwareInterface {

    protected $inputFilter;

    public function __construct() {
        parent::__construct('n_prize');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate ');
        $this->setAttribute('class', 'validate form-horizontal panel ');
    }

    public function init() {
        parent::init();
        $this->addElements();
    }

    private function addElements() {
        $factory = $this->getServiceLocator()->get('ServiceFactory');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required form-control',
                'id' => 'title'
            ),
        ));


        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'class'=>' form-control'
            ),
        ));
        
        $this->add(array(
            'name' => 'quantity',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quantity',
                'class'=>' form-control'
            ),
        ));
        
        $this->add(array(
            'name' => 'remaining',
            'attributes' => array(
                'type' => 'text',
                'id' => 'remaining',
                'class'=>' form-control'
            ),
        ));
        
         $this->add(array(
            'name' => 'schedule',
            'attributes' => array(
                'type' => 'textarea',
                'id'=>'schedule',
                'rows'=>'10',
                'class'=>' form-control'
                
            ),
        ));
        
        $this->add(array(
            'name' => 'from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'from_date',
                'class'=>'datepicker  form-control',
                'data-date-format'=>'yyyy-mm-dd 00:00:00'
            ),
        ));
        
         $this->add(array(
            'name' => 'to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'to_date',
                'class'=>'datepicker form-control',
                'data-date-format'=>'yyyy-mm-dd 23:59:59'
            ),
        ));
         
         $this->add(array(
            'name' => 'include_location',
            'attributes' => array(
                'type' => 'text',
                'id' => 'include_location',
                'class'=>' form-control'
            ),
        ));
         
         $this->add(array(
            'name' => 'exclude_location',
            'attributes' => array(
                'type' => 'text',
                'id' => 'exclude_location',
                'class'=>' form-control'
            ),
        ));
         
         $this->add(array(
            'name' => 'limit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'limit',
                'class'=>' form-control'
            ),
        ));


      
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                         'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter title'
                                    ),
                                ),
                            )
                        ),
            )));

            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'description',
                        'required' => false,
            )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quantity',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'remaining',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'schedule',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'from_date',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'to_date',
                        'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'include_location',
                        'required' => false,
            )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_location',
                        'required' => false,
            )));
               $inputFilter->add($factory->createInput(array(
                        'name' => 'limit',
                        'required' => false,
            )));



            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        $this->inputFilter = $inputFilter;
    }

}
