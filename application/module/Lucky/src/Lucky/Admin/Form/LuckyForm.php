<?php

namespace Lucky\Admin\Form;

use Core\Form\CoreForm;
use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Form\Element\Select;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LuckyForm extends CoreForm implements InputFilterAwareInterface {

    protected $inputFilter;

    public function __construct() {
        parent::__construct('n_lucky');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('class', 'validate form-horizontal panel ');
        
        
        
    }
    public function init() {
        parent::init();
        $this->addElements();
        
    }
    private function addElements(){
         $factory = $this->getServiceLocator()->get('ServiceFactory');
         
          $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
                
            ),
        ));
          
         $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required',
                'id'=>'title'
            ),
        ));

       
        $this->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
            ),
        ));

       

            $this->add(array(
                'name' => 'language',
                'type' => 'Zend\Form\Element\Select',
                'options' => array(
                    'value_options' => $factory->getLanguage()
                )
            ));

            $this->add(array(
                'name' => 'status',
                'type' => 'Zend\Form\Element\Select',
                'options' => array(
                    'value_options' => $factory->getStatus()
                )
            ));

    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            
             $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required'=>false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
             
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
          
            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required'=>false,
                       
                    )));
              $inputFilter->add($factory->createInput(array(
                        'name' => 'language',
                        'required'=>false,
                       
                    )));
             
            
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        $this->inputFilter = $inputFilter;
    }

}