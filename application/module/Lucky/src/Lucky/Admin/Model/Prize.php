<?php

namespace Lucky\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;

class Prize extends AppModel {

    public $table = 'bz1_prizes';
    public $context = 'prize';

    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);


        $orderField = $this->getUserStateFromRequest('order.field', 'order_field', '');
        $this->setState('order.field', $orderField);

        $orderDir = $this->getUserStateFromRequest('order.direction', 'order_direction', '');
        $this->setState('order.direction', $orderDir);


        parent::populateState();
    }

    public function getDefaultListQuery() {

        $select = new Select($this->table);

        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            $select->where("title like '%$keyword%'");
        }
        //order
        $filter_order = $this->getState('order.field', 'created');
        $filter_order_dir = $this->getState('order.direction', 'desc');
        if (!$filter_order) {
            $filter_order = 'created';
            $filter_order_dir = 'desc';
        }
        if (!empty($filter_order)) {
            $select->order($filter_order . " " . $filter_order_dir);
        }

        // print $select->getSqlString();exit;
        return $select;
    }

    public function getAll($isAll = false) {
        $select = new Select($this->table);
        if (!$isAll) {
            $select->where(array('status' => 1));
        }

        //print $select->getSqlString();exit;
        $result = $this->selectWith($select);
        if ($result) {
            return $result->toArray();
        }
        return array();
    }

    public function getAvailablePrize() {
        $select = new Select($this->table);
        $select->where(array('status > 0', 'status = 1', 'from_date <= "' . date("Y-m-d G:i:s") . '"', 'to_date >= "' . date("Y-m-d G:i:s") . '"'));
        //print $select->getSqlString();exit;
        $result = $this->selectWith($select);
        if ($result) {
            return $result->toArray();
        }
        return array();
    }

    public function getById($id) {
        $select = new Select($this->table);
        $select->where(array('id' => $id));
        $select->limit(1);
        $result = $this->selectWith($select);
        if ($result) {
            $item = $result->toArray();
            return $item[0];
        }
        return array();
    }

}
