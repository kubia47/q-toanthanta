<?php

namespace Lucky\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Like;

class Lucky extends AppModel {

    public $table = 'bz1_lucky';
    public $context = 'lucky';

    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);

        $fromdate = $this->getUserStateFromRequest('filter.fromdate', 'filter_from_date', '');
        $this->setState('filter.fromdate', $fromdate);

        $todate = $this->getUserStateFromRequest('filter.todate', 'filter_to_date', '');
        $this->setState('filter.todate', $todate);

        $orderField = $this->getUserStateFromRequest('order.field', 'order_field', '');
        $this->setState('order.field', $orderField);

        $orderDir = $this->getUserStateFromRequest('order.direction', 'order_direction', '');
        $this->setState('order.direction', $orderDir);

        $prizeId = $this->getUserStateFromRequest('filter.prize_id', 'filter_prize_id', '');
        $this->setState('filter.prize_id', $prizeId);

        $site = $this->getUserStateFromRequest('filter.site', 'filter_site', '');
        $this->setState('filter.site', $site);

        parent::populateState();
    }

    public function getDefaultListQuery() {
        $table = $this->table;
        $select = new Select($table);
        $select->join('bz1_users', 'bz1_users.id=' . $table . '.user_id', array('user_name' => 'name', 'user_phone' => 'phone', 'user_license_b2' => 'license_b2', 'user_site' => 'site'));
        //$select->join('bz1_prizes', 'bz1_prizes.id=' . $table . '.prize_id', array('prize_name' => 'title'));

        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            $select->where("bz1_users.name like '%$keyword%' or bz1_users.phone like '%$keyword%' or bz1_users.license_b2 like '%$keyword%' or " . $table . ".lucky_code like '%$keyword%'");
        }

        $fromdate = $this->getState('filter.fromdate');
        if (!empty($fromdate)) {
            $select->where(array($table . '.created >= "' . $fromdate . ' 00:00:00"'));
        }

        $todate = $this->getState('filter.todate');
        if (!empty($todate)) {
            $select->where(array($table . '.created <= "' . $todate . ' 23:59:59"'));
        }


        $prizeId = $this->getState('filter.prize_id');
        if (isset($prizeId)) {
            $select->where(array($table . '.prize_id' => $prizeId));
        }

        $site = $this->getState('filter.site');
        if (!empty($site)) {
            $select->where(array('bz1_users.site' => $site));
        }

        //order
        $filter_order = $this->getState('order.field', $table . '.created');
        $filter_order_dir = $this->getState('order.direction', 'desc');
        if (!$filter_order) {
            $filter_order = $table . '.created';
            $filter_order_dir = 'desc';
        }
        if (!empty($filter_order)) {
            $select->order($filter_order . " " . $filter_order_dir);
        }

//        print $select->getSqlString();
//        exit;

        return $select;
    }

    public function isLuckyCodeExist($code) {
        $select = new Select($this->table);
        $select->where(array('lucky_code' => $code));
        $select->limit(1);
        $result = $this->selectWith($select);
        if ($result) {
            if (count($result->toArray()) > 0) {
                return true;
            }
        }
        return false;
    }

    public function isLuckyBefore($uid) {
        $select = new Select($this->table);
        $select->where(array('user_id' => $uid));
        $select->limit(1);
        $result = $this->selectWith($select);
        if ($result) {
            if (count($result->toArray()) > 0) {
                return true;
            }
        }
        return false;
    }

    public function isDuplicateBill($data) {
        $select = new Select($this->table);
        $select->where(array('bill_code' => $data['bill_code'], 'bill_date' => $data['bill_date'], 'mart_name' => $data['mart_name'], 'stall_number' => $data['stall_number']));
        $select->limit(1);
        $result = $this->selectWith($select);
        if ($result) {
            if (count($result->toArray())) {
                return true;
            }
        }
        return false;
    }

    public function isLuckyThisPrizeBefore($uid, $prize_id) {
        $select = new Select($this->table);
        $select->where(array('user_id' => $uid, 'prize_id' => $prize_id));
        $select->limit(1);
        $result = $this->selectWith($select);
        if ($result) {
            if (count($result->toArray())) {
                return true;
            }
        }
        return false;
    }

    private function getWeekDates($year, $week, $start = true) {
        $from = date("Y-m-d", strtotime("{$year}-W{$week}-1")); //Returns the date of monday in week
        $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week

        if ($start) {
            return $from;
        } else {
            return $to;
        }
        //return "Week {$week} in {$year} is from {$from} to {$to}.";
    }

    public function isLuckyInWeek($user_id) {
        $fromTime = $this->getWeekDates(date("Y"), date("W"), true) . " 00:00:00";
        $toTime = $this->getWeekDates(date("Y"), date("W"), false) . " 23:59:59";

        $select = new Select($this->table);
        $select->where(array('created >= "' . $fromTime . '"', 'created <= "' . $toTime . '"', 'prize_id <> 0', 'user_id' => $user_id));
        $result = $this->selectWith($select);
        if ($result) {
            return $result->toArray();
        }
        return array();
    }

    public function isLuckyInday($user_id) {
        $fromTime = date('Y-m-d 00:00:00');
        $toTime = date('Y-m-d 23:59:59');

        $select = new Select($this->table);
        $select->where(array('created >= "' . $fromTime . '"', 'created <= "' . $toTime . '"', 'prize_id <> 0', 'user_id' => $user_id));
        $result = $this->selectWith($select);
        if ($result) {
            return $result->toArray();
        }
        return array();
    }

    public function countLuckyIndayByPrize_id($prize_id) {
        $fromTime = date('Y-m-d 00:00:00');
        $toTime = date('Y-m-d 23:59:59');

        $select = new Select($this->table);
        $select->where(array('created  >= "' . $fromTime . '"', 'created <= "' . $toTime . '"', 'prize_id' => $prize_id));
//        print $select->getSqlString();
//        exit;
        $result = $this->selectWith($select);
        if ($result) {
            return count($result->toArray());
        }
        return 0;
    }

    public function countLuckyInhourByPrize_id($prize_id) {
        $fromTime = date('Y-m-d G:00:00');
        $toTime = date('Y-m-d G:59:59');

        $select = new Select($this->table);
        $select->where(array('created >= "' . $fromTime . '"', 'created <= "' . $toTime . '"', 'prize_id' => $prize_id));
        $result = $this->selectWith($select);
        if ($result) {
            return count($result->toArray());
        }
        return 0;
    }

}
