<?php

namespace Lucky;

use Lucky\Front\Form\PrizeForm;
use Lucky\Admin\Service;

class Module {

    public function getAutoLoaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Lucky\Admin\Form\PrizeForm' => function($sm) {
                    $form = new Admin\Form\PrizeForm();
                    return $form;
                },
                'Lucky\Admin\Model\Prize' => function($sm) {
                    $model = new Admin\Model\Prize();
                    return $model;
                },
                'Lucky\Admin\Form\LuckyForm' => function($sm) {
                    $form = new Admin\Form\PrizeForm();
                    return $form;
                },
                'Lucky\Admin\Model\Lucky' => function($sm) {
                    $model = new Admin\Model\Lucky();
                    return $model;
                },
                'LuckyService' => function($sm) {
                    $service = new Service\LuckyService();
                    return $service;
                },
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            )
        );
    }

}

?>
