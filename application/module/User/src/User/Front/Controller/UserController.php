<?php

namespace User\Front\Controller;

use Core\Controller\FrontController;
use Zend\Session\Container;
use Core\Auth\Adapter\Db;
use App\HashUtil;

class UserController extends FrontController {

    const METHOD_POST_PARAM = '';


    public function loginAction(){
        $params = $this->getParams();
        $email = $params["email"];
        $password = $params["password"];
        if($email == ''){
            $this->returnJsonAjax(array('status' => false, 'message' => "Vui lòng nhập email!"));
        }

        if($password == ''){
            $this->returnJsonAjax(array('status' => false, 'message' => "Vui lòng nhập mật khẩu!"));
        }

        if($email && $password){

            $return = $this->auth($password,$email);
            $this->returnJsonAjax($return); //Response Data   
        }
    }

    public function registerAction(){

    }


    //--******************** API **************************:

    //--Customer--
    public function apiCustomerAction(){
        $params = $this->getParams(self::METHOD_POST_PARAM);
        $userModel = $this->getUserModel();

        if(isset($params['_action'])){
            if($params['_action'] == 'SEARCH_CUSTOMER'){
                // search customer with keyword
                $userModel->setState('filter.type', 'customer');

                if(isset($params['_searchkey'])){
                    $userModel->setState('filter.keyword', $params['_searchkey']);
                }else{
                    $userModel->setState('filter.keyword', false);
                }

                $customerResults = $userModel->getItems();
                $customerResults = $customerResults->toArray();

                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$customerResults));
            }

            if($params['_action'] == 'SEARCH_EMPLOYEE'){
                // search customer with keyword
                $userModel->setState('filter.type', 'employee');

                if(isset($params['_searchkey'])){
                    $userModel->setState('filter.keyword', $params['_searchkey']);
                }else{
                    $userModel->setState('filter.keyword', false);
                }

                $customerResults = $userModel->getItems();
                $customerResults = $customerResults->toArray();

                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$customerResults));
            }
            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }else{
            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }


    }

    //--Employee--
    public function apiEmployeeAction(){
        $params = $this->getParams(self::METHOD_POST_PARAM);
    }

    //--Register--
    public function apiregisterAction(){
        $params = $this->getParams(self::METHOD_POST_PARAM);
        $resultValid = $this->validateRegister($params);
        if($resultValid['status']){
            $curUser = $resultValid['curUser'];
            $userModel = $this->getUserModel();

            $birthday = strtotime( $params['datetimepicker'] );
            $dayofbirth = date( 'Y-m-d H:i:s', $birthday );
            $data = array(

                'name' => $params['first_name'].' '.$params['last_name'],
                'first_name' => $params['first_name'],
                'last_name' => $params['last_name'],
                'email' => $params['email'],
                'phone' => $params['phone'],
                'password' => $params['password'],
                'class' => $params['class'],
                'gender' => $params['gender'],
                'birthday' => $dayofbirth,
                'ip' => $this->getIpClient(),
                'status' => 1
                );

            //--Process save info---
            if(!$curUser){ //User normal
                $data['social_type'] = 'Normal';
                $data['side'] = 'website';
            }else{ //User register by fb
                $userId = $curUser['id'];
            }
            //--End Process save info---

            $return = $userModel->register($data, $userId);
            if ($return['status']) {

                $user = $userModel->getUser($return['id']);
                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$this->apiProcessUser($user)));
            }else{
                $this->returnJsonAjax(array('status'=>false,'message'=>'Đã xảy ra lỗi, vui lòng thử lại sau!'));
            }
        }else{
            $this->returnJsonAjax($resultValid);
        }

        $this->returnJsonAjax(array('status'=>false,'message'=>'Dữ liệu không hợp lệ!'));
    }

    private function validateRegister($params){
        if($params){
            $userModel = $this->getUserModel();

            $email = $params['email'];
            if($email == ''){
                return array('status'=>false, 'message'=>'Vui lòng nhập email!');
            }else{
                $validator = new \Zend\Validator\EmailAddress();
                if (!$validator->isValid($email)) {
                   return array('status'=>false, 'message'=>'Email không đúng định dạng!');
               } 


                $isExist = $userModel->isEmailExists($email);
                if($isExist['status']){
                    return array('status'=>false, 'message'=>'Email đã tồn tại!');
                }

        }






                $pass = $params['password'];
                if($pass == ''){
                    return array('status'=>false, 'message'=>'Vui lòng nhập mật khẩu!');
                }

        }else{
           return array('status'=>false, 'message'=>'Dữ liệu không hợp lệ!');
       }
       return array('status'=>true, 'message'=>'Dữ liệu hợp lệ!','curUser'=>$params);
   }

   public function apiverifysmsAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $token = $params['user_token'];
    $mobileCode = $params['mobile_code'];

    $userModel = $this->getUserModel();
    $user = $userModel->getUserByToken($token);
    if($user){
        $userId = $user['id'];  
        $resultValid = $this->checkUserStatus($user);
        if(!$resultValid['status'] && $resultValid['status_key'] == 'inactivated'){
            if(strtolower(trim($mobileCode)) == strtolower(trim($user['mobile_code']))){
                $active = $userModel->active($userId);
                if($active['status']){

                        //-Token login--
                    $this->createdUserToken($userId, $userModel);
                        //-End Token login--

                    $user = $userModel->getUser($userId);
                    $this->returnJsonAjax(array('status'=>true,'message'=>'Kích hoạt tài khoản thành công!','data'=>$this->apiProcessUser($user)));
                }else{
                    $this->returnJsonAjax($active);
                }
            }else{
                $this->returnJsonAjax(array('status' => false, 'message' => 'Mã kích hoạt không hợp lệ!'));
            }
        }else{
            if($resultValid['status']){
                $this->returnJsonAjax(array('status' => false, 'message' => 'Tài khoản của bạn đã được kích hoạt!'));
            }else{
                $this->returnJsonAjax($resultValid);
            }
        } 
    }else{
       $this->returnJsonAjax(array('status' => false, 'message' => 'Bạn không thể kích hoạt tài khoản!'));
   }
}

public function apiverifyresendsmsAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $phone = $params['phone'];
    $token = $params['user_token'];
    if($token){
        $userModel = $this->getUserModel();
        $user = $userModel->getUserByToken($token);
        if($user){
            $mobileCode = $user['mobile_code'];
                //--Validate--
            if(!$mobileCode && $user['status'] == 1){
               $this->returnJsonAjax(array('status' => false, 'message' => 'Tài khoản của bạn đã được kích hoạt!'));
           }

           if(!$mobileCode && $user['status'] == 0){
               $this->returnJsonAjax(array('status' => false, 'message' => 'Tài khoản của bạn đã được kích hoạt nhưng đã bị khóa!'));
           }

           if($phone){
            if (!is_numeric($phone)) {
               $this->returnJsonAjax(array('status' => false, 'message' => 'Số điện thoại phải là một chuối số!'));
           }

           $minLen = 10;
           if (strlen($phone) < $minLen ) {
               $this->returnJsonAjax(array('status' => false, 'message' => 'Số điện thoại phải ít nhất '.$minLen.' ký tự!'));
           }

           $maxLen = 13;
           if (strlen($phone) > $maxLen) {
               $this->returnJsonAjax(array('status' => false, 'message' => 'Số điện thoại phải nhiều nhất '.$maxLen.' ký tự!'));
           }

           $phoneFix0 = '0' . $this->phoneFix($phone);
           $phoneFix84 = '84' . $this->phoneFix($phone);
           if($user['phone'] != $phoneFix0 && $user['phone'] != $phoneFix84){
            $check = $userModel->isExists('phone', $phoneFix0);
            $check1 = $userModel->isExists('phone', $phoneFix84);
            if ($check['status'] || $check1['status']) {
               $this->returnJsonAjax(array('status' => false, 'el' => 'phone', 'message' => 'Số điện thoại này đã có người sử dụng để đăng ký trong hệ thống!'));
           }
       } 
   }
                //--End Validate--

                //--Resend sms---
   $limitResend = 2;
   if($user['verify_resend'] < $limitResend){
    $phone = $phone ? $phone : $user['phone'];
                    $isSend = $this->sendSMS($phone, $mobileCode); //Send SMS
                    if($isSend){
                        $userModel->increase(array('id' => $user['id']), 'verify_resend');
                        $this->returnJsonAjax(array('status'=>true, 'message'=>'Gửi lại tin nhắn thành công!'));
                    }  
                }else{
                   $this->returnJsonAjax(array('status'=>false, 'message'=>'Bạn chỉ được gửi lại tin nhắn tối đa '.$limitResend.' lần!')); 
               }
                //--End Resend sms---
           }else{
               $this->returnJsonAjax(array('status'=>false, 'message'=>'Bạn cần đăng ký tài khoản!')); 
           }
       }else{
           $this->returnJsonAjax(array('status'=>false, 'message'=>'Dữ liệu không hợp lệ!')); 
       }
   }

   private function sendSMS($phone, $message){
    return true;
}
    //--End register

    //--Login facebook
public function apiloginfbAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $accessToken = $params['access_token'];
    if ($params && $accessToken) {
        $config = array('appId' => APP_ID,'secret' => APP_SECRET,'cookie' => true);

            //giãn thời gian hết hạn của token lên 60 ngày.
        $extend_token_url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" . $config['appId'] . "&client_secret=" . $config['secret'] . "&fb_exchange_token=" . $accessToken;
        $extend_response = file_get_contents($extend_token_url);
        $extend_params = null;
        parse_str($extend_response, $extend_params);
        $accessTokenRefresh = $extend_params['access_token'];
        $graph_url = "https://graph.facebook.com/me?fields=id,name,email&access_token=" .  $accessTokenRefresh;
        $user = json_decode(file_get_contents($graph_url));
        $user->token =  $accessTokenRefresh;
        $return = array();

        if ($user->id) {
            $userModel = $this->getUserModel();
            $userExist = $userModel->getUserByFbUid($user->id);
            if ($userExist['social_id']) {
                $userLogin = $userExist;
            } else {
                $userLogin = $this->fbcomplete($user);
            }

                //--Check user valid--
            $resultValid =  $this->checkUserStatus($userLogin);
            if($resultValid['status'] || $resultValid['status_key'] == 'reg_fb' ){
                if($resultValid['status']){
                        //-Token login--
                    $token = $this->createdUserToken($userLogin['id'],$userModel);
                    if($token){
                        $userLogin['token'] = $token;
                    }
                        //-End Token login--
                }

                $userLoginFix = $this->apiProcessUser($userLogin);
                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng nhập thành công!','data'=>$userLoginFix));
            }else{
                $this->returnJsonAjax($resultValid); 
            }
                //--End check user valid
        }else{
           $this->returnJsonAjax(array('status' => false, 'message' => 'Đăng nhập bằng Facebook thất bại!'));
       }
   }else{
    $this->returnJsonAjax(array('status' => false, 'message' => 'Dữ liệu không hợp lệ!')); 
}

$this->returnJsonAjax(array('status' => false, 'message' => 'Đã có lỗi xảy ra, vui lòng thử lại!'));
}

public function fbcomplete($data)
{
    $userinfo = array();
    if ($data) {
        $model = $this->getUserModel();

        $urlAvatarFB = 'http://graph.facebook.com/' . $data->id . '/picture?width=280&height=280';
        $email = $data->email;
        $userinfo['social_token'] = $data->token;
        $userinfo['social_id'] = $data->id;
            $userinfo['social_link'] = $data->link; //save link
            $userinfo['gender'] = $data->gender == 'male' ? 'Male' : 'Female'; //gender
            $userinfo['social_picture'] = $urlAvatarFB;
            $socialType = 'Facebook';

            $userByEmail = get_object_vars($model->getUserByEmail($email));
            if ($userByEmail) { //Exist email
                //$userinfo['activation_code'] = ''; //Auto active
                //$userinfo['mobile_code'] = ''; //Auto active
                //$userinfo['status'] = 1; //Auto active

                $return = $model->save($userinfo, $userByEmail['id']);
            } else { //Not exist email
                $userinfo['social_type'] = $socialType;
                $userinfo['name'] = $data->name;
                $userinfo['first_name'] = $data->first_name;
                $userinfo['last_name'] = $data->last_name;
                $userinfo['avatar'] = $urlAvatarFB;

                $userinfo['email'] = $email;

                $userinfo['role'] = 10;
                $password = HashUtil::randomPassword();
                $userinfo['password'] = HashUtil::createPasswordWithSalt($password);
                $userinfo['created'] = $userinfo['modified'] = date('Y-m-d H:i:s');
                $userinfo['ip'] = $this->getIpClient();
                $userinfo['is_updated_info'] = 0; //Flag update info
                $userinfo['status'] = 0;
                $userinfo['side'] = 'website';
                $return = $model->save($userinfo);
            }

            if ($return['status']) {
                return  $model->getUser($return['id']);
            }
        }

        return array();
    }    
    //End Login facebook

    //--Login thường
    public function apiloginAction(){
        $params = $this->getParams(self::METHOD_POST_PARAM);
        $email = $params["email"];
        $password = $params["password"];
        if($email == ''){
            $this->returnJsonAjax(array('status' => false, 'message' => "Vui lòng nhập email!"));
        }

        if($password == ''){
            $this->returnJsonAjax(array('status' => false, 'message' => "Vui lòng nhập mật khẩu!"));
        }

        if($email && $password){
            $data = array('email' => $email, 'password' => $password);

            $auth = $this->getServiceLocator()->get('FrontAuthService');
            $userModel = $this->getUserModel();
            $siteAuthAdapter = new Db($userModel);

            $siteAuthAdapter->setCredential($data);
            $result = $auth->authenticate($siteAuthAdapter);

            $return = array();
            if ($result->isValid()) {
                $user = $siteAuthAdapter->getIdentity();
                if ($user) {
                    //--Check user valid--
                    $resultValid =  $this->checkUserStatus($user);
                    if($resultValid['status']){
                        //-Token login--
                        $token = $this->createdUserToken($user['id'],$userModel);
                        if($token){
                            $user['token'] = $token;
                        }
                        //-End Token login--

                        // $session = new Container('Zend_Auth');
                        // $session->setExpirationSeconds(5400);

                        $return = array('status' => true, 'message' => "Đăng nhập thành công!!!", 'data' => $this->apiProcessUser($user));
                    }else{
                     $return = $resultValid; 
                 }
                    //--End check user valid        
             } else {
                $return = array('status' => false, 'message' => "Vui lòng đăng nhập lại!", 'data' => "");
            }
        } else {
            $msg = $result->getMessages();
            $return = array('status' => false, 'message' => $msg[0], 'data' => "");
        }

            $this->returnJsonAjax($return); //Response Data   
        }
    }

    //Login by email and password
    private function loginbyemailandpasswordAction($email,$password){
     $data = array('email' => $email, 'password' => $password);

     $auth = $this->getServiceLocator()->get('FrontAuthService');
     $userModel = $this->getUserModel();
     $siteAuthAdapter = new Db($userModel);

     $siteAuthAdapter->setCredential($data);
     $result = $auth->authenticate($siteAuthAdapter);
 }

 private function loginbyuserdata($userLogin,$userModel,$type='Facebook'){
    $userLoginFix = $this->apiProcessUser($userLogin);
    $auth = $this->getServiceLocator()->get('FrontAuthService');
    $siteAuthAdapter = new \Core\Auth\Adapter\Social($userModel, $type);
    $siteAuthAdapter->setCredential($userLogin);
    $result = $auth->authenticate($siteAuthAdapter);
}
   //End login thường

    //Logout--
public function apilogoutAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $userId = $params['user_id'];
    if($userId){
        $userModel = $this->getUserModel();
        $user = $userModel->getUserByUId($userId);
        if($user){
            $authService = $this->getServiceLocator()->get('FrontAuthService');
            $authService->clearIdentity();
            $this->returnJsonAjax(array('status'=>true,'message'=>'Thoát tài khoản thành công!'));
        }else{
            $this->returnJsonAjax(array('status'=>false,'message'=>'Bạn chưa đăng nhập nên không thể thoát!'));
        }
    }else{
        $this->returnJsonAjax(array('status'=>false,'message'=>'Dữ liệu không hợp lệ!'));  
    }
}

    //Logout--
public function logoutAction(){
    $authService = $this->getServiceLocator()->get('FrontAuthService');
    $authService->clearIdentity();
    return $this->redirect()->toRoute('home', array());
}


    //End Logout--

private function createdUserToken($userId, $userModel){
    if(!$userModel){
        $userModel = $this->getUserModel();
    }

    $token = HashUtil::createRandomKey();
    $saveResult = $userModel->save(array('token'=>$token),$userId);
    if($saveResult['status']){
        return $token;
    }
    return '';
}

// Profile

public function apiprofileAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $token = $params['user_token'];
    if($params && $token){
        $userModel = $this->getUserModel();
        $user = $userModel->getUserByToken($token);

            //--Check user status--
        $checkUser = $this->checkUserStatus($user);
        if(!$checkUser['status']){
            return  $this->returnJsonAjax($checkUser); 
        }
            //--End Check user status--

        if($user){
            $item = $this->apiProcessUser($user);
            $this->returnJsonAjax(array('status'=>true,'message'=>'Lấy dữ liệu thành công!','data'=>$item));
        }
    }

    $this->returnJsonAjax(array('status'=>false,'message'=>'Dữ liệu không hợp lệ!'));
}
    //End Profile--

    //--Update info
public function apiupdateprofileAction(){
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $resultValid = $this->validateUpdateProfile($params);
    if($resultValid['status']){
        $userValid = $resultValid['curUser'];

        $userModel = $this->getUserModel();
        $data = array(
            'name' => $params['name'],
            'address'=> $params['address'],
            'is_updated_info' => '1'
            );

            //--Update email, phone if email, phone is null---
        $email = $params['email'];
        if($userValid && !$userValid['email'] && $email){
           $data['email'] = $email;
       }

       $phone = $params['phone'];
       if($userValid && !$userValid['phone'] && $phone){
           $data['phone'] = $phone;
       }
            //--End Update email, phone if email, phone is null---

       $saveResult = $userModel->save($data, $userValid['id']);
       if($saveResult['status']){
        $curUser = $userModel->getUser($userValid['id']);
        $this->returnJsonAjax(array('status'=>true, 'message'=>'Cập nhật thông tin thành công!','data'=>$this->apiProcessUser($curUser)));
    }else{
        $this->returnJsonAjax(array('status'=>false,'message'=>'Đã xảy ra lỗi, vui lòng thử lại sau!'));
    }
}else{
    $this->returnJsonAjax($resultValid);
}

$this->returnJsonAjax(array('status'=>false,'message'=>'Dữ liệu không hợp lệ!'));
}

public function apiupdatepasswordAction()
{
    $params = $this->getParams(self::METHOD_POST_PARAM);

    if (!$params['user_token'])
    {
        echo json_encode(array('status' => false, 'message' => 'Bạn cần đăng nhập mới có thể tiếp tục!'));
        exit;
    }

    $userModel = $this->getUserModel();
    $curUser = $userModel->getUserByToken($params['user_token']);
//        echo "".$curUser['password']."<pre>";
//        print_r($params);
//        exit;
    if(!HashUtil::verifyPasswordWithSalt($params['password'],$curUser['password'])){
        echo json_encode(array('status' => false, 'message' => 'Mật khẩu cũ không chính xác!'));
        exit;
    }

    $data = array(
        'password' => HashUtil::createPasswordWithSalt($params['newpassword']),
        );


    $saveResult = $userModel->save($data, $curUser['id']);
    if ($saveResult['status'])
    {
        $curUser = $userModel->getUser($curUser['id']);
        $this->returnJsonAjax(array('status' => true, 'message' => 'Cập nhật thông tin thành công!', 'data' => $this->apiProcessUser($curUser)));
    } else
    {
        $this->returnJsonAjax(array('status' => false, 'message' => 'Đã xảy ra lỗi, vui lòng thử lại sau!'));
    }


    $this->returnJsonAjax(array('status' => false, 'message' => 'Dữ liệu không hợp lệ!'));
}

private function validateUpdateProfile($params){
    $token = $params['user_token'];
    $name = $params['name'];
    $phone = $params['phone'];
    $email = $params['email'];
    $address = $params['address'];

    $userModel = $this->getUserModel();
    if($token == ''){
        return array('status'=>false,'message'=>'Bạn cần đăng nhập mới có thể tiếp tục!');
    }else{
        $curUser = $userModel->getUserByToken($token);

            //--Check user status--
        $checkUser = $this->checkUserStatus($curUser);
        if(!$checkUser['status']){
            return $checkUser; 
        }
            //--End Check user status--
    }

    if($name == ''){
        return array('status'=>false,'message'=>'Vui lòng nhập họ và tên!');
    }

    if($email){
        $validator = new \Zend\Validator\EmailAddress();
        if (!$validator->isValid($email)) {
           return array('status'=>false, 'message'=>'Email không đúng định dạng!');
       } 

       if($curUser['email'] != $email){
           $isExist = $userModel->isExists('email',$email);
           if($isExist['status']){
            return array('status'=>false,'message'=>'Email đã tồn tại!');
        }
    }
}

if($phone){
    if (!is_numeric($phone)) {
        return array('status' => false, 'message' => 'Số điện thoại phải là một chuối số!');
    }

    $minLen = 10;
    if (strlen($phone) < $minLen ) {
        return array('status' => false, 'message' => 'Số điện thoại phải ít nhất '.$minLen.' ký tự!');
    }

    $maxLen = 13;
    if (strlen($phone) > $maxLen) {
        return array('status' => false, 'message' => 'Số điện thoại phải nhiều nhất '.$maxLen.' ký tự!');
    }

    if($curUser['phone'] != $phone){
        $check = $userModel->isExists('phone', '0' . $this->phoneFix($phone));
        $check1 = $userModel->isExists('phone', '84' . $this->phoneFix($phone));
        if ($check['status'] || $check1['status']) {
            return array('status' => false, 'el' => 'phone', 'message' => 'Số điện thoại đã tồn tại!');
        }
    }
}

if($address == ''){
    return array('status'=>false,'message'=>'Vui lòng nhập địa chỉ!');
}

return array('status'=>true,'message'=>'Dữ liệu hợp lệ!' ,'curUser'=>$curUser);
}
    //--End Update info

    //--Forget pass---
public function forgetpassAction(){
    $userModel = $this->getUserModel();
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $email = $params['email'];
    if($email == ''){
        $this->returnJsonAjax(array('status'=>false,'message'=>'Vui lòng nhập email'));
    }else{
        $validator = new \Zend\Validator\EmailAddress();
        if (!$validator->isValid($email)) {
            $this->returnJsonAjax(array('status'=>false, 'message'=>'Email không đúng định dạng!'));
        } 

        $isExist = $userModel->isExists('email',$email);
        if(!$isExist['status']){
            $this->returnJsonAjax(array('status'=>false,'message'=>'Email không tồn tại trong hệ thống!'));
            }else{ //Is valid---
                $user = $isExist['item'];
                //--Check user valid--
                $resultValid =  $this->checkUserStatus($user);
                if($resultValid['status']){
                    $userId = $user['id'];
                    if($userId){
                        $forgetPassCode = HashUtil::createRandomKey();
                        $resultSave = $userModel->save(array('forget_pass_code'=>$forgetPassCode), $userId);
                        if($resultSave['status']){
                            $urlRecover = BASE_URL;
                            if(strpos($urlRecover,'/api/')!==FALSE){
                                $urlRecover = str_replace('/api/', '/', $urlRecover);
                            }
                            if(strpos($urlRecover,'/api')!==FALSE){
                                $urlRecover = str_replace('/api', '', $urlRecover);
                            }
                            $urlRecover = $urlRecover.'/doi-mat-khau?fpc='.$forgetPassCode;

                            //--Send email--
                            $mail = $this->getServiceLocator()->get('SendMail');
                            $mail->send(array(
                                'from' => array('name' => EMAIL_SEND_FROM_NAME, 'email' => EMAIL_SEND_FROM_EMAIL),
                                'to' => array('name' => $user['name'], 'email' => $user['email']),
                                'subject' => '[BZ CMS] thông báo',
                                'template' => 'email/forgetpass',
                                'data' => array(
                                    'user_name'=>$user['name'],
                                    'url_recover'=>$urlRecover,
                                    'code'=>$forgetPassCode
                                    )
                                ));
                            //--End Send email--

                            $this->returnJsonAjax(array('status'=>true,'message'=>'Đã gửi email hướng dẫn lấy lại mật khẩu!'));
                        } 
                    }
                }else{
                 $this->returnJsonAjax($resultValid); 
             }
                //--End check user valid
         }
     }
 }

 public function recoverAction(){
    $userModel = $this->getUserModel();
    $params = $this->getParams(self::METHOD_POST_PARAM);
    $forgetPassCode = $params['forget_pass_code'];
    $newPass = $params['password'];
    $cfNewPass = $params['cfpassword'];
    $result = $userModel->recover($forgetPassCode, $newPass, $cfNewPass); 
    $this->returnJsonAjax($result);
}
    //--End Forget pass---

    //--******************** END API **************************:
}
