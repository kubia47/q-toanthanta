<?php

// module/Contest/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Contest\Front\Controller\Contest' => 'Contest\Front\Controller\ContestController',
            'Contest\Admin\Controller\Contest' => 'Contest\Admin\Controller\ContestController',
            'Contest\Admin\Controller\ContestWeek' => 'Contest\Admin\Controller\ContestWeekController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'contest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/[:lang]/contest[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'index',
                    ),
                ),
            ),

            'contest-submit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contest-submit[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'submit',
                    ),
                ),
            ),
            'contest-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bai-thi-trac-nghiem/:slug/:id[/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                        'slug'=> '[\w_-]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'detail',
                    ),
                ),
            ),
            'quiz-contest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-quiz[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'createQuiz',
                    ),
                ),
            ),
            'quiz-details' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/quiz[/:post_id][/:slug][/:id][/]',
                    'constraints' => array(
                        'post_id'     => '[0-9]+',
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'quiz',
                    ),
                ),
            ),
            'quiz-submit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/submit-quiz[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'submitQuiz',
                    ),
                ),
            ),
            'quiz-result' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dap-an-trac-nghiem[/:slug][/:id][/]',
                    'defaults' => array(
                        'controller' => 'Contest\Front\Controller\Contest',
                        'action' => 'resultQuiz',
                    ),
                ),
            ),

            //--************************* ADMIN ***************************
            'contest-admin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contest[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\Contest',
                        'action' => 'index',
                    ),
                ),
            ),  

            'contestweek-admin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contestweek[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\contestweek',
                        'action' => 'index',
                    ),
                ),
            ), 
            'charge-money' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/api/charge-money[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\Contest',
                        'action' => 'charge',
                    ),
                ),
            ), 
            'charge-history' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/history[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\Contest',
                        'action' => 'history',
                    ),
                ),
            ), 
            'charge-api-history' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/api/history[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\Contest',
                        'action' => 'apiHistory',
                    ),
                ),
            ), 
            'charge-remove' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/api/charge-remove[/]',
                    'defaults' => array(
                        'controller' => 'Contest\Admin\Controller\Contest',
                        'action' => 'chargeRemove',
                    ),
                ),
            )
            //--************************* END ADMIN ***************************   
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'mail/contest/contest' => WEB_ROOT . '/application/templates/front/email/html/contest.phtml',
        )
    ),
);
