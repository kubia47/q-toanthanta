<?php
namespace Language;


class Module {

    public function getAutoLoaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig() {
        return array(
            'factories' => array(
               
                        
                'Language\Admin\Form\LanguageForm' => function($sm) {
                    $form = new Admin\Form\LanguageForm();
                    return $form;
                },
                'Language\Admin\Model\Language' => function($sm) {
                    
                    $contact = new Admin\Model\Language();
                    return $contact;
                },           
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                
            )
        );
    }

}

?>
