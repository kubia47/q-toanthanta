<?php

// module/Language/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            
            'Language\Admin\Controller\Language' => 'Language\Admin\Controller\LanguageController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            
            'languageadmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/language[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Language\Admin\Controller\Language',
                        'action'     => 'index',
                    ),
                ),
            ),
            
        ),
    ),
  
);
