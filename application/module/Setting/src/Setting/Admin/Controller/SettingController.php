<?php

namespace Setting\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;
use Zend\EventManager\EventInterface as Event;

class SettingController extends AdminController {

    public $routerName = 'setting';

    public function getForm() {
        if (empty($this->form)) {
            $this->form = $this->getServiceLocator()->get('FormElementManager')->get('Setting\Admin\Form\SettingForm');
        }
        return parent::setupForm($this->form);
    }

    public function getModelServiceName() {
        $this->modelServiceName = 'Setting\Admin\Model\Setting';
        return $this->modelServiceName;
    }

    public function infoAction() {
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Setting\Admin\Form\SiteInforForm');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($form->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getModel()->saveSetting($data);
            }
        }
        $setting = $this->getModel()->getSettingByGroup('core');
        $form->bind($setting);
        return array('form' => $form);
    }

    public function onBeforeCreate(Event $e) {
        $params = $e->getParams();
        $this->processWriteFileGa($params);
    }

    public function onBeforeEdit(Event $e) {
        $params = $e->getParams();
        $this->processWriteFileGa($params);
    }

    //File google analytics (ga.js)
    private function processWriteFileGa($params){
        $name = trim($params['name']);
        $module = $params['module'];
        $group = $params['group'];
        if(($name == 'api_e_home_setting' || $name == 'api_flora_anh_dao_setting' || $name == 'api_nam_long_home_setting' ) && $module == 'core' && $group == 'core' ){
            $path = WEB_ROOT. DS."media".DS."google";
            $fileName = $name.'.js';
            $content = $params['value'];
            $this->writeFile($path, $fileName, $content);
        }
    }

}
