<?php
namespace Customer;
use Customer\Front\Form\CustomerForm;

class Module {

    public function getAutoLoaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Customer\Front\Form\CustomerForm' => function($sm) {
                    $form = new ContactForm();
                    return $form;
                },
                'Customer\Front\Model\Customer' => function($sm) {
                    
                    $customer = new Front\Model\Customer();
                    return $customer;
                },   
                        
                'Customer\Admin\Form\CustomerForm' => function($sm) {
                    $form = new Admin\Form\CustomerForm();
                    return $form;
                },
                'Customer\Admin\Model\Customer' => function($sm) {
                    
                    $customer = new Admin\Model\Customer();
                    return $customer;
                },           
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
               
            )
        );
    }

}

?>
