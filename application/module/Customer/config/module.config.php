<?php

// module/Customer/config/module.config.php:
return array(
    'controllers' => array(
        'invokables' => array(
            'Customer\Front\Controller\Customer' => 'Customer\Front\Controller\CustomerController',
            'Customer\Admin\Controller\Customer' => 'Customer\Admin\Controller\CustomerController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(

            //--*****Block front--**********
            
            //--*****End Block front--**********
            //****************************************************************************************************
            //--*****Block admin--**********
            'customeradmin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/customer[/:action][/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Customer\Admin\Controller\Customer',
                        'action'     => 'index',
                    ),
                ),
            ),
            //--*****End Block admin--**********
            'api-company' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api-company[/]',
                    'defaults' => array(
                        'controller' => 'Customer\Admin\Controller\Customer',
                        'action'     => 'apiCompany',
                    ),
                ),
            ),
            'api-customer' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api-customer[/]',
                    'defaults' => array(
                        'controller' => 'Customer\Admin\Controller\Customer',
                        'action'     => 'apiCustomer',
                    ),
                ),
            ),
            'api-customer-add' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api-add-customer[/]',
                    'defaults' => array(
                        'controller' => 'Customer\Admin\Controller\Customer',
                        'action'     => 'apiAddCustomer',
                    ),
                ),
            ),
            
        ),
    ),
    'view_manager'=>array(
        'template_map'=>array(
            'customer/block/latest'   => __DIR__ . '/../src/Customer/Front/View/customer/block/latest/default.phtml',
        )
    )
);
