<?php

namespace Customer\Admin\Form;

use Core\Form\CoreForm;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerForm extends CoreForm implements InputFilterAwareInterface {

    protected $inputFilter;

    public function __construct() {
        parent::__construct('n_customer');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate form-horizontal panel ');
    }

    public function init() {
        parent::init();
        $this->addElements();
    }

    function addElements() {
        $factory = $this->getServiceLocator()->get('ServiceFactory');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
                'class' => 'required'
            ),
        ));

        $this->add(array(
            'name' => 'type',
            'attributes' => array(
                'type' => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required form-control'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required email form-control'
            ),
        ));

        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'autocomplete' => 'off',
                'type' => 'text',
                'class' => 'required form-control company'
            ),
        ));

        $this->add(array(
            'name' => 'company_id',
            'attributes' => array(
                'id' => 'company-id',
                'type' => 'text',
                'class' => 'required form-control'
            ),
        ));

        $this->add(array(
            'name' => 'parent_id',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $factory->getCompanies(),

            ),
            'attributes' => array(
                'class'=>'required form-control',
            )
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'id' => 'personal_address',
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'dob',
            'attributes' => array(
                'id' => 'dob',
                'type' => 'text',
                'class' => 'form-control datepicker'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'class' => 'required email form-control'
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'class'=>'required form-control'
            ),
        ));

        $this->add(array(
            'name' => 'note',
            'attributes' => array(
                'type' => 'textarea',
                'class' => ' form-control',
                'rows'  => '4'
            ),
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'value_options' => $factory->getStatus(),

            ),
            'attributes' => array(
                'class'=>' form-control',
            )
        ));
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter name'
                            ),
                        ),
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'type',
                'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'company',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'address',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'dob',
                    'required' => false,
                )
            ));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'email',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter email'
                            ),
                        ),
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'company_id',
                    'required' => false,
                )
            ));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'parent_id',
                    'required' => false,
                )
            ));

            $inputFilter->add($factory->createInput(array(
                'name' => 'note',
                'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                    'name' => 'status',
                    'required' => true,
                )
            ));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        $this->inputFilter = $inputFilter;
    }

}
