<?php

namespace Customer\Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\AdminController;
use Zend\EventManager\EventInterface as Event;

class CustomerController extends AdminController {

    public function getForm() {
        $params = $this->getParams();
        //print_r($params);die;
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Customer\Admin\Form\CustomerForm');

        if (empty($this->form)) {
            $this->form = $this->getServiceLocator()->get('FormElementManager')->get('Customer\Admin\Form\CustomerForm');
        }

        if ($params['type']) {
            $form->bind(new \ArrayObject(array('type' => $params['type'])));
        }

        return parent::setupForm($this->form);
    }

    public function getModelServiceName() {
        $this->modelServiceName = 'Customer\Admin\Model\Customer';
        return $this->modelServiceName;
    }

    //-----Process action--------
    public function onBeforeListing(Event $e) {
        $params = $e->getParams();
        if ($params) {
            $model = $this->getModel();

            if (!$params['page']) {
                //Reset filter
                $model->setState('filter.object_id', '');
                //End Reset filter
            }

            //Filter by type
            $type = $params['type'];
            if ($type) {
                $model->setState('filter.type', $type);
            }

            //Filter by objectId
            $objectId = $params['object_id'];
            if ($objectId) {
                $model->setState('filter.object_id', $objectId);
            }
        }
    }

    public function addAction() {

        $form = $this->getForm();
        $request = $this->getRequest();
        $model = $this->getModel();
        $params = $this->getParams();
        if($params['lang_group']){
            $_SESSION['lang_group_add'] = $params['lang_group'];
        }else{
            unset($_SESSION['lang_group_add']);
        }

        if ($this->isValidForm($form)) {

            $data = $form->getData();
            //echo 'a';die;
            //$data = $this->getEventManager()->prepareArgs($data);

            $this->getEventManager()->trigger('onBeforeCreate', $this, $data);

            if ($params['type'] == 'customer') {
                $branchData = array();
                $departmentData = array();

                $data['dob'] = date("Y-m-d", strtotime(str_replace('/', '-', $data['dob'])));

                // if
                if (isset($data['company_id']) && $data['company_id']) {
                    if (!isset($_POST['branch_id'])) {
                        // create new branch
                        // echo 'create new branch';
                        $branchData['name'] = 'Default';
                        $branchData['parent_id'] = $data['company_id'];
                        $branchData['type'] = 'branch';
                        $branchData['status'] = 1;

                        $saveNewBranch = $model->save($branchData);
                        if ($saveNewBranch) {
                            $newBranchId = $saveNewBranch['item']['id'];

                            // echo 'create new department';
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $newBranchId;
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        }
                    } else {
                        if (!isset($_POST['department_id'])) {
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $_POST['branch_id'];
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        } else {
                            $data['company_id'] = $_POST['department_id'];
                        }
                    }
                }
            }

            unset($data['csrf']); //Remove csrf
            $return = null;
            $return = $model->save($data);

            if ($return) {
                $data['id'] = $return['item']['id'];

                if ($params['type'] == 'company') {
                    $branchData = array();
                    $departmentData = array();

                    if ($_POST['company_branch'] != '' || $_POST['company_department'] != '') {

                        $brandName = 'Default';
                        if ($_POST['company_branch'] != '') {
                            $brandName = $_POST['company_branch'];
                        }

                        //add new company brand
                        // echo 'create new branch';
                        $branchData['name'] = $brandName;
                        $branchData['parent_id'] = $return['item']['id'];

                        if($_POST['company_branchaddress'] != '') {
                            $branchData['address'] = $_POST['company_branchaddress'];
                        }

                        $branchData['type'] = 'branch';
                        $branchData['status'] = 1;

                        $saveNewBranch = $model->save($branchData);

                        if( $_POST['company_department'] != '' && $saveNewBranch ){
                            // create new department
                            $departmentData['name'] = $_POST['company_department'];
                            $departmentData['parent_id'] = $saveNewBranch['item']['id'];
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);
                        }
                    }
                }

                $this->getEventManager()->trigger('onCreate', $this, $data);
                $this->addMessage('Save success');
                return $this->navigate($params, $return['id']);
            } else {
                $this->addMessage($return['message']);
            }
        }

        if ($params['type'] == 'customer') {
            $form->get('address')->setAttribute('disabled', 'disabled');
        }

        $companies = null;
        if ($params['type'] == 'department') {
            $customerModel = $this->getModel();
            $customerModel->setState('filter.parent', false);
            $customerModel->setState('filter.type', 'company');
            $companies = $customerModel->getItems();
            $companies = $companies->toArray();
        }

        if ($params['type']) {
            $form->bind(new \ArrayObject(array('type' => $params['type'])));
        }

        return new ViewModel(array(
            'form' => $form,
            '_page' => 'ADD',
            'companies' => $companies
        ));
    }

    public function editAction() {
        $id = $this->params()->fromRoute('id', 0);
        $form = $this->getForm();
        $params = $this->getParams();
        $model = $this->getModel();

        if ($this->isValidForm($form)) {
            $data = $form->getData();
            //$data = $this->getEventManager()->prepareArgs($data);
            $this->getEventManager()->trigger('onBeforeEdit', $this, $data);

            if ($params['type'] == 'customer') {
                $branchData = array();
                $departmentData = array();

                $data['dob'] = date("Y-m-d", strtotime(str_replace('/', '-', $data['dob'])));

                // if
                if (isset($data['company_id']) && $data['company_id']) {
                    if (!isset($_POST['branch_id'])) {
                        // create new branch
                        // echo 'create new branch';
                        $branchData['name'] = 'Default';
                        $branchData['parent_id'] = $data['company_id'];
                        $branchData['type'] = 'branch';
                        $branchData['status'] = 1;

                        $saveNewBranch = $model->save($branchData);
                        if ($saveNewBranch) {
                            $newBranchId = $saveNewBranch['item']['id'];

                            // echo 'create new department';
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $newBranchId;
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        }
                    } else {
                        if (!isset($_POST['department_id'])) {
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $_POST['branch_id'];
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        } else {
                            $data['company_id'] = $_POST['department_id'];
                        }
                    }
                }
            }

            unset($data['csrf']); //Remove csrf
            $return = $model->save($data,$id);

            $this->getEventManager()->trigger('onEdit', $this, $data);
            if ($return) {
                $this->addMessage('Save success');
                return $this->navigate($params, $return['id']);
            } else {
                $this->addMessage($return['message']);
            }
        }
        $item = $this->getItem();

        if ($item['type'] && ( $item['type'] == 'customer' || $item['type'] == 'employee' ) ) {
            $item['dob'] = date ('d/m/Y', strtotime($item['dob']));
        }

        $company_id = false;
        $branch_id = false;
        $department_id = false;
        $branches = array();
        $departments = array();
        if ($item['type'] == 'customer') {
            if (!$item['company_id']) {
                $form->get('company')->setAttribute('disabled', 'disabled');
            } else {
                $form->get('address')->setAttribute('disabled', 'disabled');

                // if has company
                $branch_id = (array) $this->getModel()->getItemById($item['company_id']);
                $branch_id = $branch_id['parent_id'];
                $company_id = (array) $this->getModel()->getItemById($branch_id);
                $company_id = $company_id['parent_id'];
                $branches = $this->getModel()->getBranches($company_id);
                $departments = $this->getModel()->getDepartments($branch_id);
            }
        }

        $form->bind($item);
        return new ViewModel(array(
            'form' => $form,
            'id' => $id,
            'data' => $item,
            '_page' => 'EDIT',
            'company_id' => $company_id,
            'branch_id' => $branch_id,
            'department_id' => $department_id,
            'branches' => $branches,
            'departments' => $departments
        ));
    }

    //--Customer--
    public function apiCompanyAction(){
        $params = $this->getParams();
        $userModel = $this->getModel();

        if(isset($params['_action'])){
            $userModel->setState('filter.type', false);
            $userModel->setState('filter.search', false);
            $userModel->setState('filter.parent', false);

            if($params['_action'] == 'SEARCH_COMPANY'){
                // search customer with keyword
                $userModel->setState('filter.type', 'company');

                if(isset($params['_searchkey'])){
                    $userModel->setState('filter.search', $params['_searchkey']);
                }

                $userModel->setLimit(5);

                $companyResults = $userModel->getItems();
                $companyResults = $companyResults->toArray();

                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$companyResults));
            } else if ($params['_action'] == 'GET_BRANCH') {

                if(isset($params['parent_id'])){
                    $userModel->setState('filter.parent', $params['parent_id']);
                }

                // search customer with keyword
                $userModel->setState('filter.type', 'branch');

                $brandResults = $userModel->getItems();
                $brandResults = $brandResults->toArray();
                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$brandResults));
            } else if ($params['_action'] == 'GET_DEPARTMENT') {

                if(isset($params['parent_id'])){
                    $userModel->setState('filter.parent', $params['parent_id']);
                }

                // search customer with keyword
                $userModel->setState('filter.type', 'department');

                $brandResults = $userModel->getItems();
                $brandResults = $brandResults->toArray();
                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$brandResults));
            }

            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }else{
            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }
    }

    //--Customer--
    public function apiCustomerAction(){
        $params = $this->getParams();
        $userModel = $this->getModel();

        if(isset($params['_action'])){
            if($params['_action'] == 'SEARCH_CUSTOMER'){
                // search customer with keyword
                $userModel->setState('filter.type', 'customer');
                $userModel->setState('filter.parent', false);

                if(isset($params['_searchkey'])){
                    $userModel->setState('filter.search', $params['_searchkey']);
                }else{
                    $userModel->setState('filter.search', false);
                }

                $userModel->setLimit(5);

                $companyResults = $userModel->getItems();
                $companyResults = $companyResults->toArray();

                $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$companyResults));
            }

            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }else{
            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }
    }

    //--Customer--
    public function apiAddCustomerAction(){
        $params = $this->getParams();
        $model = $this->getModel();

        if(isset($params['_action'])){
            if($params['_action'] == 'ADD_CUSTOMER'){
                parse_str($params['_data'], $data);

                $branchData = array();
                $departmentData = array();

                $data['dob'] = date("Y-m-d", strtotime(str_replace('/', '-', $data['dob'])));

                // if has company (create new brand and department default if missing
                if (isset($data['company_id']) && $data['company_id']) {
                    if (!isset($data['branch_id'])) {
                        // create new branch
                        // echo 'create new branch';
                        $branchData['name'] = 'Default';
                        $branchData['parent_id'] = $data['company_id'];
                        $branchData['type'] = 'branch';
                        $branchData['status'] = 1;

                        $saveNewBranch = $model->save($branchData);
                        if ($saveNewBranch) {
                            $newBranchId = $saveNewBranch['item']['id'];

                            // echo 'create new department';
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $newBranchId;
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        }
                    } else {
                        if (!isset($data['department_id'])) {
                            $departmentData['name'] = 'Default';
                            $departmentData['parent_id'] = $data['branch_id'];
                            $departmentData['type'] = 'department';
                            $departmentData['status'] = 1;

                            $saveNewDepartment = $model->save($departmentData);

                            if ($saveNewDepartment) {
                                $newDepartmentId = $saveNewDepartment['item']['id'];
                                $data['company_id'] = $newDepartmentId;
                            }
                        } else {
                            $data['company_id'] = $data['department_id'];
                        }

                        unset($data['branch_id']);
                        unset($data['department_id']);
                    }
                }

                // save new customer
                $data['type'] = 'customer';
                $return = $model->save($data);

                if ($return) {
                    $this->returnJsonAjax(array('status'=>true,'message'=>'Đăng ký thành công', 'data'=>$return));
                } else {
                    $this->returnJsonAjax(array('status'=>false,'message'=>'Lỗi không tạo được khách hàng mới!'));
                }

            }

            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }else{
            $this->returnJsonAjax(array('status'=>false,'message'=>'No request access'));
        }
    }

    public function exportAction(){
        $params = $this->getParams();
        $type = $params['type'];

        $this->processExport();  /*Export with no image*/
    }

    private function processExport(){
        $factory = $this->getServiceLocator()->get('ServiceFactory');
        $model = $this->getModel();
        $data = $model->getDataExport();

        require_once VENDOR_INCLUDE_DIR . '/phpoffice/phpexcel-1.8.1/Classes/PHPExcel.php';

        //Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        //Set document properties
        $objPHPExcel->getProperties()->setCreator("BzCMS")
            ->setLastModifiedBy("BzCMS")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription('BzCMS')
            ->setKeywords("office 2007 openxml php")
            ->setCategory('BzCMS');

        //---Set name--
        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $objPHPExcel->getDefaultStyle()->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Danh Sách Khách Hàng");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
        $objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true)->setSize(15); //Size of Intro file
        $objPHPExcel->getActiveSheet()->getStyle("A2:K1")->getFont()->setBold(true)->setSize(14); //Set size Head Title
        //--End set name--

        //Add some data
        $indexCellTitle = 2;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$indexCellTitle, 'STT')
            ->setCellValue('B'.$indexCellTitle, 'Id')
            ->setCellValue('C'.$indexCellTitle, 'Họ và tên')
            ->setCellValue('D'.$indexCellTitle, 'Công Ty/Địa chỉ')
            ->setCellValue('E'.$indexCellTitle, 'Chi Nhánh')
            ->setCellValue('F'.$indexCellTitle, 'Điện thoại')
            ->setCellValue('G'.$indexCellTitle, 'Điểm')
            ->setCellValue('H'.$indexCellTitle, 'Điểm đổi')
            ->setCellValue('I'.$indexCellTitle, 'Hạng')
            ->setCellValue('J'.$indexCellTitle, 'Trạng Thái')
            ->setCellValue('K'.$indexCellTitle, 'Ngày tạo');

        $cell = 0;
        foreach($data as $key => $val){
            $user = $factory->getUser($val['user_id']);
            $cell = 3 + $key;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $cell, $key + 1)
                ->setCellValue('B' . $cell, $val['id'])
                ->setCellValue('C' . $cell, $val['name'])
                ->setCellValue('D' . $cell, ($val['company_id']) ? $val['companyName'] : $val['address'])
                ->setCellValue('E' . $cell, $val['branchName'])
                ->setCellValue('F' . $cell, $val['phone'])
                ->setCellValue('G' . $cell, $val['score'])
                ->setCellValue('H' . $cell, $val['exchange_score'])
                ->setCellValue('I' . $cell, $val['rankTitle'])

                ->setCellValue('J' . $cell, $val['status'] == 1 ? 'Publish' : ($val['status'] == 2 ? 'Rejected' : 'Unpublish'))
                ->setCellValue('K' . $cell, $val['created']);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);

            $objPHPExcel->getActiveSheet()->getStyle('I'. $cell)->getAlignment()->setWrapText(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->getFont()->setSize(13); //Set size content

        //--Set border---
        // $style = array(
        //     'borders' => array(
        //         'allborders' => array(
        //             'style' => \PHPExcel_Style_Border::BORDER_THIN
        //         )
        //     )
        // );
        // $objPHPExcel->getActiveSheet()->getStyle("A2:K2".$cell)->applyFromArray($style);
        //--End Set border---

        //Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Users');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        //--Set protected--
        //$objSheet = $objPHPExcel->getActiveSheet();
        //$objSheet->getProtection()->setSheet(true)->setPassword('contest');
        //--End Set protected--

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Users_' .time(). '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function onBeforeCreate(Event $e) {
        $params = $e->getParams();
    }

    public function onBeforeEdit(Event $e) {
        $params = $e->getParams();
    }

    //-----End Process action-------
    
}

