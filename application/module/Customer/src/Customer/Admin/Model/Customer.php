<?php

namespace Customer\Admin\Model;
use Zend\Db\Sql\Select;
use Core\Model\AppModel;

class Customer extends AppModel {

    public $table = 'bz1_customers';
    public $context = 'customer';
    
    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);

        $fromdate = $this->getUserStateFromRequest('filter.from_date', 'filter_from_date');
        $fromdate = trim($fromdate);
        $this->setState('filter.from_date', $fromdate);

        $todate = $this->getUserStateFromRequest('filter.to_date', 'filter_to_date');
        $todate = trim($todate);
        $this->setState('filter.to_date', $todate);

        $published = $this->getUserStateFromRequest('filter.status', 'filter_status', '');
        $this->setState('filter.status', $published);
        parent::populateState();
    }

    public function getDefaultListQuery() {
        $select = new Select($this->table);
        $status = $this->getState('filter.status');
        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }

        $type = $this->getState('filter.type');

        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            if ($type && $type == 'company') {
                $select->where(" (".$this->table.".name like '%$keyword%') ");
            }

            if ($type && $type == 'customer') {
                $select->where(" (".$this->table.".name like '%$keyword%' or ".$this->table.".phone like '%$keyword%') ");
            }
        }

        //Filter by type
        if ($type) {
            $select->where(array($this->table.'.type' => $type));
        }

        //--Date user submit contest
        $fromDate = $this->getState('filter.from_date');
        if (!empty($fromDate)) {
            $select->where('date_format('.$this->table.'.created,"%Y-%m-%d") >= "'.$fromDate.'"');
        }

        $toDate = $this->getState('filter.to_date');
        if (!empty($toDate)) {
            $select->where('date_format('.$this->table.'.created,"%Y-%m-%d") <= "'.$toDate.'"');
        }

        //Filter by parent
        $parent_id = $this->getState('filter.parent');
        if ($parent_id) {
            $select->where(array($this->table.'.parent_id' => $parent_id));
        }

        //order
        $filter_order = $this->getState('order.field');
        $filter_order_dir = $this->getState('order.direction');

        if (!empty($filter_order)) {
            $select->order($this->table.".". $filter_order . " " . $filter_order_dir);
        }else{
              $select->order($this->table.".created DESC");
        }

        $_SESSION['select_export'] = $select;
        //print $select->getSqlString(); die;
        return $select;
    }

    public function getBranches($parent_id) {
        $type = 'branch';

        return $this->getAllItemsToArray(array('type' => $type, 'parent_id' => $parent_id));
    }

    public function getDepartments($parent_id) {
        $type = 'department';

        return $this->getAllItemsToArray(array('type' => $type, 'parent_id' => $parent_id));
    }

    public function getCustomerByUId($Id) {
        return $this->getItem(array('id' => $Id));
    }

    public function getCustomerDetailsByUId($Id) {
        $this->setState('filter.parent', false);
        $this->setState('filter.search', false);
        $select = $this->getListQuery();
        $select->where(array($this->table.'.id' => $Id));
        // echo $select->getSqlString();

        $result = $this->selectWith($select);
        $rows = $result->toArray();
        return $rows[0];
    }

    public function countCustomer() {
        $type = 'customer';
        $select = new Select($this->table);
        $select->where(array('type' => $type));

        $result = $this->selectWith($select);
        $rows = $result->toArray();

        return count($rows);
    }

    public function countCompany() {
        $type = 'company';
        $select = new Select($this->table);
        $select->where(array('type' => $type));

        $result = $this->selectWith($select);
        $rows = $result->toArray();

        return count($rows);
    }

    public function getDataExport(){
        $select = $_SESSION['select_export'];
        if ($select) {
            $select->limit(10000000);
            return  $this->selectWith($select);
        }

        return  null;
    }
}