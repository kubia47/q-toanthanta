<?php

namespace Customer\Front\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Core\Model\AppModel;
use Zend\Db\Sql\Select;

class Customer extends AppModel {

    public $table = 'bz1_customers';
    public $context = 'customer';

}