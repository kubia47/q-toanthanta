<?php

namespace User\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Like;
use App\HashUtil;

class User extends AppModel {

    public $table = 'bz1_users';
    public $context = 'user';

    public function save($data = array(), $id = 0, $primarykey = 'id') {

        $id = (int) $data['id'];

        $data['dob'] = date("Y-m-d", strtotime($data['dob']));

        unset($data['cfpassword']);
        if ($data['password']) {
            $hashPassword = HashUtil::createPasswordWithSalt($data['password']);
            $data['password'] = $hashPassword;
        } else {
            unset($data['password']);
        }

        return parent::save($data, $id, 'id');
    }

    public function getUserByEmail($email = '') {
        return $this->getItem(array('email' => $email));
    }

    public function getUserByUId($Id) {
            return $this->getItem(array('id' => $Id));
    }

    public function countUserByType($type) {
        $select = new Select($this->table);
        $select->where(array('type' => $type));

        $result = $this->selectWith($select);
        $rows = $result->toArray();

        return count($rows);
    }

    public function populateState($ordering = null, $direction = null) {

        $search = $this->getUserStateFromRequest('filter.search', 'filter_search');
        $search = trim($search);
        $this->setState('filter.search', $search);

        $published = $this->getUserStateFromRequest('filter.status', 'filter_status', '');
        $this->setState('filter.status', $published);

        $fromdate = $this->getUserStateFromRequest('filter.from_date', 'filter_from_date');
        $fromdate = trim($fromdate);
        $this->setState('filter.from_date', $fromdate);

        $todate = $this->getUserStateFromRequest('filter.to_date', 'filter_to_date');
        $todate = trim($todate);
        $this->setState('filter.to_date', $todate);

        $group = $this->getUserStateFromRequest('filter.group', 'filter_group', '');
        $this->setState('filter.group', $group);
        $type = $this->getUserStateFromRequest('filter.type', 'filter_type', '');
        $this->setState('filter.type', $type);
        
        parent::populateState();
    }

    public function getDefaultListQuery() {
        $select = new Select($this->table);
        $status = $this->getState('filter.status');
        if (strlen($status) > 0) {
            $select->where(array('status' => $status));
        }

        $type = $this->getState('filter.type');

        $keyword = $this->getState('filter.search');
        if ($keyword) {
            $keyword = trim($keyword);
            $select->where(" (id like '%$keyword%' or email like '%$keyword%' or name like '%$keyword%' or phone like '%$keyword%' ) ");

            /*if ($type && $type == 'company') {
                $select->where(" (name like '%$keyword%') ");
            }

            if ($type && $type == 'customer') {
                $select->where(" (".$this->table.".name like '%$keyword%' or ".$this->table.".phone like '%$keyword%') ");
            }*/
        }


        if ($type) {
            $type = trim($type);

            $select->where(array($this->table.'.type' => $type));
        }

        //--Date user submit contest
        $fromDate = $this->getState('filter.from_date');
        if (!empty($fromDate)) {
            $select->where('date_format('.$this->table.'.created,"%Y-%m-%d") >= "'.$fromDate.'"');
        }

        $toDate = $this->getState('filter.to_date');
        if (!empty($toDate)) {
            $select->where('date_format('.$this->table.'.created,"%Y-%m-%d") <= "'.$toDate.'"');
        }

        //order
        $filter_order = $this->getState('order.field');
        $filter_order_dir = $this->getState('order.direction');

        if (!empty($filter_order)) {
            $select->order($this->table. "." . $filter_order . " " . $filter_order_dir);
        }else{
            $select->order($this->table.".created DESC");
        }

        $_SESSION['select_export'] = $select;

        return $select;
    }

    public function getDataExport(){
        $select = $_SESSION['select_export'];
        if ($select) {
            $select->limit(10000000);
            return  $this->selectWith($select);
        }
       
        return  null;
    }

}
