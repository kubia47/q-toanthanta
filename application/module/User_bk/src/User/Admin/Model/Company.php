<?php

namespace User\Admin\Model;

use Core\Model\AppModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class Company extends AppModel {

    public $table = 'bz1_company';

    public function getInternalCompanies() {

        $select = new Select($this->table);

        $result = $this->selectWith($select);
        $items = $result->toArray();

        return $items;
    }
}
