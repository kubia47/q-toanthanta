$(function () {
    "use strict";
    //This is for the Notification top right
    /*$.toast({
            heading: 'Welcome to Elite admin'
            , text: 'Use the predefined ones, or specify a custom position object.'
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'info'
            , hideAfter: 3500
            , stack: 6
        })*/
        // Dashboard 1 Morris-chart
    $('.clockpicker').clockpicker({
        donetext: 'Chọn',
    }).find('input').change(function() {
        console.log(this.value);
    });

    /* Validate form */
    var formValidate = function(){
        /* Sale form */
        var register_validate = $("form[name='create-sale-form']").validate({
            onkeyup: false,
            onfocusout: false,
            // Specify validation rules
            rules: {
                customerId: "required",
                employeeId: "required",
                buy_date: "required",
                sum_price: "required",
                order_number: {
                    required: true,
                    maxlength: 7
                }
            },
            // Specify validation error messages
            messages: {
                customerId: "Vui lòng chọn khách hàng",
                employeeId: "Vui lòng chọn nhân viên",
                buy_date: "Vui lòng chọn ngày xuất hóa đơn",
                sum_price: "Vui lòng nhập tổng giá trị đơn hàng",
                order_number: "Vui lòng nhập đúng số hóa đơn (tối đa 7 kí tự)",
            }
        });

        /* customer form */
        var customer_validate = $("form[name='customer-api-form']").validate({
            onkeyup: false,
            onfocusout: false,
            // Specify validation rules
            rules: {
                name: "required"
            },
            // Specify validation error messages
            messages: {
                name: "Vui lòng nhập tên khách hàng",
                company: "Vui lòng chọn công ty",
                address: "Vui lòng nhập địa chỉ công ty",
                phone: "Vui lòng nhập số điện thoại",
                email: "Vui lòng địa chỉ email",
            },
            submitHandler: function(form, event) {
            }
        });

        $("#customer-api-form").on('submit', function(e) {
            e.preventDefault();
        });

        /* Sale form */
        var changegiftvalidate = $("form[name='change-voucher-form']").validate({
            onkeyup: false,
            onfocusout: false,
            // Specify validation rules
            rules: {
                customerId: "required",
                voucherId: "required"
            },
            // Specify validation error messages
            messages: {
                customerId: "Vui lòng chọn khách hàng",
                voucherId: "Vui lòng chọn voucher"
            }
        });
    }


    /* Personal Customer Selection */
    $('#is_personal_customer').change( function() {
        if ($(this).prop('checked')) {
            $('#personal_address').removeAttr('disabled');
            $('#personal_address').addClass('required');

            companyLabel.val('');
            companyLabel.attr({'disabled': 'disable'});
            companyLabel.closest('.form-group').removeClass('has-error');
            companyInputId.val(0);
        } else {
            $('#personal_address').attr({'disabled': 'disabled'});
            $('#personal_address').removeClass('required');
            $('#personal_address').closest('.form-group').removeClass('has-error');

            companyLabel.removeAttr('disabled');
            companyLabel.val('');
        }
    });


    /* Ecoink Ajax Suggestion */
    var companyResults = [];
    var productResults = [];
    var employeeResults = [];

    var suggestAjaxRequest = function() {
        /* Companies ajax request */
        companyLabel.keyup(function() {
            var searchKey = $(this).val();

            companySuggestList.empty();

            if(searchKey != ''){
                $.ajax({
                    url: baseurl + "/api-company",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _action: "SEARCH_COMPANY",
                        _searchkey: searchKey
                    },
                    success: function (res) {
                        if (res.status) {
                            companyResults = res.data;
                            if(companyResults.length > 0) {
                                for(var i in companyResults){
                                    var company = companyResults[i];

                                    companySuggestList.append('<li class="list-group-item" id="item-id' + company.id + '" onclick="selectGroupItem(' + company.id + ')">' + company.name + '</li>');
                                }
                            }
                        }
                    }
                });
            }
        });

        /* Customer ajax request */
        customerLabel.keyup(function() {
            var searchKey = $(this).val();

            customerSuggestList.empty();

            if(searchKey != ''){
                $.ajax({
                    url: baseurl + "/api-customer",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _action: "SEARCH_CUSTOMER",
                        _searchkey: searchKey
                    },
                    success: function (res) {
                        if (res.status) {
                            customerResults = res.data;
                            if(customerResults.length > 0) {
                                for(var i in customerResults){
                                    var customer = customerResults[i];

                                    customerSuggestList.append('<li class="list-group-item" id="cus-id' + customer.id + '" onclick="selectCustomerItem(' + customer.id + ')">' + customer.name + '</li>');
                                }
                            }
                        }
                    }
                });
            }
        });

        /* Product ajax request */
        productLabel.keyup(function() {
            var searchKey = $(this).val();

            productSuggestList.empty();

            if(searchKey != ''){
                $.ajax({
                    url: baseurl + "/api-product",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _action: "SEARCH_PRODUCT",
                        _searchkey: searchKey
                    },
                    success: function (res) {
                        if (res.status) {
                            productResults = res.data;
                            if(productResults.length > 0) {
                                for(var i in productResults){
                                    var product = productResults[i];

                                    productSuggestList.append('<li class="list-group-item" id="product-id' + product.id + '" onclick="selectProductItem(' + product.id + ', ' + product.price + ')">' + product.title + '</li>');
                                }
                            }
                        }
                    }
                });
            }
        });

        /* Customer ajax request */
        employeeLabel.keyup(function() {
            var searchKey = $(this).val();

            employeeSuggestList.empty();

            if(searchKey != ''){
                $.ajax({
                    url: baseurl + "/api-employee",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _action: "SEARCH_EMPLOYEE",
                        _searchkey: searchKey
                    },
                    success: function (res) {
                        if (res.status) {
                            employeeResults = res.data;
                            if(employeeResults.length > 0) {
                                for(var i in employeeResults){
                                    var employee = employeeResults[i];

                                    employeeSuggestList.append('<li class="list-group-item" id="employ-id' + employee.id + '" onclick="selectEmployeeItem(' + employee.id + ')">' + employee.identify + ' - ' + employee.name + '</li>');
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    var elementEventInit = function() {
        /* -- -- -- */
        companyLabel.keyup(function(e){
            if (e.keyCode == 8 && companyInputId.val() != '') {
                companyInputId.val('');
                companyLabel.val('');
                setTimeout(function(){ companySuggestList.empty(); }, 200);
            }
        });
        customerLabel.keyup(function(e){
            if (e.keyCode == 8 && customerInputId.val() != '') {
                customerInputId.val('');
                customerLabel.val('');
                setTimeout(function(){ customerSuggestList.empty(); }, 200);
            }
        });
        productLabel.keyup(function(e){
            if (e.keyCode == 8 && productInputId.val() != '') {
                productInputId.val('');
                productLabel.val('');
                setTimeout(function(){ productSuggestList.empty(); }, 200);
            }
        });
        employeeLabel.keyup(function(e){
            if (e.keyCode == 8 && employeeInputId.val() != '') {
                employeeInputId.val('');
                employeeLabel.val('');
                setTimeout(function(){ employeeSuggestList.empty(); }, 200);
            }
        });
    }

    formValidate();
    suggestAjaxRequest();
    elementEventInit();
});

var companyInputId = $('#company-id');
var companySuggestList = $('.company-suggestion-list');
var companyLabel = $('.company-searchable .company');

var selectGroupItem = function (id) {
    var selectItem = $('#item-id' + id);

    var companyId = id;
    var companyName = selectItem.html();

    companyLabel.val(companyName);
    companyInputId.val(companyId);

    // get list branch
    showBranches(id, function(res){
        $('#branch-select').empty();

        if (res.status) {
            var branchResults = res.data;
            if (branchResults.length > 0) {
                for(var i = 0; i < branchResults.length; i++) {
                    $('#branch-select').append('<option value="'+branchResults[i].id+'">'+branchResults[i].name+'</option>');
                }

                // department selection
                customershowDepartment(branchResults[0].id);
            }
        }
    });

    companySuggestList.empty();
}

function customershowDepartment( parent_id ) {
    showDepartment( parent_id, function (res) {
        $('#department-select').empty();

        if (res.status) {
            var departmentResults = res.data;
            if (departmentResults.length > 0) {
                for(var i = 0; i < departmentResults.length; i++) {
                    $('#department-select').append('<option value="'+departmentResults[i].id+'">'+departmentResults[i].name+'</option>');
                }
            }
        }
    } );
}


var customerResults = [];
var customerInputId = $('#value-search1');
var customerSuggestList = $('.customer-suggestion-list');
var customerLabel = $('#input-search1');

var selectCustomerItem = function (id) {
    var selectItem = $('#cus-id' + id);

    var customerId = id;
    var customerName = selectItem.html();

    customerLabel.val(customerName);
    customerInputId.val(customerId);

    customerSuggestList.empty();

    /* show customer info */
    if ($('#customer-infos').length > 0) {
        // find customer info in list by id
        for(var i = 0; i < customerResults.length; i++) {
            if(customerResults[i].id == id) {
                var hasCompany = (customerResults[i].company_id == "0") ? false : true;
                var labelColor = hasCompany ? 'primary' : 'info';
                var lableCompany = hasCompany ? customerResults[i].companyName : 'Khách Lẻ';
                var lableInfo = hasCompany ? '<span>Chi Nhánh: <b>'+customerResults[i].branchName+'</b></span><br/><span>Phòng Ban: <b>'+customerResults[i].departmentName+'</b></span><br/>' : '<b>'+customerResults[i].address+'</b>';
                var customerInfos = '<span class="label label-table label-'+labelColor+'">'+lableCompany+'</span><br/><br/>' + lableInfo;
                $('#customer-score').html(customerResults[i].score - customerResults[i].exchange_score);
                $('#customer-infos .info1').html(customerInfos);

                $('#customer-infos').css({'display':'flex'});
                break;
            }
        }
    }
}

var productInputId = $('#value-search2');
var productSuggestList = $('.product-suggestion-list');
var productLabel = $('#input-search2');
var productSumPrice = $('#sum-price');

var selectProductItem = function (id, price) {
    var selectItem = $('#product-id' + id);

    var productId = id;
    var productName = selectItem.html();

    PRO_PRICE = price;
    calculateSumPrice();

    productLabel.val(productName);
    productInputId.val(productId);

    productSuggestList.empty();
}

var calculateSumPrice = function() {
    var quantity = parseInt($('#pro-amount').val());
    var sumprice = quantity * PRO_PRICE;
    var sumPriceFormat = sumprice.formatMoney(0, '.', '.');
    productSumPrice.html(sumPriceFormat);
}

var employeeInputId = $('#value-search3');
var employeeSuggestList = $('.employee-suggestion-list');
var employeeLabel = $('#input-search3');

var selectEmployeeItem = function (id) {
    var selectItem = $('#employ-id' + id);

    var employeeId = id;
    var employeeName = selectItem.html();

    employeeLabel.val(employeeName);
    employeeInputId.val(employeeId);

    employeeSuggestList.empty();
}

/* Voucher */
var selectVoucher = function(voucherId) {
    $('#voucher-id').val(voucherId);

    $('.voucher-item').removeClass('selected');
    $('.voucher-item-' + voucherId).addClass('selected');
}

/* Libs */
Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}