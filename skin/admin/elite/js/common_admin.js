$(document).ready(function(){
    // company change
    $('#company-select').change(function(){
        // get list branch
        showBranches($(this).children(":selected").attr("id"), function(res){
            $('#branch-select').empty();

            if (res.status) {
                var branchResults = res.data;
                if (branchResults.length > 0) {
                    for(var i = 0; i < branchResults.length; i++) {
                        $('#branch-select').append('<option value="'+branchResults[i].id+'">'+branchResults[i].name+'</option>');
                    }
                }
            }
        });
    });

    $('#company-select').change();
    
    $('#branch-select').change(function () {
        customershowDepartment($(this).val());
    });
});

function showBranches( parent_id, callback ) {
    console.log(parent_id);
    $.ajax({
        url: baseurl + "/api-company",
        type: 'post',
        dataType: 'json',
        data: {
            _action: "GET_BRANCH",
            parent_id: parent_id
        },
        success: function (res) {
            /*var res = JSON.decode(response);
            console.log(res);*/
            callback(res);
        }
    });
}

function showDepartment( parent_id, callback ) {
    console.log(parent_id);
    $.ajax({
        url: baseurl + "/api-company",
        type: 'post',
        dataType: 'json',
        data: {
            _action: "GET_DEPARTMENT",
            parent_id: parent_id
        },
        success: function (res) {
            callback(res);
        }
    });
}