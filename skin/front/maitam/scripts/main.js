'use strict';


/* HELPER: Checks Whether an Element Exists
----------------------------------------------------------------------------------------------------*/
(function( $ ) {

  $.fn.extend({
    exists: function() {
      if ( this.length > 0 ) {
        return true;
      } else {
        return false;
      }
    }
  });

})( jQuery );



jQuery(document).on("ready",function () {
	
});
$('.toggle-menu').click(function(){
	$(".header-mobile").css("left","0");
});
$('.md-close').click(function(){
	$(".header-mobile").css("left","-110" + "%");
});
// $('.select-language').click(function(){
// 	$('.choose-language').slideToggle();
// 	$(this).css('display','none');
// });
$('.ui.dropdown')
  .dropdown()
;



$('.slider-banner').owlCarousel({
    margin:0,
    nav:true,
    autoplay:true,
    autoplaySpeed:800,
    autoplayTimeout:3500,
    autoplayHoverPause:true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
$('.slider-our-project').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true
        }
    }
});

$('.slider-news').owlCarousel({
    loop:true,
    margin:10,
    autoplayTimeout:5000,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        600:{
            items:1,
            nav:true,
            loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});

$('.slider-talk').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay: true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.slider-blk-yt4').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        600:{
            items:1,
            nav:true,
            loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});


var hm_img = $(".meaning .image-meaning img").length;
var width_inner = $(".meaning .ctn-inner").width();
var width_scroll = width_inner * hm_img;

if($(window).width()<=768){
    $(".meaning .image-meaning img").css('width',width_inner);
     $(".meaning .md-scroll").css('width',width_scroll);
}else{
    $(".meaning .image-meaning img").css('width',25 + "%");
     $(".meaning .md-scroll").css('width',100 + '%');
}


// setInterval(function() {
        
//         var hm_img = $(".meaning .image-meaning img").length;
//         var width_inner = $(".meaning .ctn-inner").width();
//         var width_scroll = width_inner * hm_img;
//         if($(window).width()<=768){
//             $(".meaning .image-meaning img").css('width',width_inner);
//              $(".meaning .md-scroll").css('width',width_scroll);
//         }else{
//             $(".meaning .image-meaning img").css('width',25 + "%");
//              $(".meaning .md-scroll").css('width',100 + '%');
//         }

// } ,100);




var timer = false;
$('.title-through').each(function(i, obj) {
    var width_title_through=$(this).width()-$(this).find("h3").width() - 25;
    $(this).find(".line-through").css('width', width_title_through);
});

var h3height = 0;
    $('.block-four-col .item').each(function() {
        if(h3height < $(this).height()){
          h3height = $(this).height();
        };
    });
$('.block-four-col .item').height(h3height); 
var onresize = function() {
   $('.title-through').each(function(i, obj) {
        var width_title_through=$(this).width()-$(this).find("h3").width() - 25;
        $(this).find(".line-through").css('width', width_title_through);
    
    $('.block-four-col .item').css("height","auto");
            var h3height = 0;
            $('.block-four-col .item').each(function() {
                if(h3height < $(this).height()){
                  h3height = $(this).height();
                };
            });
            $('.block-four-col .item').height(h3height);
    });


   var hm_img = $(".meaning .image-meaning img").length;
   var width_inner = $(".meaning .ctn-inner").width();
   var width_scroll = width_inner * hm_img;
   if($(window).width()<=768){
       $(".meaning .image-meaning img").css('width',width_inner);
        $(".meaning .md-scroll").css('width',width_scroll);
   }else{
       $(".meaning .image-meaning img").css('width',25 + "%");
        $(".meaning .md-scroll").css('width',100 + '%');
   }


    /*var height_1 = $('.news').height();
    var height_2 = $('.event .search').height();
    var height_3 = $('.event .title-through').height();
    var height_4 = height_1 - height_2 - height_3 - 30;
    $('.list-event').css('height', height_4);*/
}
window.addEventListener("resize", onresize);

// setInterval(function() {
        
//     $('.title-through').each(function(i, obj) {
//         var width_title_through=$(this).width()-$(this).find("h3").width() - 25;
//         $(this).find(".line-through").css('width', width_title_through);
    
//     $('.block-four-col .item').css("height","auto");
//             var h3height = 0;
//             $('.block-four-col .item').each(function() {
//                 if(h3height < $(this).height()){
//                   h3height = $(this).height();
//                 };
//             });
//             $('.block-four-col .item').height(h3height);
//     });
// } ,100);

/*var height_1 = $('.news').height();
var height_2 = $('.event .search').height();
var height_3 = $('.event .title-through').height();
var height_4 = height_1 - height_2 - height_3 - 30;
$('.list-event').css('height', height_4);*/


/*setTimeout(function() {
    var height_1 = $('.news').height();
    var height_2 = $('.event .search').height();
    var height_3 = $('.event .title-through').height();
    var height_4 = height_1 - height_2 - height_3 - 70;
    $('.list-event').css('height', height_4);
} ,1000);*/
 

var width_ggmap = $('.md-gg-map').width();
var height_ggmap = width_ggmap * 0.29;
$('.md-gg-map').css('height',height_ggmap);

var timer = false;
    $(window).resize(function() {
        if (timer) clearTimeout(timer);
        timer = setTimeout(function(){
            
           var width_ggmap = $('.md-gg-map').width();
           var height_ggmap = width_ggmap * 0.29;
           $('.md-gg-map').css('height',height_ggmap);

        }, 300);
    });

$(".md-nav-tab .item span").click(function(){
   var cc_Sd = $(this).attr("data-href");
   $(".md-nav-tab .item span").removeClass("active");
   $(this).addClass("active");
   $(".md-content-tab").removeClass("active");
   $(cc_Sd).addClass("active");
});
$(".smd-nav-control span").click(function(){
    var sc89_jh= $(this).attr("data-href");
    $(".tab-content-8p .tab-pane").removeClass('active');
    $(sc89_jh).addClass("active");
});


var grd = function(){
  $(".nm-form input[type='radio']").click(function() {
    var previousValue = $(this).attr('previousValue');
    var name = $(this).attr('name');

    if (previousValue == 'checked') {
      $(this).removeAttr('checked');
      $(this).attr('previousValue', false);
    } else {
      $("input[name="+name+"]:radio").attr('previousValue', false);
      $(this).attr('previousValue', 'checked');
    }
  });
};

grd('1');


$('.field-spl').click(function(){
    $('.md-form-spl').toggleClass('active');
});

