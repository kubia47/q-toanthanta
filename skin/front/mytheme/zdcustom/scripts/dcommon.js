var App = App || {};

//---MAIN----
$(function () {
    App.Site.init(); //Common
    App.Quiz.init(); //Common
});

App.Ajax = function() {
    var getDocs = function(category_id) {
         $.ajax({
            url: baseurl + "/api-get-docs",
            dataType: 'json',
            data: {
                category: category_id
            },
            type: 'post',
            success: function (res) {
                if (res.status) {
                    $('.your-profile-menu li').removeClass("active");
                    $(".category-" + category_id).addClass("active");

                    if(res.data.length > 0) {
                        let html = '';
                        for(let i = 0; i < res.data.length; i++) {
                            var d = new Date(res.data[i].created);
                            var datestring = d.getHours() + " giờ, ngày " + d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
                            let href = baseurl + "/doc-tai-lieu/" + res.data[i].id + "/" + res.data[i].slug;

                            let item = '<li><div class="notification-event">';
                            if (res.data[i].featured == 1) { 
                                item = '<li><div class="icon"><svg style="fill:orange" class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FAV PAGE"><use xlink:href="'+baseurl+'/skin/front/social/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg></div><div class="notification-event">';
                            }
                            item += '<a href="'+ href +'" class="h6 notification-friend">' + res.data[i].title + '</a>';
                            item += '<span class="chat-message-item">' + datestring + '</span></div><span class="notification-icon"><a href="'+ href +'" class="accept-request">Đọc tài liệu</a></span>';
                            
                            html += item;
                        }

                        $('#list-docs').html(html);
                    } else {
                        $('#list-docs').html('<li><div class="notification-event">Chưa có tài liệu</div></li>');
                    }
                } else {
                    
                }
            }
        });
    }

    var getQuizs = function(category_id) {
         $.ajax({
            url: baseurl + "/api-get-quizs",
            dataType: 'json',
            data: {
                category: category_id
            },
            type: 'post',
            success: function (res) {
                if (res.status) {
                    $('.your-profile-menu li').removeClass("active");
                    $(".category-" + category_id).addClass("active");
                        
                    if(res.data.length > 0) {
                        let html = '';
                        for(let i = 0; i < res.data.length; i++) {
                            var d = new Date(res.data[i].created);
                            var datestring = d.getHours() + " giờ, ngày " + d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
                            let href = baseurl + "/trac-nghiem/" + res.data[i].slug + "/" + res.data[i].id;

                            let item = '<li><div class="notification-event">';
                            if (res.data[i].featured == 1) { 
                                item = '<li><div class="icon"><svg style="fill:orange" class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FAV PAGE"><use xlink:href="'+baseurl+'/skin/front/social/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg></div><div class="notification-event">';
                            }
                            item += '<a href="'+ href +'" class="h6 notification-friend">' + res.data[i].title + '</a>';
                            item += '<span class="chat-message-item">' + datestring + '</span></div><span class="notification-icon"><a href="'+ href +'" class="accept-request">Làm bài</a></span>';
                            
                            html += item;
                        }

                        $('#list-docs').html(html);
                    } else {
                        $('#list-docs').html('<li><div class="notification-event">Chưa có đề thi</div></li>');
                    }
                } else {
                    
                }
            }
        });
    }

    var getVideos = function(category_id) {
         $.ajax({
            url: baseurl + "/api-get-videos",
            dataType: 'json',
            data: {
                category: category_id
            },
            type: 'post',
            success: function (res) {
                if (res.status) {
                    $('.your-profile-menu li').removeClass("active");
                    $(".category-" + category_id).addClass("active");
                        
                    if(res.data.length > 0) {
                        let html = '';
                        for(let i = 0; i < res.data.length; i++) {
                            var d = new Date(res.data[i].created);
                            var datestring = d.getHours() + " giờ, ngày " + d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
                            let href = baseurl + "/xem-video/" + res.data[i].slug + "/" + res.data[i].id;

                            let item = '<li><div class="notification-event">';
                            if (res.data[i].featured == 1) { 
                                item = '<li><div class="icon"><svg style="fill:orange" class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FAV PAGE"><use xlink:href="'+baseurl+'/skin/front/social/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg></div><div class="notification-event">';
                            }
                            item += '<a href="'+ href +'" class="h6 notification-friend">' + res.data[i].title + '</a>';
                            item += '<span class="chat-message-item">' + datestring + '</span></div><span class="notification-icon"><a href="'+ href +'" class="accept-request">Xem Video</a></span>';
                            
                            html += item;
                        }

                        $('#list-docs').html(html);
                    } else {
                        $('#list-docs').html('<li><div class="notification-event">Chưa có video</div></li>');
                    }
                } else {
                    
                }
            }
        });
    }

    var comment = function(dataObj, callback) {
        $.ajax({
            url: baseurl + "/comment",
            dataType: 'json',
            data: dataObj,
            type: 'post',
            success: function (res) {
                callback(res);
            }
        });
    }

    var getcomment = function(dataObj, callback) {
        $.ajax({
            url: baseurl + "/get-comment",
            dataType: 'json',
            data: dataObj,
            type: 'post',
            success: function (res) {
                callback(res);
            }
        });
    }

    return {
        getDocs: getDocs,
        getQuizs: getQuizs,
        getVideos: getVideos,
        comment: comment,
        getcomment: getcomment
    };
}();

//--All site
App.Site = function(){
    var init = function(){
        // active menu list
        if ($('#your-profile-menu .active').length > 0) {
            $('#your-profile-menu .active a').click();
        }

        // auto open login popup
        if ($(".page-500-sup-title").length > 0) {
            App.Popup.openLogin();
        }

        // form validate
        formProfileValidate();

        // chart
        var pieColorChart = document.getElementById("pie-chart");
        if (null !== pieColorChart) var ctx_pc = pieColorChart.getContext("2d"),
            data_pc = {
                labels: ["Sai", "Đúng", "Chưa làm"],
                datasets: [{
                    data: [$("#sentence_wrong").val(), $("#sentence_correct").val(), $("#sentence_notdo").val()],
                    borderWidth: 0,
                    backgroundColor: ["#ff5e3a", "#28a745", "#38a9ff"]
                }]
            },
            pieColorEl = new Chart(ctx_pc, {
                type: "doughnut",
                data: data_pc,
                options: {
                    deferred: {
                        delay: 300
                    },
                    cutoutPercentage: 93,
                    legend: {
                        display: !1
                    },
                    animation: {
                        animateScale: !1
                    }
                }
            });
    };

    // functions here
    var formRegValidate = function() {
        var formRegister = $('form#registerForm');
        if (formRegister.length < 1) {
            return;
        }
        $.validator.addMethod("validatePhone", function (value, element) {
            var flag = false;
            var phone = value;
            phone = phone.replace('(+84)', '0');
            phone = phone.replace('+84', '0');
            phone = phone.replace('0084', '0');
            phone = phone.replace(/ /g, '');
            if (phone != '') {
                var firstNumber = phone.substring(0, 3);
                var validNumber = ["086","096","097","098","032","033","034","035","036","037","038","039","089","090","093","070","079","077","076","078","091","094","083","084","085","081","082","092","056","058"];
                if ((jQuery.inArray(firstNumber,validNumber)!='-1') && phone.length == 10) {
                    if (phone.match(/^\d{10}/)) {
                        flag = true;
                    }
                }
            }
            if (phone == '') {flag = true;}
            return flag;
        }, "Hãy nhập đúng định dạng số điện thoại");
        formRegister.validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                email:{
                    required: true,
                    email:true
                },
                phone: {
                    required: false,
                    validatePhone:true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                first_name: {
                    required: "Vui lòng nhập họ"
                },
                last_name: {
                    required: "Vui lòng nhập tên"
                },
                phone: {
                },
                email: {
                    required: "Vui lòng nhập email",
                    email: "Vui lòng nhập đúng định dạng email"

                },
                password: {
                    required: "Vui lòng nhập mật khẩu"

                }
            },
            errorElement : 'p',
            errorClass: 'text-danger',
            errorPlacement: function(error, element) {

                error.insertAfter(element);

            },
            highlight: function (element, errorClass) {
                $(element).closest('.form-group').addClass('error');
            },
            unhighlight: function (element, errorClass) {
                $(element).closest('.form-group').removeClass('error');
            },
        });
    }

    var register = function() {
        var flag = 1;
        if ($('form#registerForm').valid()) {
            if (flag == 1) {
                var data = $('form#registerForm').serialize();

                flag = 0;
                $.ajax({
                    url: baseurl + "/api-user-register",
                    dataType: 'json',
                    data:data,
                    type: 'post',
                    success: function (res) {
                        flag = true;

                        //remove loading icon
                        if (res.status) {
                            // reset form
                            $('form#registerForm').trigger("reset");

                            $('#reg-error').hide();
                            $('#reg-success').show();

                            setTimeout(function(){ 
                                // change to login tab
                                $('#login-tab').click();
                            }, 3000);
                        } else {
                            $('#reg-error').show();
                            $('#reg-success').hide();

                            $('#reg-error').html(res.message);
                        }
                    }
                });
            }


        }
    }

    var formLoginValidate = function(){
        var formLogin = $('form#loginForm');
        if (formLogin.length < 1) {
            return;
        }
        formLogin.validate({
            rules: {
                email:{
                    required: true,
                    email:true
                },
                password: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: "Vui lòng nhập email",
                    email: "Vui lòng nhập đúng định dạng email"

                },
                password: {
                    required: "Vui lòng nhập mật khẩu"

                }
            },
            errorElement : 'p',
            errorClass: 'text-danger',
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).closest('.form-group').addClass('error');
            },
            unhighlight: function (element, errorClass) {
                $(element).closest('.form-group').removeClass('error');
            },
        });
    }

    var login = function(){
        var flag = 1;
        if ($('form#loginForm').valid()) {
            if (flag == 1) {
                var data = $('form#loginForm').serialize();
                flag = 0;
                $.ajax({
                    url: baseurl + "/api-user-login",
                    dataType: 'json',
                    data: data,
                    type: 'post',
                    success: function (res) {
                        flag = 1;

                        if (res.status) {
                            window.location.reload();
                        } else {
                            $("#loginForm").prepend('<label class="error text-danger">' + res.message + '</label>');
                        }
                    }
                });
            }


        }
    }

    var logout = function(user_id) {
        $.ajax({
            url: baseurl + "/user-logout",
            dataType: 'json',
            data: {
                user_id: user_id
            },
            type: 'post',
            success: function (res) {
                console.log(res);
                if (res.status) {
                    window.location.href = baseurl;
                } else {
                    
                }
            }
        });
    }

    // functions here
    var formProfileValidate = function() {
        var formProfile = $('form#profileForm');
        if (formProfile.length < 1) {
            return;
        }

        $.validator.addMethod("validatePhone", function (value, element) {
            var flag = false;
            var phone = value;
            phone = phone.replace('(+84)', '0');
            phone = phone.replace('+84', '0');
            phone = phone.replace('0084', '0');
            phone = phone.replace(/ /g, '');
            if (phone != '') {
                var firstNumber = phone.substring(0, 3);
                var validNumber = ["086","096","097","098","032","033","034","035","036","037","038","039","089","090","093","070","079","077","076","078","091","094","083","084","085","081","082","092","056","058"];
                if ((jQuery.inArray(firstNumber,validNumber)!='-1') && phone.length == 10) {
                    if (phone.match(/^\d{10}/)) {
                        flag = true;
                    }
                }
            }
            if (phone == '') {flag = true;}
            return flag;
        }, "Hãy nhập đúng định dạng số điện thoại");
        formProfile.validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                phone: {
                    required: false,
                    validatePhone:true
                }
            },
            messages: {
                first_name: {
                    required: "Vui lòng nhập họ"
                },
                last_name: {
                    required: "Vui lòng nhập tên"
                },
                phone: {
                }
            },
            errorElement : 'p',
            errorClass: 'text-danger',
            errorPlacement: function(error, element) {

                error.insertAfter(element);

            },
            highlight: function (element, errorClass) {
                $(element).closest('.form-group').addClass('error');
            },
            unhighlight: function (element, errorClass) {
                $(element).closest('.form-group').removeClass('error');
            },
        });
    }

    // _________Comment__________ //
    let commentFlag = true;
    let getcommentFlag = true;
    let commentID = 0;


    var writeComment = function(post_id) {
        if (commentID != post_id) {
            commentID = post_id;
            $('#comment-post').remove(); 

            let avatar = $('.avatar').attr('src');
            let htmlwrite = '<div id="comment-post" class="post__author author vcard inline-items"><img src="'+ avatar +'" alt="author"><div class="form-group with-icon-right "><textarea id="comment-content" class="form-control" placeholder=""></textarea></div></div><button class="btn btn-md-2 btn-primary" onclick="App.Site.comment('+ post_id +');">Bình luận</button>';
            $('#comment-form-' + post_id).html(htmlwrite);
        }
    }

    var comment = function(post_id) {
        if ($('#comment-content').val() != "") {
            $('#comment-content').css({'border-color': '#e6ecf5'});

            let comment = $('#comment-content').val();
            let dataObj = {
                'post_id': post_id,
                'comment': comment
            }

            if (commentFlag) {
                commentFlag = false;

                App.Ajax.comment(dataObj, function(res) {
                    console.log(res);
                    commentFlag = true;

                    if (res.status) {
                        $('#comment-content').val('');

                        let post_avatar = $('.avatar').attr('src');
                        let post_username = $('#user-login-name').val();
                        let post_time = '1 second ago';
                        let post_comment = res.data.item.comment;
                        $('#comments-list-' + post_id).append('<li class="comment-item"><div class="post__author author vcard inline-items"><img src="' + post_avatar + '" alt="author"><div class="author-date"><a class="h6 post__author-name fn" href="javascript:void(0);">' + post_username + '</a><div class="post__date"><time class="published" datetime="2017-03-24T18:18">' + post_time + '</time></div></div></div><p>'+ post_comment +'</p></li>');
                    } else {
                        window.location.reload();
                    }
                });
            }
        } else {
            $('#comment-content').css({'border-color': 'red'});
        }
    }

    var getComment = function(post_id) {
        if (getcommentFlag) {
            getcommentFlag = false;
            App.Ajax.getcomment({post_id: post_id}, function(res){
                console.log(res);
                getcommentFlag = true;

                if (res.status) {
                    $('#comments-list-' + post_id).empty()
                    let comments = res.data;
                    for(let comment of comments) {
                        let post_avatar = comment.avatar != "" ? (baseurl + '/media/images/' + comment.avatar) : baseurl + "/skin/front/social/img/avatar71-sm.jpg";
                        let post_username = comment.first_name + ' ' + comment.last_name;
                        let post_time = '1 second ago';
                        let post_comment = comment.comment;
                        $('#comments-list-' + post_id).append('<li class="comment-item"><div class="post__author author vcard inline-items"><img src="' + post_avatar + '" alt="author"><div class="author-date"><a class="h6 post__author-name fn" href="javascript:void(0);">' + post_username + '</a><div class="post__date"><time class="published" datetime="2017-03-24T18:18">' + post_time + '</time></div></div></div><p>'+ post_comment +'</p></li>');
                    }
                } else {
                    window.location.reload();
                }
                
            });
        }
            
    }
    // _________Comment__________ //


    return {
        init: init,
        formRegValidate: formRegValidate,
        register: register,
        formLoginValidate: formLoginValidate,
        login: login,
        logout: logout,
        comment: comment,
        writeComment: writeComment,
        getComment: getComment
    };
}();

App.Quiz = function(){
    var init = function(){
        if (typeof QUIZTIMER !== typeof undefined) {
            start(QUIZTIMER);

            // prevent reload page
            $(window).bind('beforeunload', function(){
              return 'Are you sure you want to leave?';
            });
        }

        // THPQ
        if ($('#THPQ').length > 0) {
            $('#THPQ').click();
        }
    };

    var create = function(post_id) {
        $.ajax({
            url: baseurl + "/create-quiz",
            dataType: 'json',
            data: {
                post_id: post_id
            },
            type: 'post',
            success: function (res) {
                if (res.status) {
                    // redirect to quiz page
                    window.location.href = baseurl + "/quiz/" + post_id + "/" + $("#slug").val() + "/" + res.return.id;
                }
            }
        });
    }

    var startTimer = function(duration, display) {
        var timer = duration, minutes, seconds;
        let timerInterval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            // change timer to red color
            if (timer == 300) {
                $("#timer").addClass("text-danger");
            }

            if (--timer < 0) {
                // timeup
                clearInterval(timerInterval);

                // end quiz
                endQuiz();
            }
        }, 1000);
    }

    var getChoices = function(maximum = 50) {
        let data = [];
        for (var n = 0; n < maximum; n++) {
            var radios = document.getElementsByName('quiz-anwser-' + (n+1));

            for (var i = 0, length = radios.length; i < length; i++) {
              if (radios[i].checked) {
                // do whatever you want with the checked radio
                data[n] = radios[i].value;

                // only one radio can be logically checked, don't check the rest
                break;
              }
            }
        }

        return data;
    }

    var start = function(minutes) {
        minutes = 60 * minutes,
        display = document.querySelector('#timer');
        startTimer(minutes, display);
    }

    var end = function() {
        let choices = getChoices(QUIZMaximum);
        if (choices.length > 0) {
            // show notification
            $("#notify-content").html("Bạn có chắc chắn muốn nộp bài không?");
            $("#notification-popup").modal('show');
            $("#notify-ok").attr({"notification": "ENDQUIZ"});
        }
    }

    var endQuiz = function() {
        // get the quiz choice
        let choices = getChoices(QUIZMaximum);
        let dataStr = JSON.stringify(choices);
        let worktime = $("#timer").html();

        $.ajax({
            url: baseurl + "/submit-quiz",
            dataType: 'json',
            data: {
                contest_id: QUIZContest,
                content_id: QUIZPost,
                user_id: QUIZUser,
                worktime: worktime,
                quiz: dataStr
            },
            type: 'post',
            success: function (res) {
                if (res.status) {
                    // redirect to page dap an
                    window.location.href = QUIZResultPage;
                }
            }
        });

        // unbind prevent
        $(window).unbind('beforeunload');
    }

    return {
        init: init,
        create: create,
        end: end,
        endQuiz: endQuiz
    };
}();

App.Popup = function () {
    var openLogin = function(){
        $('#registration-login-form-popup').modal('show');
        App.Site.formRegValidate();
        App.Site.formLoginValidate();
    }

    var accept = function(element) {
        if ($(element).attr("notification") != "") {
            switch ($(element).attr("notification")) {
                case "ENDQUIZ": App.Quiz.endQuiz(); break;
            }
        }

        $("#notification-popup").modal('hide');
    }

    return {
        openLogin: openLogin,
        accept: accept
    };
}();
//--End All site