'use strict';


/* HELPER: Checks Whether an Element Exists
----------------------------------------------------------------------------------------------------*/
(function( $ ) {

  $.fn.extend({
    exists: function() {
      if ( this.length > 0 ) {
        return true;
      } else {
        return false;
      }
    }
  });

})( jQuery );



jQuery(document).on("ready",function () {
	
});
$('.toggle-menu').click(function(){
	$(".header-mobile").css("left","0");
});
$('.md-close').click(function(){
	$(".header-mobile").css("left","-110" + "%");
});
// $('.select-language').click(function(){
// 	$('.choose-language').slideToggle();
// 	$(this).css('display','none');
// });
$('.ui.dropdown')
  .dropdown()
;



$('.slider-banner').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay: true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
$('.slider-our-project').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        600:{
            items:1,
            nav:true,
            loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});

$('.slider-news').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        600:{
            items:1,
            nav:true,
            loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});

$('.slider-talk').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay: true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.slider-blk-yt4').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        600:{
            items:1,
            nav:true,
            loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});


var hm_img = $(".meaning .image-meaning img").length;
var width_inner = $(".meaning .ctn-inner").width();
var width_scroll = width_inner * hm_img;

if($(window).width()<=768){
    $(".meaning .image-meaning img").css('width',width_inner);
     $(".meaning .md-scroll").css('width',width_scroll);
}else{
    $(".meaning .image-meaning img").css('width',25 + "%");
     $(".meaning .md-scroll").css('width',100 + '%');
}


$(window).resize(function(){
        
        var hm_img = $(".meaning .image-meaning img").length;
        var width_inner = $(".meaning .ctn-inner").width();
        var width_scroll = width_inner * hm_img;
        if($(window).width()<=768){
            $(".meaning .image-meaning img").css('width',width_inner);
             $(".meaning .md-scroll").css('width',width_scroll);
        }else{
            $(".meaning .image-meaning img").css('width',25 + "%");
             $(".meaning .md-scroll").css('width',100 + '%');
        }

});




var timer = false;
$('.title-through').each(function(i, obj) {
    var width_title_through=$(this).width()-$(this).find("h3").width() - 25;
    $(this).find(".line-through") .css('width', width_title_through);
});

var h3height = 0;
    $('.block-four-col .item').each(function() {
        if(h3height < $(this).height()){
          h3height = $(this).height();
        };
    });
$('.block-four-col .item').height(h3height); 

$(window).resize(function(){
        
       $('.title-through').each(function(i, obj) {
    var width_title_through=$(this).width()-$(this).find("h3").width() - 25;
    $(this).find(".line-through") .css('width', width_title_through);
    $('.block-four-col .item').css("height","auto");
            var h3height = 0;
            $('.block-four-col .item').each(function() {
                if(h3height < $(this).height()){
                  h3height = $(this).height();
                };
            });
            $('.block-four-col .item').height(h3height);
    });
});

var height_1 = $('.news').height();
var height_2 = $('.event .search').height();
var height_3 = $('.event .title-through').height();
var height_4 = height_1 - height_2 - height_3 - 30;
$('.list-event').css('height', height_4);


var timer = false;
$(window).resize(function() {
    if (timer) clearTimeout(timer);
    timer = setTimeout(function(){
        
      var height_1 = $('.news').height();
    var height_2 = $('.event .search').height();
    var height_3 = $('.event .title-through').height();
    var height_4 = height_1 - height_2 - height_3 - 30;
    $('.list-event').css('height', height_4); 
    }, 300);
});

 

var width_ggmap = $('.md-gg-map').width();
var height_ggmap = width_ggmap * 0.29;
$('.md-gg-map').css('height',height_ggmap);

var timer = false;
    $(window).resize(function() {
        if (timer) clearTimeout(timer);
        timer = setTimeout(function(){
            
           var width_ggmap = $('.md-gg-map').width();
           var height_ggmap = width_ggmap * 0.29;
           $('.md-gg-map').css('height',height_ggmap);

        }, 300);
    });







